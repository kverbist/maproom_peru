<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      xmlns:wn30="http://purl.org/vocabularies/princeton/wn30/"
      xmlns:wordnet-ontology="http://wordnet-rdf.princeton.edu/ontology#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Probabilidades de Condiciones H&#250;medos o Secos seg&#250;n la Fase de ENSO</title>
      <link rel="stylesheet" type="text/css" href="../../unesco.css" />
      <link class="share" rel="canonical" href="ENSO_PRCP_Prob_TS2p1.html" />
      <link class="altLanguage" rel="alternate" hreflang="en" href="ENSO_PRCP_Prob_TS2p1.html?Set-Language=en" />
      <link class="carryLanguage" rel="home" href="http://iri.columbia.edu/" title="IRI" />
      <link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />
      <link rel="iridl:hasGlossary" href="/dochelp/definitions/index.html" />
      <meta xml:lang="" property="maproom:Entry_Id" content="ENSO_Climate_Impacts_ENSO_PRCP_Prob_TS2p1" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_term" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#LandSurface" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#PlanetarySurface" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Surface" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#enso" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_Climate_Impacts_term" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#seasonal" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#percentile" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#interactive" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere" />
      <link rel="term:icon" href="http://ons.snirh.gob.pe/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/.Precipitation/T/%28Jan-Mar%29//season/parameter/seasonalAverage/%5BT%5Dpercentileover/%7BDry/0.33333/Normal/0.66667/Wet%7Dclassify/SOURCES/.Peru/.SENAMHI/.PISCO/.Peru_mask/.SPI_mask/mul/%28http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCDC/.ERSST/.version4/.sst/dods%29/readdods/.sst/zlev/removeGRID/X/-85/-60/RANGE/Y/-16/0.5/RANGEEDGES/a:/:a:/T/%28Jan%201981%29/%28Dec%202010%29/RANGE/yearly-climatology/:a/sub/%7BY/cosd%7D%5BX/Y%5Dweighted-average/T/%28Aug%201949%29/last/RANGE/T/3/1.0/runningAverage/%7BLaNina/-0.45/Neutral/0.45/ElNino%7Dclassify/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/5/flagge/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/1.0/flagge/dup/a:/sst/%28LaNina%29/VALUE/:a:/sst/%28ElNino%29/VALUE/:a/add/1/maskge/dataflag/1/index/2/flagge/add/sst/%28phil%29/unitmatrix/sst_out/%28Neutral%29/VALUE/mul/exch/sst/%28phil2%29/unitmatrix/sst_out/%28LaNina%29/%28ElNino%29/VALUES/%5Bsst_out%5Dsum/mul/add/T//season/get_parameter/VALUES/1/masklt/mul/%5BT%5Daverage//long_name/%28Probabilidad%29/def/tercileclassesscale/sst//ENSO/renameGRID/Precipitation//Tercile/renameGRID/grid://name//Tercilebis/def//units//ids/def/values:/%28Seca%29/%28Normal%29/%28Humeda%29/:values/:grid/CopyStream//name//Tercile_es/def/streamgrids/Tercile/replaceGRID/exch/2/%7Bexch/ENSO/%28ElNino%29//ENSO/parameter/VALUE/Tercile/%28Dry%29//Tercile/parameter/VALUE%7Drepeat/DATA/0/1/RANGE/ENSO/%28ElNino%29//ENSO/parameter/VALUE/Tercile/%28Dry%29//Tercile/parameter/VALUE//name//proba/def/exch/X/Y/fig-/colors/thinnish/coasts_gaz/grey/countries_gaz/plotlabel/-fig//plotaxislength/600/psdef//plotborderbottom/40/psdef//plotbordertop/40/psdef//antialias/true/psdef//Y/-19/0.5/plotrange//framelabel/%28La%20probabilidad%20de%20una%20condicion%20%22%25=%5BTercile_es%5D%22%20para%20un%20anio%20%22%25=%5BENSO%5D%22%29/psdef//fontsize/20/psdef+//framelabel+%28La%20probabilidad%20de%20una%20condicion%20%22%25=%5BTercile_es%5D%22%20para%20un%20anio%20%22%25=%5BENSO%5D%22%29+psdef//plotborder+0+psdef//plotaxislength+432+psdef//fontsize+20+psdef//antialias+true+psdef//color_smoothing+1+psdef+.gif" /><script type="text/javascript" src="../../../uicore/uicore.js"></script><script type="text/javascript" src="../../../maproom/unesco.js"></script></head>
   <body xml:lang="es">
      
      <form name="pageform" id="pageform" class="carryup carry dlimg dlauximg share">
         <input class="carryup carry " name="Set-Language" type="hidden" />
         <input class="carry dlimg dlimgloc share" name="bbox" type="hidden" />
         <input class="carry dlimg dlimgts share" name="season" type="hidden" data-default="Jan-Mar" />
         <input class="carry share dlimgts dlimgloc dlimglocclick" name="region" type="hidden" /> 
         <input class="transformRegion dlimglocclick" name="clickpt" type="hidden" />
         <input class="dlimgloc dlimgts carry pickarea admin" name="resolution" type="hidden" data-default="irids:SOURCES:Features:Hydrological:Cuencas:Mayores:ds" />
         <input class="carry dlimg share" name="Tercile" type="hidden" data-default="Dry" />
         <input class="carry dlimg share" name="ENSO" type="hidden" data-default="ElNino" />
         <input class="bodyClass dlimg dlauximg share" name="maskselect" type="hidden" data-default="prob" />
         <input class="dlimg dlauximg" name="plotaxislength" type="hidden" />
         <input class="dlimg share" name="layers" value="probmap" checked="checked" type="checkbox" />
         <input class="dlimg share" name="layers" value="lakes" checked="checked" type="checkbox" />
         <input class="dlimg share" name="layers" value="coasts_gaz" checked="checked" type="checkbox" />
         <input class="dlimg share" name="layers" value="countries_gaz" checked="checked" type="checkbox" />
         <input class="dlimg share" name="layers" value="states_gaz" checked="checked" type="checkbox" />
         
      </form>
      
      <div class="controlBar">
         
         <fieldset class="navitem" id="toSectionList"> 
            
            <legend>Per&#250;</legend> 
            <a rev="section" class="navlink carryup" href="/maproom/ENSO/Impacts.html">Impactos del ENSO</a>
            
         </fieldset> 
         
         <fieldset class="navitem" id="chooseSection"> 
            
            <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_Climate_Impacts_term"><span property="term:label">Per&#250;</span></legend>
            
         </fieldset> 
         
         <fieldset class="navitem">
            
            <legend>Regi&#243;n</legend>
            <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsPeru.json" shape="rect"></a>
            <select class="RegionMenu" name="bbox">
               <option value="">Per&#250;</option>
               <optgroup class="template" label="Region">
                  <option class="irigaz:hasPart irigaz:id@value term:label"></option>
               </optgroup></select>
            
         </fieldset>
         
           <fieldset class="navitem"><legend>Temporada</legend><span class="selectvalue"></span><select class="pageformcopy" name="season"><option value="Jan-Mar">Ene-Mar</option><option value="Feb-Apr">Feb-Abr</option><option value="Mar-May">Mar-May</option><option value="Apr-Jun">Abr-Jun</option><option value="May-Jul">May-Jul</option><option value="Jun-Aug">Jun-Ago</option><option value="Jul-Sep">Jul-Sep</option><option value="Aug-Oct">Ago-Oct</option><option value="Sep-Nov">Sep-Nov</option><option value="Oct-Dec">Oct-Dec</option><option value="Nov-Jan">Nov-Ene</option><option value="Dec-Feb">Dec-Feb</option></select></fieldset>
  
          <fieldset class="navitem"><legend>Condici&#243;n</legend><span class="selectvalue"></span><select class="pageformcopy" name="Tercile"><option value="Dry">Seco</option><option value="Normal">Normal</option><option value="Wet">H&#250;medo</option></select></fieldset>
  
          <fieldset class="navitem"><legend>Fase ENSO</legend><span class="selectvalue"></span><select class="pageformcopy" name="ENSO"><option value="ElNino">El Ni&#241;o</option><option value="Neutral">Neutral</option><option value="LaNina">La Ni&#241;a</option></select></fieldset>         


         <fieldset class="navitem">
              <legend>Promedio Espacial Sobre</legend><span class="selectvalue"></span><select class="pageformcopy" name="resolution">
               <option value="0.5">Ubicaci&#243;n Puntual</option>
               <option value="irids:SOURCES:Features:Political:Peru:Departamento:ds">Departamento</option>
               <option value="irids:SOURCES:Features:Political:Peru:Provincia:ds">Provincia</option>
               <option value="irids:SOURCES:Features:ANA:AmbAdm:AAA:ds">Autoridad Administrativa del Agua  (AAA)</option>
               <option value="irids:SOURCES:Features:ANA:AmbAdm:ALA:ds">Administraci&#243;n Local de Agua (ALA)</option>
               <option value="irids:SOURCES:Features:Hydrological:Cuencas:Mayores:ds">Unidad Hidrogr&#225;fica</option></select>       	    
            
            <link class="admin" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/%28irids:SOURCES:Features:Hydrological:Cuencas:Mayores:ds%29//resolution/parameter/geoobject/%28bb:-85:-19:-65:0:bb%29//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
            <select class="pageformcopy" name="region">
               <optgroup class="template" label="Label">
                  <option class="iridl:values region@value label"></option>
               </optgroup></select>            
            
         </fieldset>	     
         
      </div>
      
      
      
      <div class="ui-tabs">
         
         
         <ul class="ui-tabs-nav">
            
            
            <li><a href="#tabs-1">Descripción</a></li>
            
            
            <li><a href="#tabs-2">Documentación</a></li>
            
            
            <li><a href="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.Analyses/.ENSO-RP/.ver1950-2002/">Fuente</a></li>
            
            
            <li><a href="#tabs-3">Contáctanos</a></li> 
            
            
         </ul>
         
         
<fieldset class="regionwithinbbox dlimage bis" about="">
  <img class="dlimgloc" src="http://ons.snirh.gob.pe/SOURCES/.Peru/.CAZALAC/.MinExpectedPrecip/.5YR/X/Y/%28bb:-82%2C-17%2C-68%2C0%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-76%2C-8%2C-76.2%2C-8.2%29/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborder/0/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotaxislength/120/psdef/+.gif" />

  <div class="valid" style="display: inline-block; text-align: top;">
  <a class="dlimgloc" rel="iridl:hasJSON" href="/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json"></a>
  <div class="template" align="center">Observacones para <span class="bold iridl:long_name"></span></div>
  </div>
  <br />
  <br />
  <br />


  <img class="dlimgts bis1" rel="iridl:hasFigureImage" src="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/.Precipitation//long_name/%28Precipitaci%C3%B3n%20Mensual%29/def//units/%28mm%29/def/-Infinity/maskle/%28bb:-71.5%2C-5.0%2C-71.0%2C-4.5%29//region/parameter/geoobject/%5BX/Y%5D0.0/weighted-average/%28Jan-Mar%29//season/parameter/pop/T//season/get_parameter/seasonalAverage/T//season/get_parameter/VALUES/DATA/AUTO/AUTO/RANGE/dup/%5BT%5D0.33333/0.66667/0/replacebypercentile/1/index/0.0/mul/add//fullname/%28Precipitaci%C3%B3n%20Normal%20-%20Percentil%29/def/%28http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCDC/.ERSST/.version4/.sst/dods%29/readdods/.sst/zlev/removeGRID/X/-170/-120/RANGE/Y/-5/5/RANGEEDGES/a:/:a:/T/%28Jan%201981%29/%28Dec%202010%29/RANGE/yearly-climatology/:a/sub/%7BY/cosd%7D%5BX/Y%5Dweighted-average/T/%28Aug%201949%29/last/RANGE/T/3/1.0/runningAverage/%7BLaNina/-0.45/Neutral/0.45/ElNino%7Dclassify/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/5/flagge/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/1.0/flagge/dup/a:/sst/%28LaNina%29/VALUE/:a:/sst/%28ElNino%29/VALUE/:a/add/1/maskge/dataflag/1/index/2/flagge/add/sst/%28phil%29/unitmatrix/sst_out/%28Neutral%29/VALUE/mul/exch/sst/%28phil2%29/unitmatrix/sst_out/%28LaNina%29/%28ElNino%29/VALUES/%5Bsst_out%5Dsum/mul/add/T/%28Jan%201983%29/%28Dec%202014%29/RANGE/T//season/get_parameter/VALUES/%5Bsst%5Ddominant_class//long_name/%28Condici%C3%B3n%20ENSO%29/def/CLIST/exch//CLIST/undef/1.0/sub/exch//CLIST/exch/def/startcolormap/DATA/0/2/RANGE/blue/blue/blue/grey/red/red/endcolormap/exch/dup/percentile/last/VALUE/exch/percentile/first/VALUE/T/fig-/colorbars2/medium/solid/black/line/dashed/black/line/-fig//antialias/true/def/+.gif" />
  <br />

  <img class="dlimgts bis2" rel="iridl:hasFigureImage" src="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/.Precipitation/%28bb:-71.5%2C-5.0%2C-71.0%2C-4.5%29//region/parameter/geoobject/%5BX/Y%5D0.0/weighted-average/%28Jan-Mar%29//season/parameter/pop/T//season/get_parameter/seasonalAverage/T//season/get_parameter/VALUES/DATA/AUTO/AUTO/RANGE/dup/%5BT%5D0.33333/0.66667/0/replacebypercentile/1/index/0.0/mul/add//fullname/%28Rainfall%20Tercile%29/def/%28http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCDC/.ERSST/.version4/.sst/dods%29/readdods/.sst/zlev/removeGRID/X/-170/-120/RANGE/Y/-5/5/RANGEEDGES/a:/:a:/T/%28Jan%201981%29/%28Dec%202010%29/RANGE/yearly-climatology/:a/sub/%7BY/cosd%7D%5BX/Y%5Dweighted-average/T/%28Aug%201949%29/last/RANGE/T/3/1.0/runningAverage/%7BLaNina/-0.45/Neutral/0.45/ElNino%7Dclassify/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/5/flagge/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/1.0/flagge/dup/a:/sst/%28LaNina%29/VALUE/:a:/sst/%28ElNino%29/VALUE/:a/add/1/maskge/dataflag/1/index/2/flagge/add/sst/%28phil%29/unitmatrix/sst_out/%28Neutral%29/VALUE/mul/exch/sst/%28phil2%29/unitmatrix/sst_out/%28LaNina%29/%28ElNino%29/VALUES/%5Bsst_out%5Dsum/mul/add/T/%28Jan%201983%29/%28Dec%202014%29/RANGE/T//season/get_parameter/VALUES/%5Bsst%5Ddominant_class//long_name/%28ENSO%20Phase%29/def/CLIST/exch//CLIST/undef/1.0/sub/exch//CLIST/exch/def/startcolormap/DATA/0/2/RANGE/blue/blue/blue/grey/red/red/endcolormap/exch/dup/percentile/last/VALUE/exch/percentile/first/VALUE/T/fig-/colorbars2/medium/dashed/black/line/solid/black/line/-fig//antialias/true/def/+.auxfig/+.gif" />
</fieldset>
         
         <fieldset class="dlimage" id="content" about="">
            
            <link rel="iridl:hasFigure" href="http://ons.snirh.gob.pe/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/.Precipitation/T/%28Jan-Mar%29//season/parameter/seasonalAverage/%5BT%5Dpercentileover/%7BDry/0.33333/Normal/0.66667/Wet%7Dclassify/SOURCES/.Peru/.SENAMHI/.PISCO/.Peru_mask/.SPI_mask/mul/%28http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCDC/.ERSST/.version4/.sst/dods%29/readdods/.sst/zlev/removeGRID/X/-85/-60/RANGE/Y/-16/0.5/RANGEEDGES/a:/:a:/T/%28Jan%201981%29/%28Dec%202010%29/RANGE/yearly-climatology/:a/sub/%7BY/cosd%7D%5BX/Y%5Dweighted-average/T/%28Aug%201949%29/last/RANGE/T/3/1.0/runningAverage/%7BLaNina/-0.45/Neutral/0.45/ElNino%7Dclassify/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/5/flagge/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/1.0/flagge/dup/a:/sst/%28LaNina%29/VALUE/:a:/sst/%28ElNino%29/VALUE/:a/add/1/maskge/dataflag/1/index/2/flagge/add/sst/%28phil%29/unitmatrix/sst_out/%28Neutral%29/VALUE/mul/exch/sst/%28phil2%29/unitmatrix/sst_out/%28LaNina%29/%28ElNino%29/VALUES/%5Bsst_out%5Dsum/mul/add/T//season/get_parameter/VALUES/1/masklt/mul/%5BT%5Daverage//long_name/%28Probabilidad%29/def/tercileclassesscale/sst//ENSO/renameGRID/Precipitation//Tercile/renameGRID/grid://name//Tercilebis/def//units//ids/def/values:/%28Seca%29/%28Normal%29/%28H%C3%BAmeda%29/:values/:grid/CopyStream//name//Tercile_es/def/streamgrids/Tercile/replaceGRID/exch/2/%7Bexch/ENSO/%28ElNino%29//ENSO/parameter/VALUE/Tercile/%28Dry%29//Tercile/parameter/VALUE%7Drepeat/DATA/0/1/RANGE/ENSO/%28ElNino%29//ENSO/parameter/VALUE/Tercile/%28Dry%29//Tercile/parameter/VALUE//name//proba/def/exch/X/Y/fig-/colors/thinnish/coasts_gaz/grey/countries_gaz/plotlabel/-fig//plotaxislength/600/psdef//plotborderbottom/40/psdef//plotbordertop/40/psdef//antialias/true/psdef//Y/-19/0.5/plotrange//framelabel/%28La%20probabilidad%20de%20una%20condici%C3%B3n%20%22%25=%5BTercile_es%5D%22%20para%20un%20a%C3%B1o%20%22%25=%5BENSO%5D%22%29/psdef//fontsize/20/psdef/" />
            <img class="dlimg" src="http://ons.snirh.gob.pe/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/.Precipitation/T/%28Jan-Mar%29//season/parameter/seasonalAverage/%5BT%5Dpercentileover/%7BDry/0.33333/Normal/0.66667/Wet%7Dclassify/SOURCES/.Peru/.SENAMHI/.PISCO/.Peru_mask/.SPI_mask/mul/%28http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCDC/.ERSST/.version4/.sst/dods%29/readdods/.sst/zlev/removeGRID/X/-85/-60/RANGE/Y/-16/0.5/RANGEEDGES/a:/:a:/T/%28Jan%201981%29/%28Dec%202010%29/RANGE/yearly-climatology/:a/sub/%7BY/cosd%7D%5BX/Y%5Dweighted-average/T/%28Aug%201949%29/last/RANGE/T/3/1.0/runningAverage/%7BLaNina/-0.45/Neutral/0.45/ElNino%7Dclassify/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/5/flagge/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/1.0/flagge/dup/a:/sst/%28LaNina%29/VALUE/:a:/sst/%28ElNino%29/VALUE/:a/add/1/maskge/dataflag/1/index/2/flagge/add/sst/%28phil%29/unitmatrix/sst_out/%28Neutral%29/VALUE/mul/exch/sst/%28phil2%29/unitmatrix/sst_out/%28LaNina%29/%28ElNino%29/VALUES/%5Bsst_out%5Dsum/mul/add/T//season/get_parameter/VALUES/1/masklt/mul/%5BT%5Daverage//long_name/%28Probabilidad%29/def/tercileclassesscale/sst//ENSO/renameGRID/Precipitation//Tercile/renameGRID/grid://name//Tercilebis/def//units//ids/def/values:/%28Seca%29/%28Normal%29/%28H%C3%BAmeda%29/:values/:grid/CopyStream//name//Tercile_es/def/streamgrids/Tercile/replaceGRID/exch/2/%7Bexch/ENSO/%28ElNino%29//ENSO/parameter/VALUE/Tercile/%28Dry%29//Tercile/parameter/VALUE%7Drepeat/DATA/0/1/RANGE/ENSO/%28ElNino%29//ENSO/parameter/VALUE/Tercile/%28Dry%29//Tercile/parameter/VALUE//name//proba/def/exch/X/Y/fig-/colors/thinnish/coasts_gaz/grey/countries_gaz/plotlabel/-fig//plotaxislength/600/psdef//plotborderbottom/40/psdef//plotbordertop/40/psdef//antialias/true/psdef//Y/-19/0.5/plotrange//framelabel/%28La%20probabilidad%20de%20una%20condici%C3%B3n%20%22%25=%5BTercile_es%5D%22%20para%20un%20a%C3%B1o%20%22%25=%5BENSO%5D%22%29/psdef//fontsize/20/psdef/Tercile/last/plotvalue/ENSO/last/plotvalue+.gif" border="0" alt="image" />
            <br />
            <img class="dlauximg" src="http://ons.snirh.gob.pe/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/.Precipitation/T/%28Jan-Mar%29//season/parameter/seasonalAverage/%5BT%5Dpercentileover/%7BDry/0.33333/Normal/0.66667/Wet%7Dclassify/SOURCES/.Peru/.SENAMHI/.PISCO/.Peru_mask/.SPI_mask/mul/%28http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCDC/.ERSST/.version4/.sst/dods%29/readdods/.sst/zlev/removeGRID/X/-85/-60/RANGE/Y/-16/0.5/RANGEEDGES/a:/:a:/T/%28Jan%201981%29/%28Dec%202010%29/RANGE/yearly-climatology/:a/sub/%7BY/cosd%7D%5BX/Y%5Dweighted-average/T/%28Aug%201949%29/last/RANGE/T/3/1.0/runningAverage/%7BLaNina/-0.45/Neutral/0.45/ElNino%7Dclassify/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/5/flagge/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/1.0/flagge/dup/a:/sst/%28LaNina%29/VALUE/:a:/sst/%28ElNino%29/VALUE/:a/add/1/maskge/dataflag/1/index/2/flagge/add/sst/%28phil%29/unitmatrix/sst_out/%28Neutral%29/VALUE/mul/exch/sst/%28phil2%29/unitmatrix/sst_out/%28LaNina%29/%28ElNino%29/VALUES/%5Bsst_out%5Dsum/mul/add/T//season/get_parameter/VALUES/1/masklt/mul/%5BT%5Daverage//long_name/%28Probabilidad%29/def/tercileclassesscale/sst//ENSO/renameGRID/Precipitation//Tercile/renameGRID/grid://name//Tercilebis/def//units//ids/def/values:/%28Seca%29/%28Normal%29/%28H%C3%BAmeda%29/:values/:grid/CopyStream//name//Tercile_es/def/streamgrids/Tercile/replaceGRID/exch/2/%7Bexch/ENSO/%28ElNino%29//ENSO/parameter/VALUE/Tercile/%28Dry%29//Tercile/parameter/VALUE%7Drepeat/DATA/0/1/RANGE/ENSO/%28ElNino%29//ENSO/parameter/VALUE/Tercile/%28Dry%29//Tercile/parameter/VALUE//name//proba/def/exch/X/Y/fig-/colors/thinnish/coasts_gaz/grey/countries_gaz/plotlabel/-fig//plotaxislength/600/psdef//plotborderbottom/40/psdef//plotbordertop/40/psdef//antialias/true/psdef//Y/-19/0.5/plotrange//framelabel/%28La%20probabilidad%20de%20una%20condici%C3%B3n%20%22%25=%5BTercile_es%5D%22%20para%20un%20a%C3%B1o%20%22%25=%5BENSO%5D%22%29/psdef//fontsize/20/psdef/Tercile/last/plotvalue/ENSO/last/plotvalue+.auxfig/+.gif" />
            
         </fieldset>
         
         
         
         <div id="tabs-1" class="ui-tabs-panel" about="">
            
            
            <h2 align="center" property="term:title">Probabilidades de Condiciones Húmedos o Secos según la Fase de ENSO</h2>
            
            
            <p align="left" property="term:description"> Este mapa muestra la probablidad de encontrar condiciones húmedas o secas según la
               Fase de ENSO (El Niño, La Niña o Normal).
               
            </p>
            
            <p align="left">Se puede utilizar los controles en la parte superior de la página para seleccionar el periodo
               de tres meses de interés, la fase del ENSO y la condición (húmeda, normal o seca). Tambi&#233;n permite visualizar la precipitaci&#243;n anual de un punto de inter&#233;s, con indicaci&#243;n de los a&#241;os 'El Ni&#241;o', 'La Ni&#241;a' y los a&#241;os normales.
            </p>
            
            <p align="left">Para este análisis, se definen los eventos El Niño y La Niña según la anomalía de
               la temperatura superficial del mar en la zona NINO3.4. Se evalúa la relación entre
               la precipitación y la fase del ENSO para periodos consecutivos de tres meses, y el
               mapa muestra la proporción de los años que muestran condiciones húmedas, normales
               o secas.
               		 Como ejemplo, un punto que muestra un valor 0.6 en la categoría 'húmeda' (mayor
               precipitación que lo normal) durante la fase 'Niño', indica que para los años entre
               1950-2002 donde ocurrió un año 'El Niño', 6 de los 10 años mostraron condiciones húmedas
               durante este fenómeno.     
               
            </p>
            
            
             <p align="left"><b>Fuente de Datos</b> 
               
               
            </p>
            
            
            
            <p align="left">Los datos usados para el cálculo provienen de estaciones meteorológicas del Servicio
               Nacional de Meterología e Hidrología del Perú  (SENAMHI) y la base de datos PISCO
               ('Peruvian Interpolation of the SENAMHIs Climatological and Hydrological Stations').
               
               <p align="left"><b>NOTA:</b> La base de datos de PISCO se encuentra validada hasta diciembre 2013, mientras que
                  los valores posteriores a esa fecha son valores preliminares que serán validados posteriormente.
                  
               </p>
                              
            </p>
            <p align="left"><a href="http://www.peruclima.pe/?p=Indicadores-de-sequias" shape="rect"><img src="../../icons/Logo_Senamhi.jpg" alt="Logo SENAMHI" /></a></p>

            
            <h4>Referencias</h4>
                      
            <p>
               Mason, S.J. and L. Goddard, 2001: Probabilistic precipitation anomalies associated
               with ENSO. <i>Bull. Amer. Meteor. Soc.</i>, <b>82</b>, 619-638. doi: <a href="http://dx.doi.org/10.1175/1520-0477(2001)082&lt;0619:PPAAWE?2.3.CO;2">http://dx.doi.org/10.1175/1520-0477(2001)082&lt;0619:PPAAWE?2.3.CO;2</a>
               
               
            </p>
            <p align="left">Waldo Lavado Casimiro, Carlos Fernandez, Fiorella Vega, Tania Caycho, Sofia Endara,
               Adrian Huerta y Oscar FelSPI Obando, 2015. <a href="PISCO_reporte.pdf" shape="rect">
                  			PISCO: Peruvian Interpolated data of the SENAMHIs Climatological and hydrological
                  Observations. Precipitación v1.0. </a><i>Servicio Nacional de Meteorología e Hidrología</i>.               
               
            </p>
            
            
         </div>
         
         
         <div id="tabs-2" class="ui-tabs-panel">
            
            
            <h2 align="center">Documentación</h2>
            
            
            <h4>Datos de Precipitación</h4>
            
            
            <dl class="datasetdocumentation">
               
 	     <p align="left"><a href="http://ons.snirh.gob.pe/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/" shape="rect">Precipitación observada en estaciones</a>, entregado por SENAMHI <a href="http://www.senamhi.gob.pe/" shape="rect">(SENAMHI)</a> 
            </p>
               
            	     
            <p align="left">Los datos mensuales de las variables de precipitación del periodo 1981 al 2013 con
               un número de estaciones variables desde 180 hasta 448, después de este año hasta la
               actualidad son utilizados para la elaboración del producto PISCO, datos de información
               pasando esa fecha no tiene control de calidad y el uso de ellos vienen con la incertidumbre
               si van a ser modificados al hacer dicho control.
            </p>
            	     
            <p align="left">Nota: Estos datos pueden variar por indicación de la Dirección Regional del SENAMHI
               o la comparación con la planilla meteorológica.
            </p>
            	     
            <p align="left">En la Figura se muestra el número de estaciones utilizado cada año. </p>
            	     
            <p align="left"><img src="PISCO_estaciones.png" alt="numero de estaciones" /></p>              
            
            
            <p align="left"><b>Referencia</b> 
            </p>
            
            <p align="left">Waldo Lavado Casimiro, Carlos Fernandez, Fiorella Vega, Tania Caycho, Sofia Endara,
               Adrian Huerta y Oscar FelSPI Obando, 2015. <a href="PISCO_reporte.pdf" shape="rect">
                  			PISCO: Peruvian Interpolated data of the SENAMHIs Climatological and hydrological
                  Observations. Precipitación v1.0. </a><i>Servicio Nacional de Meteorología e Hidrología</i>. 
            </p>
            
            <p align="left"><a href="http://www.peruclima.pe/?p=Indicadores-de-sequias" shape="rect"><img src="../../icons/SENAMHI" alt="Logo SENAMHI" /></a></p>

               
               
            </dl>
            
            
            <h4>Datos de Temperatura Superficial del Mar</h4>
            
            
            <dl class="datasetdocumentation">
               
               
               <dt>Data</dt>
               
               <dd>NINO3.4 índice mensual de temperatura del mar superficial para 1950-2002 (promedio
                  espacial sobre la región 5°S
                  a 5°N y 170°W a 120°W).
                  
               </dd>
               
               
               <dt>Data Source</dt>
               
               <dd>Acceder a la base de datos en (<a href="http://iridl.ldeo.columbia.edu/SOURCES/.KAPLAN/.EXTENDED/">la librería de datos climáticos</a>) 
                  
               </dd>
               
               
               <dt>Referencia</dt>
               
               <dd>Kaplan, A., M. Cane, Y. Kushnir, A. Clement, M. Blumenthal, and B. Rajagopalan, 1998:
                  <a href="http://onlinelibrary.wiley.com/doi/10.1029/97JC01736/abstract">Analyses of global sea surface temperature 1856-1991</a>. <i>Journal of Geophysical Research</i>. <b>103</b>, 18,567-18,589.  DOI: 10.1029/97JC01736
                  
               </dd>
               
               
            </dl>
            
            
         </div>
         
         
         <div class="ui-tabs-panel-hidden">
            
            
            <h2 align="center">Fuente</h2>
            
            
            <p>
               <a href="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.Analyses/.ENSO-RP/.ver1950-2002/">Access the dataset used to create this map.</a>
               
               
            </p>
            
            
         </div>
         
         <div id="tabs-3" class="ui-tabs-panel">
            
            <h2 align="center">Helpdesk</h2>
            
            <p>
               Contact <a href="mailto:help@iri.columbia.edu?subject=Historical Probability of Seasonal Gridded Precipitation Tercile Conditioned on ENSO">help@iri.columbia.edu</a> with any technical questions or problems with this Map Room.
               
            </p>
            
         </div>
         
      </div>
      
      <div class="optionsBar">
         
         <fieldset class="navitem" id="share">
            <legend>Compartir</legend>
         </fieldset>
         
         <fieldset class="navitem langgroup" id="contactus"></fieldset>
         
      </div>
      
   </body>
 </html>
