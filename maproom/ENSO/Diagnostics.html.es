<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
 xml:lang="es"
      >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Monitoreo de ENSO</title>
<link rel="stylesheet" type="text/css" href="../unesco.css" />
<link rel="canonical" href="Diagnostics.html" />
<link class="altLanguage" rel="alternate" hreflang="en" href="Diagnostics.html?Set-Language=en" /> 
<link class="carryLanguage" rel="home" href="http://ons.snirh.gob.pe/Peru/maproom/" title="ONS" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="../../maproom//navmenu.json" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy"
href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#diagnostics" />
<link rel="term:icon" href="New/elnino-lalnina-conditions_maproom.jpg" />
<script type="text/javascript" src="../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../maproom/unesco.js"></script>
<style>
    #nodisplay {
      display: none;
    }
  </style>
</head>
<body xml:lang="es">

<form name="pageform" id="pageform">
<input class="carryLanguage carryup" name="Set-Language" type="hidden" />
<input class="carryup titleLink itemImage" name="bbox" type="hidden" />
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>Peru</legend> 
                    <a rev="section" class="navlink carryup" href="index.html">ENSO</a>
            </fieldset> 
           <fieldset class="navitem" id="chooseSection">
                           <legend>Peru</legend>
             </fieldset>
</div>
<div>
    <span id="nodisplay">
        <h2 property="term:title">Monitoreo de ENSO</h2>
<p align="left" property="term:description">Esta secci&#243;n contiene mapas, series de tiempo y otros análisis &#250;tiles para el monitoreo de ENSO y para identificar la presencia de un cambio hacia El Niño o La Niña. </p>
</span>
<iframe name="portaldl" width="100%" height="800px" src="http://iridl.ldeo.columbia.edu/maproom/ENSO/Diagnostics.html">
</iframe>
</div>
<div class="optionsBar">
<fieldset class="navitem" id="share"><legend>Compartir</legend>
</fieldset>
<fieldset class="navitem langgroup" id="cont&#225;ctanos"></fieldset>
</div>
 </body>

 </html>
