<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Componente Hidroenergético</title>
<link rel="stylesheet" type="text/css" href="../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="index.html?Set-Language=en" /> 
<link rel="canonical" href="HidrologiaPotElec.html" />
<link class="carryLanguage" rel="home" href="http://ons.snirh.gob.pe/Peru/maproom/" title="ONS" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="../../maproom//navmenu.json" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Electrico_term"/>
<link rel="term:icon" href="coes.JPG" />
<script type="text/javascript" src="../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../maproom/unesco.js"></script>
</head>
<body xml:lang="es">

<form name="pageform" id="pageform">
<input class="carry carryup" name="Set-Language" type="hidden" />
<input class="carry carryup" name="bbox" type="hidden" />
</form>
<div class="controlBar">

           <fieldset class="navitem" id="toSectionList">
                           <legend>Peru</legend>
                                               <a rev="section" class="navlink carryup" href="index.html">Componente Hidroenergético</a>
                                                           </fieldset>
                                                                      <fieldset class="navitem" id="chooseSection">
                                                                                                 <legend>Peru</legend>
                                                            </fieldset>

            
</div>
<div>
 <div id="content" class="searchDescription">
<h2 property="term:title">Componente Hidroenergético</h2>
<p align="left" property="term:description">
	Esta sección contiene vínculos relacionados con el Potencial Eléctrico e Hidrología proveído por el COES.
</p> 


</div>

<div class="rightcol">
            <div class="ui-tabs">
               <ul class="ui-tabs-nav"></ul>
               <div class="ui-tabs-panel">
                  	<div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="http://www.coes.org.pe/portal/portalinformacion/generacion/" shape="rect">Energía Producida</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="http://http://www.coes.org.pe/portal/portalinformacion/generacion/" shape="rect"><img class="itemImage" src="EnergiaProducida.JPG" /></a></div>
                     <div class="itemDescription">Muestra un enlace de los datos de energía producida por Empresa, proveído por COES. </div>
                     <div class="itemFooter"></div>
					</div>
					<div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="http://www.coes.org.pe/portal/portalinformacion/hidrologia/">Indicadores hidrológicos</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="http://www.coes.org.pe/portal/portalinformacion/hidrologia/"><img class="itemImage" src="coes.JPG" /></a></div>
                     <div class="itemDescription">Muestra un enlace de los datos de volúmen y nivel utilizados para la producción de energía, proveídos por el COES. </div>
                     <div class="itemFooter"></div>
					</div>
               </div>
            </div>
</div>
         



</div>

<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend>

</fieldset>
<fieldset class="navitem langgroup" id="cant&#225;ctanos"></fieldset>
</div>
 </body>

 </html>
