<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#" xmlns:og="http://ogp.me/ns#" 
      version="XHTML+RDFa 1.0"
      >
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta xml:lang="es" property="maproom:Entry_Id" content="Forecasts_Seasonal" />
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
      <title>Pron&#243;sticos Clim&#225;ticos</title>
      <link rel="stylesheet" type="text/css" href="../../unesco.css" />
      <link class="altLanguage" rel="alternate" hreflang="en" href="SENAMHI_Forecast_Precip.html?Set-Language=en" />
      <link class="share" rel="canonical" href="SENAMHI_Forecast_Precip.html" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Forecasts_Seasonal_term"/>
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
      <link rel="term:icon" href="http://ons.snirh.gob.pe/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.SENAMHI/.Pronosticos/.Precipitacion/lon/lat/2/copy/SOURCES/.Peru/.SENAMHI/.Pronosticos/.Precipitacion/%7B/Bajo_lo_Normal/Normal/Sobre_lo_Normal%7Dgrouptogrid//name//forecast/def//long_name/%28Pronostico%29/def/%5BM%5Ddominant_class/startcolormap/DATA/1/3/RANGE/transparent/yellow/yellow/grey/green/green/endcolormap/6/-1/roll/pop/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/24/psdef//T/last/plotvalue//Y/-19/0/plotrange+//plotborder+0+psdef//plotaxislength+432+psdef//fntsze+24+psdef//antialias+true+psdef//color_smoothing+1+psdef+.gif" /><script type="text/javascript" src="../../../uicore/uicore.js" xml:space="preserve"></script><script type="text/javascript" src="../../../maproom/unesco.js" xml:space="preserve"></script></head>
   <body xml:lang="es">
      
      
      
      <form name="pageform" id="pageform" class="carryup carry dlimg dlauximg share" method="get" enctype="application/x-www-form-urlencoded">
         <input class="carryLanguage carryup carry " name="Set-Language" type="hidden" />
         <input class="dlimg dlimgloc admin maptable share" name="bbox" type="hidden" />
         <input class="dlimg share" name="T" type="hidden" />
         <input class="dlimg" name="plotaxislength" type="hidden" />
         <input class="share dlimgts dlimgloc dlimglocclick station" name="region" type="hidden" />
         <input class="transformRegion dlimglocclick" name="clickpt" type="hidden" />
         <input class="dlimg dlimgts dlauximg maptable share" name="anal" type="hidden" />
         <input class="pickarea dlimgts admin" name="resolution" type="hidden" value="irids:SOURCES:Peru:SENAMHI:Pronosticos:Precipitacion:ds" />
         

         
         
      </form>
      
      
      
      <div class="controlBar">
         
         
         
         <fieldset class="navitem" id="toSectionList"> 
            
            
            
            <legend>Maproom</legend> 
            <a rev="section" class="navlink carryup" href="/maproom/Forecasts/" shape="rect">Predicciones</a>
            
            
            
         </fieldset> 
         
         
         
         <fieldset class="navitem" id="chooseSection"> 
            
            
            
            <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Forecasts_Seasonal_term"><span property="term:label">Pronóstico Estacional</span></legend>
            
            
            
         </fieldset> 
         
         
         
         <fieldset class="navitem">
            
            
            
            <legend>Región</legend>
            <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsPeru.json" shape="rect"></a>
            <select class="RegionMenu" name="bbox">
               <option value="">Peru</option>
               <optgroup class="template" label="Región">
                  <option class="irigaz:hasPart irigaz:id@value term:label"></option>
               </optgroup></select>
            
            
            
         </fieldset>
         
         
         
            <fieldset class="navitem">     
            
            <legend>Selección de Estación</legend>	
            
            
            <link class="admin" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/(irids%3ASOURCES%3APeru%3ASENAMHI%3APronosticos%3APrecipitacion%3Ads)//resolution/parameter/geoobject/(bb%3A-82%3A-19%3A-65%3A0%3Abb)//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
            <select class="pageformcopy" name="region">
               
               <optgroup class="template" label="Label">
                  
                  <option class="iridl:values region@value label"></option>
                  
               </optgroup></select>
            
          </fieldset>
         
         
         
         
      </div>
      
      
      
      <div class="ui-tabs">
         
         
         
         <ul class="ui-tabs-nav">
            <li><a href="#tabs-1" shape="rect">Descripción</a></li>
            <li><a href="#tabs-2" shape="rect">Más información</a></li>
            <li><a href="#tabs-3" shape="rect">Fuente</a></li>
            <li><a href="#tabs-4" shape="rect">Soporte</a></li>
            <li><a href="#tabs-5" shape="rect">Instrucciones</a></li>
         </ul>
         
         
         
         
         
         <fieldset class="regionwithinbbox dlimage" about="">
            		
            <a class="dlimgts" rel="iridl:hasTable" href="http://ons.snirh.gob.pe/SOURCES/.Peru/.SENAMHI/.Pronosticos/Precipitacion/Sobre_lo_Normal/Normal/Bajo_lo_Normal/4/-1/roll/pop/3/array/astore/%7Bnombre/(irids%3ASOURCES%3APeru%3ASENAMHI%3APronosticos%3ATemperatura%3AMaxima%3Anombre%40COTAHUASI%3Ads)//region/parameter/geoobject/.nombre/.first/VALUE%7Dforall/T/4/-3/roll/table%3A/4/%3Atable/" shape="rect"></a>
            
            
            
            
            
            <div style="float: left;">
               <img class="dlimgloc" src="http://ons.snirh.gob.pe/SOURCES/.WORLDBATH/.bath/X/Y/%28bb:-82%2C-19%2C-65%2C0%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28pt:-75:-5:pt%29//region/parameter/geoobject/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/geometrypushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0.1/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif" />
               
               
               
               
               
            </div>
            
            
            
            
            
            
            <div style="float: left;">
               
               
               
               
               
               
               <div class="valid" style="text-align: top;">
                  <a class="station" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/%28pt:-75:-5:pt%29//region/parameter/geoobject/info.json" shape="rect"></a>
                  
                  
                  
                  
                  
                  <div class="template" style="color : black">
                     Estación <b><span class="iridl:long_name"></span></b>
                     
                     
                     
                     
                     
                  </div>
                  
                  
                  
                  
                  
               </div>
               
               
               
               
               
               
               <div class="valid" style="text-align: top;">
                  <a class="station" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/SOURCES/.Peru/.SENAMHI/.Pronosticos/Precipitacion/nombre/(irids%3ASOURCES%3APeru%3ASENAMHI%3APronosticos%3ATemperatura%3AMaxima%3Anombre%40ABANCAY%3Ads)//region/parameter/geoobject/.nombre/.first/VALUE/lon/lat/2/ds/info.json" shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"
}
}
}
</script>
                  <div>
                     
                     
                     
                     
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="name " rowspan="1" colspan="1"></td>
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                        </tr>
                     </table>
                     
                     
                     
                     
                     
                  </div>
                  
                  
                  
                  
                  
                  
               </div>
               
               
               
               
               
                  <div>
                     <a class="dlimgloc" rel="iridl:hasJSON"
                        href="http://ons.snirh.gob.pe/SOURCES/.Peru/.SENAMHI/.Pronosticos/.Precipitacion/.Bajo_lo_Normal/nombre/%28irids:SOURCES:Peru:SENAMHI:Pronosticos:Precipitacion:nombre%40BAMBAMARCA:ds%29//region/parameter/geoobject/.nombre/.first/VALUE/T/last/VALUE/info.json"
                        shape="rect"></a>
                     <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:values": {
 "td.time+": "var.T"
}
}
}
</script>
                     <div>           
                        
                        <table class="valid template">
                           <tr style="color : black">
                              <b><td class="time " rowspan="1" colspan="1">El pron&#243;stico realizado en el mes de </td> </b>
                           </tr>
                        </table>
                        
                     </div>                  
                     
                  </div>
               
               
               
               
               
               <div class="valid" style="text-align: top;">
                  <a class="dlimgloc" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/SOURCES/.Peru/.SENAMHI/.Pronosticos/.Precipitacion/.Bajo_lo_Normal/nombre/%28irids:SOURCES:Peru:SENAMHI:Pronosticos:Precipitacion:nombre%40BAMBAMARCA:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Probabilidad%20de%20Precipitacion%20Bajo%20lo%20Normal:%29def/T/last/VALUE/toi4/SOURCES/.Peru/.SENAMHI/.Pronosticos/.Precipitacion/.Normal/nombre/%28irids:SOURCES:Peru:SENAMHI:Pronosticos:Precipitacion:nombre%40BAMBAMARCA:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Probabilidad%20de%20Precipitacion%20Normal:%29def/T/last/VALUE/toi4/SOURCES/.Peru/.SENAMHI/.Pronosticos/.Precipitacion/.Sobre_lo_Normal/nombre/%28irids:SOURCES:Peru:SENAMHI:Pronosticos:Precipitacion:nombre%40BAMBAMARCA:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Probabilidad%20de%20Precipitacion%20Sobre%20lo%20Normal:%29def/T/last/VALUE/toi4/3/ds/info.json" shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"

}
}
}
</script>
                  <div>
                     
                     
                     
                     
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="name " rowspan="1" colspan="1"></td>
                           <td align="right" class="value " rowspan="1" colspan="1">%</td>
                        </tr>
                     </table>
                     
                     
                     
                     
                     
                  </div>
                  
                  
                  
                  
                  
                  
               </div>
               
               
               
               
               
               
            </div>
            
            <br clear="none" />
            
            
            <div class="dlimgtsbox">
               <img class="dlimgts regionwithinbbox" rel="iridl:hasFigureImage" src="http://ons.snirh.gob.pe/SOURCES/.Peru/.SENAMHI/.Pronosticos/.Precipitacion/nombre/%28irids:SOURCES:Peru:SENAMHI:Pronosticos:Precipitacion:nombre%40BAMBAMARCA:ds%29//region/parameter/geoobject/.nombre/.first/VALUE/%7BBajo_lo_Normal/Normal/Sobre_lo_Normal%7Dgrouptogrid//name//forecast/def//long_name/%28Pron%C3%B3stico%29/def/dup/%5BM/%5Dmaxover/DATA/0/100/RANGE//units//percent/def/exch/%5BM%5Ddominant_class/startcolormap/DATA/1/3/RANGE/transparent/yellow/yellow/grey/green/green/endcolormap/T/fig-/colorbars2/-fig/nombre/last/plotvalue/+.gif" />
          
            </div>
            <br clear="none" />
        
            
            <div class="dlimgtsbox">
               <img class="dlimgts regionwithinbbox" rel="iridl:hasFigureImage" src="http://ons.snirh.gob.pe/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.SENAMHI/.Pronosticos/.Precipitacion/lon/lat/2/copy/SOURCES/.Peru/.SENAMHI/.Pronosticos/.Precipitacion/%7B/Bajo_lo_Normal/Normal/Sobre_lo_Normal%7Dgrouptogrid//name//forecast/def//long_name/%28Pron%C3%B3stico%29/def/%5BM%5Ddominant_class/startcolormap/DATA/1/3/RANGE/transparent/yellow/yellow/grey/green/green/endcolormap/6/-1/roll/pop/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//T/last/plotvalue//Y/-19/0/plotrange/.auxfig/+.gif" />
            </div>


            
         </fieldset>
         
         
         
         
         
         <fieldset class="dlimage" id="content" about="">
            <link rel="iridl:hasFigure" href="http://ons.snirh.gob.pe/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.SENAMHI/.Pronosticos/.Precipitacion/lon/lat/2/copy/SOURCES/.Peru/.SENAMHI/.Pronosticos/.Precipitacion/%7B/Bajo_lo_Normal/Normal/Sobre_lo_Normal%7Dgrouptogrid//name//forecast/def//long_name/%28Pronostico%29/def/%5BM%5Ddominant_class/startcolormap/DATA/1/3/RANGE/transparent/yellow/yellow/grey/green/green/endcolormap/6/-1/roll/pop/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/24/psdef//T/last/plotvalue//Y/-19/0/plotrange//plotborder/0/psdef//plotaxislength/432/psdef//fntsze/24/psdef//antialias/true/psdef//color_smoothing/1/psdef/" />
            <img class="dlimg" src="http://ons.snirh.gob.pe/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.SENAMHI/.Pronosticos/.Precipitacion/lon/lat/2/copy/SOURCES/.Peru/.SENAMHI/.Pronosticos/.Precipitacion/%7B/Bajo_lo_Normal/Normal/Sobre_lo_Normal%7Dgrouptogrid//name//forecast/def//long_name/%28Pronostico%29/def/%5BM%5Ddominant_class/startcolormap/DATA/1/3/RANGE/transparent/yellow/yellow/grey/green/green/endcolormap/6/-1/roll/pop/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/24/psdef//T/last/plotvalue//Y/-19/0/plotrange//plotborder/0/psdef//plotaxislength/432/psdef//fntsze/24/psdef//antialias/true/psdef//color_smoothing/1/psdef/+.gif" border="0" alt="image" /><br clear="none" />
            <img class="dlauximg" src="http://ons.snirh.gob.pe/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.SENAMHI/.Pronosticos/.Precipitacion/lon/lat/2/copy/SOURCES/.Peru/.SENAMHI/.Pronosticos/.Precipitacion/%7B/Bajo_lo_Normal/Normal/Sobre_lo_Normal%7Dgrouptogrid//name//forecast/def//long_name/%28Pron%C3%B3stico%29/def/%5BM%5Ddominant_class/startcolormap/DATA/1/3/RANGE/transparent/yellow/yellow/grey/green/green/endcolormap/6/-1/roll/pop/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//T/last/plotvalue//Y/-19/0/plotrange/.auxfig/+.gif" />
         </fieldset>
         
         
         
         
         <div id="tabs-1" class="ui-tabs-panel" about="">
            
            
            
            <h3 align="center" property="term:title">Pron&#243;stico de la Precipitaci&#243;n</h3>
            
            
            
            <p align="left" property="term:description">Este mapa muestra el pron&#243;stico de la precipitaci&#243;n para los pr&#243;ximos tres meses para las estaciones meteorológicas del Servicio
               Nacional de Meterología e Hidrología del Perú  (SENAMHI).               
            </p>
            
            <p align="left">El mapa muestra las estaciones y la condici&#243;n m&#225;s probable (una precipitaci&#243;n por debajo de lo normal, normal o sobre lo normal) para los pr&#243;ximos tres meses.
             
            </p>

            <p align="left">Seleccione la estaci&#243;n de inter&#233;s para visualizar la evoluci&#243;n del pron&#243;stico en el tiempo.
            Utiliza el navegador de tiempo para consultar el mes de interés.
             
            </p>

            
            
            
             <p align="left"><b>Fuente de Datos</b> </p>
            
            
            
            <p align="left">Los datos usados para el pron&#243;stico provienen de estaciones meteorológicas del Servicio
               Nacional de Meterología e Hidrología del Perú  (SENAMHI).
            </p>
            <p align="left"><a href="http://www.peruclima.pe/?p=pronosticos-climaticos" shape="rect"><img src="../../icons/Logo_Senamhi.jpg" alt="Logo SENAMHI" /></a></p>

            <p align="left"><b>Procesamiento</b> </p>            
            <p align="left">La integraci&#243;n de los pron&#243;sticos en el Observatorio se realiz&#243; con apoyo del Centro del Agua para Zonas &#193;ridas en Am&#233;rica Latina y el Caribe (CAZALAC).</p> 
            <p align="left">  <img src="../../icons/Logo_cazalac" alt="Logo CAZALAC" /></p>
            
         </div>
         
         
         
         
         <div id="tabs-2" class="ui-tabs-panel">
            
            
            
            <h3 align="center">¿Cómo se calcula el pron&#243;tico?</h3>
            
            
            <p>El pron&#243;tico se obtiene a trav&#233;s de un modelo estad&#237;stico que relaciona las precipitaciones en Per&#250; con la temperatura superficial del mar en el Pac&#237;fico ecuatorial para el per&#237;odo 1981-2012. 
               
            </p>
            
            

            
            <p>Para tener más información sobre los pron&#243;ticos del SENAMHI, se puede ingresar a la pagina web del <a href="http://www.peruclima.pe/?p=pronosticos-climaticos" shape="rect"><img src="../../icons/Logo_Senamhi.jpg" alt="Logo SENAMHI" /></a>
            </p>			
            
            
            
         </div>
         
         
         
         
         <div id="tabs-3" class="ui-tabs-panel">
            
            
            
            <h3 align="center">Fuente de los Datos</h3>
            
            
            
            <p align="left"><a href="http://www.peruclima.pe/?p=pronosticos-climaticos" shape="rect">Pronosticos climaticos</a>, realizado por SENAMHI <a href="http://senamhi.gob.pe/" shape="rect">(SENAMHI)</a>
               
               
               
            </p>
            
            
            
         </div>
         
         
         
         
         <div id="tabs-4" class="ui-tabs-panel">
            
            
            
            <h3 align="center">Soporte</h3>
            
            
            
            <p>
               Sus consultas a: <a href="mailto:snirh@ana.gob.pe?subject=Pronostico Peru" shape="rect">snirh@ana.gob.pe</a>
               
               
               
            </p>
            
            
            
         </div>
         
         
         
         <div id="tabs-5" class="ui-tabs-panel">
            
            
            
            <h3 align="center">Instrucciones</h3>
            
            
            
            <div class="buttonInstructions"></div>
            
            
            
         </div>
         
         
         
      </div>
      
      
      
      <div class="optionsBar">
         
         
         
         <fieldset class="navitem" id="share">
            
            
            <legend>Share</legend>
            
            
         </fieldset>
         
         
         
      </div>
      
      
      
   </body>
</html>