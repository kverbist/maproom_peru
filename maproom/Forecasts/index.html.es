<?xml version="1.0" encoding="utf-8"?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:wms="http://www.opengis.net/wms#" xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#" xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#" xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#" xmlns:xs="http://www.w3.org/2001/XMLSchema#" xmlns:og="http://ogp.me/ns#" version="XHTML+RDFa 1.0" xml:lang="es">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="description" xml:lang="es" content="Pueden ser instrumentos relevantes para preparar riesgos climáticos futuros. En este 'maproom' se visualizan los pronósticos de modelos internacionales y pronósticos con mayor detalle local." />
      <meta name="twitter:card" content="summary_large_image" />
      <meta property="og:title" content="Pronósticos Estacionales" />
      <meta property="og:description" content="Pueden ser instrumentos relevantes para preparar riesgos climáticos futuros. En este 'maproom' se visualizan los pronósticos de modelos internacionales y pronósticos con mayor detalle local." />
      <meta property="og:image" content="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/Y/-85/85/RANGE/a:/.dominant/:a:/.target_date/:a/X/Y/fig-/colors/plotlabel/black/thin/coasts_gaz/thin/countries_gaz/-fig//L/1.0/plotvalue//F/last/plotvalue//antialias/true/psdef//framelabel/%28Forecast%20for%20%25=%5Btarget_date%5D%29/psdef//plotaxislength/590/psdef//plotborderbottom/40/psdef//plotbordertop/40/psdef//L/1.0/plotvalue//F/last/plotvalue/X/-82.0/-68.0/plotrange/Y/-18.3/0/plotrange//framelabelstart/%28%25=%5Btarget_date%5D%20Seasonal%20Forecast%20issued%20%25=%5BF%5D%29/psdef//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/590/psdef//L/1.0/plotvalue//F/last/plotvalue//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/590/psdef+//plotaxislength+432+psdef//plotborder+0+psdef//antialias+true+psdef//color_smoothing+1+psdef//framelabel+%28Forecast%20for%20%25=%5Btarget_date%5D%29+psdef//framelabelstart+%28%25=%5Btarget_date%5D%20Seasonal%20Forecast%20issued%20%25=%5BF%5D%29+psdef+.gif" />
      <meta name="viewport" content="width=device-width; initial-scale=1.0;" />
      <title>Pronósticos</title>
      <link rel="stylesheet" type="text/css" href="../unesco.css" />
      <link class="altLanguage" rel="alternate" hreflang="en" href="index.html?Set-Language=en" />
      <link rel="canonical" href="index.html" />
      <meta property="maproom:Sort_Id" content="a04" />
      <link class="" rel="home" href="http://www.minagri.gob.cl/agroclimatico/unidad_nacional.php/" title="UNEA" />
      <link class="carryLanguage" rel="home alternate" type="application/json" href="../../maproom//navmenu.json" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#forecast" />
      <link rel="term:icon" href="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/Y/-85/85/RANGE/a:/.dominant/:a:/.target_date/:a/X/Y/fig-/colors/plotlabel/black/thin/coasts_gaz/thin/countries_gaz/-fig//L/1.0/plotvalue//F/last/plotvalue//antialias/true/psdef//framelabel/%28Forecast%20for%20%25=%5Btarget_date%5D%29/psdef//plotaxislength/590/psdef//plotborderbottom/40/psdef//plotbordertop/40/psdef//L/1.0/plotvalue//F/last/plotvalue/X/-82.0/-68.0/plotrange/Y/-18.3/0/plotrange//framelabelstart/%28%25=%5Btarget_date%5D%20Seasonal%20Forecast%20issued%20%25=%5BF%5D%29/psdef//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/590/psdef//L/1.0/plotvalue//F/last/plotvalue//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/590/psdef+//plotaxislength+432+psdef//plotborder+0+psdef//antialias+true+psdef//color_smoothing+1+psdef//framelabel+%28Forecast%20for%20%25=%5Btarget_date%5D%29+psdef//framelabelstart+%28%25=%5Btarget_date%5D%20Seasonal%20Forecast%20issued%20%25=%5BF%5D%29+psdef+.gif" /><script type="text/javascript" src="../../uicore/uicore.js" xml:space="preserve"></script><script type="text/javascript" src="../../maproom/unesco.js" xml:space="preserve"></script></head>
   <body>
      
      
      <form name="pageform" id="pageform" method="get" enctype="application/x-www-form-urlencoded">
         <input class="carryLanguage carryup" name="Set-Language" type="hidden" />
         
      </form>
      
      <div class="controlBar">
         
         <fieldset class="navitem"> 
            
            <legend>Peru</legend> 
            <a rev="section" class="navlink carryup" href="/maproom/" shape="rect">Observatorio Nacional de Sequías</a>
            
         </fieldset> 
         
         <fieldset class="navitem"> 
            
            <legend>Peru</legend> 
            <span class="navtext">Predicciones</span>
            
         </fieldset> 
         
      </div>
      
      <div>
         
         <div id="content" class="searchDescription">
            
            <h2 property="term:title">Pronósticos</h2>
            
            <p align="left" property="term:description">
               Pueden ser instrumentos relevantes para preparar riesgos climáticos futuros. En este
               'maproom' se visualizan los pronósticos de modelos internacionales y pronósticos con
               mayor detalle local.
               
            </p>
            
            <p>Los pronósticos Estacionales fueron realizados usando el IRI Climate Prediction Tool (CPT), propuesto
               por IRI-Columbia.
            </p>
            
			<p>
			AMICAF es un proyecto que la Organización de las Naciones Unidas para la Alimentación y la Agricultura (FAO) ejecutó junto con el Ministerio de Agricultura y Riego del Perú. El proyecto es financiado por el Ministerio de Agricultura, Silvicultura y Pesca de Japón.
			</p>
         </div>
         
         <div class="rightcol">
            <div class="ui-tabs">
               <ul class="ui-tabs-nav">
                  <li><a href="#tabs-1">Pronóstico Estacional</a></li>
				  <li><a href="#tabs-2">AMICAF</a></li>
               </ul>
               <div id="tabs-1" class="ui-tabs-panel">
                  <div class="itemGroup">Pronóstico Estacional</div>
                  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="Seasonal/IRI_Forecast.html">Pronóstico Estacional Global (IRI)</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="Seasonal/IRI_Forecast.html"><img class="itemImage" src="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/Y/-85/85/RANGE/a:/.dominant/:a:/.target_date/:a/X/Y/fig-/colors/plotlabel/black/thin/coasts_gaz/thin/countries_gaz/-fig//L/1.0/plotvalue//F/last/plotvalue//antialias/true/psdef//framelabel/%28Forecast%20for%20%25=%5Btarget_date%5D%29/psdef//plotaxislength/590/psdef//plotborderbottom/40/psdef//plotbordertop/40/psdef//L/1.0/plotvalue//F/last/plotvalue/X/-82.0/-68.0/plotrange/Y/-18.3/0/plotrange//framelabelstart/%28%25=%5Btarget_date%5D%20Seasonal%20Forecast%20issued%20%25=%5BF%5D%29/psdef//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/590/psdef//L/1.0/plotvalue//F/last/plotvalue//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/590/psdef+//plotaxislength+432+psdef//plotborder+0+psdef//antialias+true+psdef//color_smoothing+1+psdef//framelabel+%28Forecast%20for%20%25=%5Btarget_date%5D%29+psdef//framelabelstart+%28%25=%5Btarget_date%5D%20Seasonal%20Forecast%20issued%20%25=%5BF%5D%29+psdef+.gif" /></a></div>
                     <div class="itemDescription">Este mapa muestra el pronóstico estacional para precipitaciones y temperaturas en
                        Perú, e indica el escenario más probable para la próxima temporada, expresado como
                        más alto, normal y más bajo que lo normal.
                     </div>
                     <div class="itemFooter"></div>
                  </div>
                  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="Seasonal/SENAMHI_Forecast_Precip.html">Pronóstico de la Precipitación</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="Seasonal/SENAMHI_Forecast_Precip.html"><img class="itemImage" src="http://ons.snirh.gob.pe/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.SENAMHI/.Pronosticos/.Precipitacion/lon/lat/2/copy/SOURCES/.Peru/.SENAMHI/.Pronosticos/.Precipitacion/%7B/Bajo_lo_Normal/Normal/Sobre_lo_Normal%7Dgrouptogrid//name//forecast/def//long_name/%28Pronostico%29/def/%5BM%5Ddominant_class/startcolormap/DATA/1/3/RANGE/transparent/yellow/yellow/grey/green/green/endcolormap/6/-1/roll/pop/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/24/psdef//T/last/plotvalue//Y/-19/0/plotrange+//plotborder+0+psdef//plotaxislength+432+psdef//fntsze+24+psdef//antialias+true+psdef//color_smoothing+1+psdef+.gif" /></a></div>
                     <div class="itemDescription">Este mapa muestra el pronóstico de la precipitación para los próximos tres meses para
                        las estaciones meteorológicas del Servicio Nacional de Meterología e Hidrología del
                        Perú (SENAMHI).
                     </div>
                     <div class="itemFooter"></div>
                  </div>
                  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="Seasonal/SENAMHI_Forecast_Tmax.html">Pronóstico de la Temperatura Máxima</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="Seasonal/SENAMHI_Forecast_Tmax.html"><img class="itemImage" src="http://ons.snirh.gob.pe/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.SENAMHI/.Pronosticos/.Temperatura/.Maxima/lon/lat/2/copy/SOURCES/.Peru/.SENAMHI/.Pronosticos/.Temperatura/.Maxima/%7B/Bajo_lo_Normal/Normal/Sobre_lo_Normal%7Dgrouptogrid//name//forecast/def//long_name/%28Pronostico%29/def/%5BM%5Ddominant_class/startcolormap/DATA/1/3/RANGE/transparent/blue/blue/grey/red/red/endcolormap/6/-1/roll/pop/X/Y/fig-/colors/||/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/24/psdef//T/last/plotvalue//Y/-19/0/plotrange+//plotborder+0+psdef//plotaxislength+432+psdef//fntsze+24+psdef//antialias+true+psdef//color_smoothing+1+psdef+.gif" /></a></div>
                     <div class="itemDescription">Este mapa muestra el pronóstico de la temperatura máxima para los próximos tres meses
                        para las estaciones meteorológicas del Servicio Nacional de Meterología e Hidrología
                        del Perú (SENAMHI).
                     </div>
                     <div class="itemFooter"></div>
                  </div>
                  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="Seasonal/SENAMHI_Forecast_Tmin.html">Pronóstico de la Temperatura Mínima</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="Seasonal/SENAMHI_Forecast_Tmin.html"><img class="itemImage" src="http://ons.snirh.gob.pe/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.SENAMHI/.Pronosticos/.Temperatura/.Minima/lon/lat/2/copy/SOURCES/.Peru/.SENAMHI/.Pronosticos/.Temperatura/.Minima/%7B/Bajo_lo_Normal/Normal/Sobre_lo_Normal%7Dgrouptogrid//name//forecast/def//long_name/%28Pronostico%29/def/%5BM%5Ddominant_class/startcolormap/DATA/1/3/RANGE/transparent/blue/blue/grey/red/red/endcolormap/6/-1/roll/pop/X/Y/fig-/colors/||/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/24/psdef//T/last/plotvalue//Y/-19/0/plotrange+//plotborder+0+psdef//plotaxislength+432+psdef//fntsze+24+psdef//antialias+true+psdef//color_smoothing+1+psdef+.gif" /></a></div>
                     <div class="itemDescription">Este mapa muestra el pronóstico de la temperatura mínima para los próximos tres meses
                        para las estaciones meteorológicas del Servicio Nacional de Meterología e Hidrología
                        del Perú (SENAMHI).
                     </div>
                     <div class="itemFooter"></div>
                  </div>
               </div>
			   <div id="tabs-2" class="">
                  <div class="itemGroup">AMICAF</div>
                  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="AMICAF/Precipitation.html">Precipitación Modelada</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="AMICAF/Precipitation.html"><img class="itemImage" src="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.CAZALAC/.AMICAF/.Precipitation/.Historical/.CANESM2/lon/lat/2/copy/Daily/-20/masklt/DATA/0/AUTO/RANGE/6/-1/roll/pop//long_name/%28Precipitacion%20observada%29def//name/%28st_precip%29def/precip_colors/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//Y/-19/0/plotrange//st_precip/-3/3/plotrange/%28antialias%29true/psdef/%28fntsze%2920/psdef//antialias/true/psdef//fntsze/20/psdef//plotaxislength/432/psdef//color_smoothing/1/psdef//plotborder/0/psdef+.gif" /></a></div>
                     <div class="itemDescription">Este mapa muestra la precipitación diaria modelada, desarrollada en el proyecto AMICAF. 
                     </div>
                     <div class="itemFooter"></div>
                  </div>
				  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="AMICAF/Precipitation_historica.html">Precipitación Histórica</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="AMICAF/Precipitation_historica.html"><img class="itemImage" src="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.CAZALAC/.AMICAF/.Precipitation/.Historical/.CANESM2/lon/lat/2/copy/Daily/-20/masklt/DATA/0/AUTO/RANGE/6/-1/roll/pop//long_name/%28Precipitacion%20observada%29def//name/%28st_precip%29def/precip_colors/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//Y/-19/0/plotrange//st_precip/-3/3/plotrange/%28antialias%29true/psdef/%28fntsze%2920/psdef//antialias/true/psdef//fntsze/20/psdef//plotaxislength/432/psdef//color_smoothing/1/psdef//plotborder/0/psdef+.gif" /></a></div>
                     <div class="itemDescription">Este mapa muestra la precipitación diaria histórica, desarrollada en el proyecto AMICAF. 
                     </div>
                     <div class="itemFooter"></div>
                  </div>
				  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="AMICAF/Tmax_historica.html">Temperatura Máxima Histórica</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="AMICAF/Tmax_historica.html"><img class="itemImage" src="http://ons.snirh.gob.pe/expert/(CANESM2-RCP4.5)//anal/parameter/(CANESM2-RCP4.5)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.CAZALAC/.AMICAF/.Tmax/.RCP4.5/.CANESM2/lon/lat/2/copy/TmaxDaily//units/(Grado%20Celsius)/def/6/-1/roll/pop//long_name/(CANESM2-RCP4.5)/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/(CNRM-CM5-RCP4.5)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.CAZALAC/.AMICAF/.Tmax/.RCP4.5/.CNRM-CM5/lon/lat/2/copy/TmaxDaily//units/(Grado%20Celsius)/def/6/-1/roll/pop//long_name/(CNRM-CM5-RCP4.5)/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/(MPI-ESM-MR-RCP4.5)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.CAZALAC/.AMICAF/.Tmax/.RCP4.5/.MPI-ESM-MR/lon/lat/2/copy/TmaxDaily//units/(Grado%20Celsius)/def/6/-1/roll/pop//long_name/(MPI-ESM-MR-RCP4.5)/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/(CANESM2-RCP8.5)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.CAZALAC/.AMICAF/.Tmax/.RCP8.5/.CANESM2/lon/lat/2/copy/TmaxDaily//units/(Grado%20Celsius)/def/6/-1/roll/pop//long_name/(CANESM2-RCP8)/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/(CNRM-CM5-RCP8.5)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.CAZALAC/.AMICAF/.Tmax/.RCP8.5/.CNRM-CM5/lon/lat/2/copy/TmaxDaily//units/(Grado%20Celsius)/def/6/-1/roll/pop//long_name/(CNRM-CM5-RCP8.5)/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/(MPI-ESM-MR-RCP8.5)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.CAZALAC/.AMICAF/.Tmax/.RCP8.5/.MPI-ESM-MR/lon/lat/2/copy/TmaxDaily//units/(Grado%20Celsius)/def/6/-1/roll/pop//long_name/(MPI-ESM-MR-RCP8.5)/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef/+.gif" /></a></div>
                     <div class="itemDescription">Este mapa muestra la temperatura diaria máxima histórica, desarrollada en el proyecto AMICAF. 
                     </div>
                     <div class="itemFooter"></div>
                  </div>
				  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="AMICAF/Tmax.html">Temperatura Máxima Modelada</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="AMICAF/Tmax.html"><img class="itemImage" src="http://ons.snirh.gob.pe/expert/(CANESM2-RCP4.5)//anal/parameter/(CANESM2-RCP4.5)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.CAZALAC/.AMICAF/.Tmax/.RCP4.5/.CANESM2/lon/lat/2/copy/TmaxDaily//units/(Grado%20Celsius)/def/6/-1/roll/pop//long_name/(CANESM2-RCP4.5)/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/(CNRM-CM5-RCP4.5)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.CAZALAC/.AMICAF/.Tmax/.RCP4.5/.CNRM-CM5/lon/lat/2/copy/TmaxDaily//units/(Grado%20Celsius)/def/6/-1/roll/pop//long_name/(CNRM-CM5-RCP4.5)/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/(MPI-ESM-MR-RCP4.5)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.CAZALAC/.AMICAF/.Tmax/.RCP4.5/.MPI-ESM-MR/lon/lat/2/copy/TmaxDaily//units/(Grado%20Celsius)/def/6/-1/roll/pop//long_name/(MPI-ESM-MR-RCP4.5)/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/(CANESM2-RCP8.5)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.CAZALAC/.AMICAF/.Tmax/.RCP8.5/.CANESM2/lon/lat/2/copy/TmaxDaily//units/(Grado%20Celsius)/def/6/-1/roll/pop//long_name/(CANESM2-RCP8)/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/(CNRM-CM5-RCP8.5)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.CAZALAC/.AMICAF/.Tmax/.RCP8.5/.CNRM-CM5/lon/lat/2/copy/TmaxDaily//units/(Grado%20Celsius)/def/6/-1/roll/pop//long_name/(CNRM-CM5-RCP8.5)/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/(MPI-ESM-MR-RCP8.5)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.CAZALAC/.AMICAF/.Tmax/.RCP8.5/.MPI-ESM-MR/lon/lat/2/copy/TmaxDaily//units/(Grado%20Celsius)/def/6/-1/roll/pop//long_name/(MPI-ESM-MR-RCP8.5)/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef/+.gif" /></a></div>
                     <div class="itemDescription">Este mapa muestra la temperatura diaria máxima modelada, desarrollada en el proyecto AMICAF.
                     </div>
                     <div class="itemFooter"></div>
                  </div>
				  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="AMICAF/Tmin_historica.html">Temperatura Mínima Histórica</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="AMICAF/Tmin_historica.html"><img class="itemImage" src="http://ons.snirh.gob.pe/expert/(CANESM2)//anal/parameter/(CANESM2)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.CAZALAC/.AMICAF/.Tmin/.Historical/.CANESM2/lon/lat/2/copy/TminDaily//units/(%C2%B0C)/def/6/-1/roll/pop//long_name/(CANESM2)/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/(CNRM-CM5)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.CAZALAC/.AMICAF/.Tmin/.Historical/.CNRM-CM5/lon/lat/2/copy/TminDaily//units/(%C2%B0C)/def/6/-1/roll/pop//long_name/(CNRM-CM5)/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/(MPI-ESM-MR)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.CAZALAC/.AMICAF/.Tmin/.Historical/.MPI-ESM-MR/lon/lat/2/copy/TminDaily//units/(%C2%B0C)/def/6/-1/roll/pop//long_name/(MPI-ESM-MR)/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef/+.gif" /></a></div>
                     <div class="itemDescription">Este mapa muestra la temperatura diaria mínima histórica, desarrollada en el proyecto AMICAF.
                     </div>
                     <div class="itemFooter"></div>
                  </div>
				  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="AMICAF/Tmin.html">Temperatura Mínima Modelada</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="AMICAF/Tmin.html"><img class="itemImage" src="http://ons.snirh.gob.pe/expert/(CANESM2)//anal/parameter/(CANESM2)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.CAZALAC/.AMICAF/.Tmin/.Historical/.CANESM2/lon/lat/2/copy/TminDaily//units/(%C2%B0C)/def/6/-1/roll/pop//long_name/(CANESM2)/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/(CNRM-CM5)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.CAZALAC/.AMICAF/.Tmin/.Historical/.CNRM-CM5/lon/lat/2/copy/TminDaily//units/(%C2%B0C)/def/6/-1/roll/pop//long_name/(CNRM-CM5)/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/(MPI-ESM-MR)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.CAZALAC/.AMICAF/.Tmin/.Historical/.MPI-ESM-MR/lon/lat/2/copy/TminDaily//units/(%C2%B0C)/def/6/-1/roll/pop//long_name/(MPI-ESM-MR)/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef/+.gif" /></a></div>
                     <div class="itemDescription">Este mapa muestra la temperatura diaria mínima modelada, desarrollada en el proyecto AMICAF.
                     </div>
                     <div class="itemFooter"></div>
                  </div>
               </div>
            </div>
         </div>
         
         
      </div>
      
      <div class="optionsBar">
         
         <fieldset class="navitem" id="share">
            <legend>Compartir</legend>
            
            
         </fieldset>
         
      </div>
      
   </body>
</html>