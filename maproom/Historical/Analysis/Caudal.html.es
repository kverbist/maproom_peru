<?xml version="1.0" encoding="utf-8"?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:wms="http://www.opengis.net/wms#" xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#" xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#" xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#" xmlns:xs="http://www.w3.org/2001/XMLSchema#" xmlns:og="http://ogp.me/ns#" version="XHTML+RDFa 1.0">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="description" xml:lang="es" content="Este mapa muestra el caudal mensual histórico de las estaciones del Perú." />
      <meta name="twitter:card" content="summary_large_image" />
      <meta property="og:title" content="Caudales Históricos" />
      <meta property="og:description" content="Este mapa muestra el caudal mensual histórico de las estaciones del Perú." />
      <meta property="og:image" content="http://ons.snirh.gob.pe/expert/(Historico)//anal/parameter/(Historico)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Caudales/.Historico/lon/lat/2/copy/Caudal//units/(m3/s)/def/6/-1/roll/pop//long_name/(Caudal%20historico)/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/(IndiceCaudalesDifNorm)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Caudales/.Historico/lon/lat/2/copy/IndiceCaudalesDifNorm/6/-1/roll/pop//long_name/(Indice%20de%20Caudales%20de%20Diferencia%20Normalizada)/correlationcolorscale/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/(IndiceCaudalesEstan)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Caudales/.Historico/lon/lat/2/copy/IndiceCaudalesEstan/6/-1/roll/pop//long_name/(Indice%20de%20Caudal%20Estandarizado)/correlationcolorscale/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//Y/-19/0/plotrange%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef+.gif" />
      <meta name="viewport" content="width=device-width; initial-scale=1.0;" />
      <meta xml:lang="" property="maproom:Entry_Id" content="Historical" />
      <title>Caudales Histióricos</title>
      <link rel="stylesheet" type="text/css" href="../../unesco.css" />
	  <link class="altLanguage" rel="alternate" hreflang="en" href="Caudal.html?Set-Language=en" />
      <link class="share" rel="canonical" href="Caudal.html" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Historical_Analysis_term" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
      <link rel="term:icon" href="http://ons.snirh.gob.pe/expert/(Historico)//anal/parameter/(Historico)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Caudales/.Historico/lon/lat/2/copy/Caudal//units/(m3/s)/def/6/-1/roll/pop//long_name/(Caudal%20historico)/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/(IndiceCaudalesDifNorm)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Caudales/.Historico/lon/lat/2/copy/IndiceCaudalesDifNorm/6/-1/roll/pop//long_name/(Indice%20de%20Caudales%20de%20Diferencia%20Normalizada)/correlationcolorscale/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/(IndiceCaudalesEstan)/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Caudales/.Historico/lon/lat/2/copy/IndiceCaudalesEstan/6/-1/roll/pop//long_name/(Indice%20de%20Caudal%20Estandarizado)/correlationcolorscale/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//Y/-19/0/plotrange%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef+.gif" /><script type="text/javascript" src="../../../../uicore/uicore.js" xml:space="preserve"></script><script type="text/javascript" src="../../../maproom/unesco.js" xml:space="preserve"></script><style xml:space="preserve">
.dlimage.ver2 {
        width: 24%;
        float: right;
}
.dlimagei.ver3 {
        width: 99%;
}
.dlimage.ver4 {
        width: 33%;
}
.dlimgtsbox {
width: 99%;
display: inline-block
 }
</style></head>
   <body xml:lang="es">
      
      <form name="pageform" id="pageform" class="carryup carry dlimg dlauximg share" method="get" enctype="application/x-www-form-urlencoded">
         <input class="carryLanguage carryup carry " name="Set-Language" type="hidden" /> 
		 <input class="dlimg dlimgloc admin maptable share" name="bbox" type="hidden" />
		 <input class="dlimg share" name="T" type="hidden" />
		 <input class="dlimg" name="plotaxislength" type="hidden" />
		 <input class="share dlimgts dlimgloc dlimglocclick station" name="region" type="hidden" />
		 <input class="transformRegion dlimglocclick" name="clickpt" type="hidden" />
		 <input class="dlimg dlimgts dlauximg maptable share" name="anal" type="hidden" />		 
		 <!--input class="dlimg dlimgts dlauximg maptable share" name="anal" type="hidden" /-->		 
         <!--input class="share dlimgloc dlimgts" name="region" type="hidden" /-->
         <input class="pickarea dlimgts admin" name="resolution" type="hidden" value="irids:SOURCES:Peru:ANA:Caudales:Historico:ds" />
         
      </form>
      
      <div class="controlBar">
         
         <fieldset class="navitem" id="toSectionList">
            
            <legend>Maproom</legend> 
			<a rev="section" class="navlink carryup" href="/maproom/Historical/" shape="rect">Histórica</a>
            
         </fieldset>
         
         <fieldset class="navitem" id="chooseSection"> 
            
            <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Historical_Analysis_term"><span property="term:label">Analisis de Sequías Históricas</span></legend>
            
         </fieldset> 
         
         <fieldset class="navitem">
            
            <legend>Región</legend>
            <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsPeru.json" shape="rect"></a>
            <select class="RegionMenu" name="bbox">
               <option value="">Peru</option>
               <optgroup class="template" label="Región">
                  <option class="irigaz:hasPart irigaz:id@value term:label"></option>
               </optgroup></select>
            
         </fieldset>
         
         <fieldset class="navitem">
            
            <legend>Análisis</legend>
			   <span class="selectvalue"></span><select class="pageformcopy" name="anal">
               <option value="">Histórico</option>
               <option value="IndiceCaudalesDifNorm">ICDN</option>
               <option value="IndiceCaudalesEstan">ICE</option></select>
            
         </fieldset>
         
         <fieldset class="navitem">
            
            <legend>Selección de Estación</legend>	
            
            <link class="admin" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/%28irids:SOURCES:Peru:ANA:Caudales:Historico:ds%29//resolution/parameter/geoobject/%28bb:-82:-19:-65:0:bb%29//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
            <select class="pageformcopy" name="region">
               <optgroup class="template" label="Label">
                  <option class="iridl:values region@value label"></option>
               </optgroup></select>
            
         </fieldset>
         	
         
      </div>      
      
      <div class="ui-tabs">
         
         <ul class="ui-tabs-nav">
            <li><a href="#tabs-1" shape="rect">Descripción</a></li>
            <li><a href="#tabs-2" shape="rect">Más información</a></li>
            <li><a href="#tabs-3" shape="rect">Fuente</a></li>
            <li><a href="#tabs-4" shape="rect">Soporte</a></li>
            <li><a href="#tabs-5" shape="rect">Instrucciones</a></li>
         </ul>
         	
         
         <!--fieldset class="dlimage regionwithinbbox"-->
         <fieldset class="regionwithinbbox dlimage" about="">
            <a class="dlimgts" rel="iridl:hasTable" href="http://ons.snirh.gob.pe/expert/expert/SOURCES/.Peru/.ANA/.Caudales/.Historico/.Caudal//long_name/%28Caudal%20Historico%29def//units/%28m3/m%29def/SOURCES/.Peru/.ANA/.Caudales/.Historico/.IndiceCaudalesDifNorm//long_name/%28Indice%20de%20Caudales%20de%20Diferencia%20Normalizada%29def//units/%28-%29def/SOURCES/.Peru/.ANA/.Caudales/.Historico/.IndiceCaudalesEstan//long_name/%28Indice%20de%20Caudales%20Estandarizado%29def//units/%28-%29def/3/array/astore/%7Bnombre/%28irids:SOURCES:Peru:ANA:Caudales:Historico%40ARDILLA:ds%29//region/parameter/geoobject/.nombre/.first/VALUE%7Dforall//name/%28st_precip%29def/T/%28Feb%202000%295/-4/roll/4/-3/roll/table:/4/:table/" shape="rect"></a>
            
            <div style="float: left;">
               <img class="dlimgloc" src="http://ons.snirh.gob.pe/SOURCES/.WORLDBATH/.bath/X/Y/%28bb:-82%2C-19%2C-65%2C0%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28pt:-75:-5:pt%29//region/parameter/geoobject/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/geometrypushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0.1/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif" />
               
            </div>	
            
            
            <div style="float: left;">
               
               
               <!--div class="valid" style="display: inline-block; text-align: top;"-->
                <div class="valid" style="text-align: top;">
                  <a class="station" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/%28pt:-75:-5:pt%29//region/parameter/geoobject/info.json" shape="rect"></a>
                  
                  <div class="template" style="color : black"> 
				     Estacion SENAMHI <b><span class="iridl:long_name"></span></b>
                     
                  </div>
                  
                </div>
               
               
               
                <div class="valid" style="text-align: top;">
                  <!--a class="dlimgloc" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/SOURCES/.CAZALAC/.DroughtAtlas/.Peru/.MaxExpectedPrecip/%7B%7D%7B%28bb:-76%2C-8%2C-76.2%2C-8.2%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/toi4/name/cvntos/%28YR%29search/pop/nip/nip//long_name/exch/def%7Dforalldatasets2/%7Ba4/a2/a3/a1%7Dds/info.json" shape="rect"></a-->
				  <a class="station" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.ANA/.Caudales/.Historico/nombre/%28irids:SOURCES:Peru:ANA:Caudales:Historico%40RACARUMI:ds%29//region/parameter/geoobject/.nombre/.first/VALUE/lon/lat/2/ds/info.json" shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"
}
}
}
</script>
                  <div>
                     
                     <table class="valid template">
                        <tr style="color : black">
                           <!--td rowspan="1" colspan="1">Precipitación Máxima Esperada para un Periodo de Retorno de</td-->
                           <td class="name " rowspan="1" colspan="1"></td>
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                        </tr>
                     </table>
                     
                     
                  </div>
                  
                  
               </div>
			   
			   <div class="template"> <b>Observaciones para el mes actual: </b></div>
               
			   <div class="valid" style="text-align: top;">
                  <!--a class="dlimgloc" rel="iridl:hasJSON" href="http://iri.ana.gob.pe/expert/SOURCES/.Peru/.ANA/.Caudales/.Historico/.Caudal//T/%28Jan%202000%29VALUE/nombre/%28irids:SOURCES:Peru:ANA:Caudales:Historico%40RACARUMI:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Caudal%20Registrado%20%28m3/s%29:%20%29def/SOURCES/.Peru/.ANA/.Caudales/.Historico/.IndiceCaudalesDifNorm//T/%28Jan%202000%29VALUE/nombre/%28irids:SOURCES:Peru:ANA:Caudales:Historico%40RACARUMI:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Indice%20de%20Caudales%20de%20Dif.%20Normalizada:%20%29def/SOURCES/.Peru/.ANA/.Caudales/.Historico/.IndiceCaudalesEstan//T/%28Jan%202000%29VALUE/nombre/%28irids:SOURCES:Peru:ANA:Caudales:Historico%40RACARUMI:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Indice%20de%20Caudal%20Estandarizado:%20%29def/3/ds/info.json" shape="rect"></a-->
                  <a class="dlimgloc" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.ANA/.Caudales/.Historico/.Caudal/T/last/VALUE/nombre/%28irids:SOURCES:Peru:ANA:Caudales:Historico%40RACARUMI:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Caudal%20Registrado%20%28m3/s%29:%20%29def/SOURCES/.Peru/.ANA/.Caudales/.Historico/.IndiceCaudalesDifNorm/T/last/VALUE/nombre/%28irids:SOURCES:Peru:ANA:Caudales:Historico%40RACARUMI:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Indice%20de%20Caudales%20de%20Dif.%20Normalizada:%20%29def/SOURCES/.Peru/.ANA/.Caudales/.Historico/.IndiceCaudalesEstan/T/last/VALUE/nombre/%28irids:SOURCES:Peru:ANA:Caudales:Historico%40RACARUMI:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Indice%20de%20Caudal%20Estandarizado:%20%29def/3/ds/info.json" shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"

}
}
}
</script>
                  <div>
                     
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="name " rowspan="1" colspan="1"></td>
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                        </tr>
                     </table>
                     
                  </div>
                  
               </div>
			   
            </div>
            
            <br clear="none" />
            <!--img class="dlimgts regionwithinbbox" src="http://iri.ana.gob.pe/expert/SOURCES/.Peru/.SENAMHI/.Temperatura/.Mensual/.tmax/T/last/dup/18.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Peru:SENAMHI:Temperatura:Mensual%40LIRCAY:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Temperatura%20Maxima%29def//name/%28tmax%29def/pdsi_colorbars/DATA/0/AUTO/RANGE/dup/T/fig-/colorbars2/-fig/+//plotborder+72+psdef//plotaxislength+432+psdef+.gif" /-->


			<div class="dlimgtsbox">
				<img class="dlimgts regionwithinbbox" rel="iridl:hasFigureImage" src="http://ons.snirh.gob.pe/expert/(Historico)//anal/parameter/(Historico)/eq/%7BSOURCES/.Peru/.ANA/.Caudales/.Historico/.Caudal/nombre/(irids%3ASOURCES%3APeru%3AANA%3ACaudales%3AHistorico%40RACARUMI%3Ads)//region/parameter/geoobject/.nombre/.first/VALUE//long_name/(Caudal%20Registrado)/def/dup/T/fig-/colorbars2/thin/grey/dotted/200/200/3600/horizontallines/%7C/colorbars2/-fig%7Dif//anal/get_parameter/(IndiceCaudalesDifNorm)/eq/%7BSOURCES/.Peru/.ANA/.Caudales/.Historico/.IndiceCaudalesDifNorm/nombre/(irids%3ASOURCES%3APeru%3AANA%3ACaudales%3AHistorico%40RACARUMI%3Ads)//region/parameter/geoobject/.nombre/.first/VALUE//long_name/(Indice%20de%20Cudales%20de%20Dif.%20Normalizada)/def/correlationcolorscale/DATA/AUTO/AUTO/RANGE/dup/T/fig-/colorbars2/thin/grey/dotted/-0.8/0.2/0.8/horizontallines/%7C/colorbars2/-fig%7Dif//anal/get_parameter/(IndiceCaudalesEstan)/eq/%7BSOURCES/.Peru/.ANA/.Caudales/.Historico/.IndiceCaudalesEstan/nombre/(irids%3ASOURCES%3APeru%3AANA%3ACaudales%3AHistorico%40RACARUMI%3Ads)//region/parameter/geoobject/.nombre/.first/VALUE//long_name/(Indice%20de%20Caudal%20Estandarizado)/def/correlationcolorscale/DATA/AUTO/AUTO/RANGE/dup/T/fig-/colorbars2/thin/grey/dotted/-12/1/12/horizontallines/%7C/colorbars2/-fig%7Dif//plotborder/72/psdef//plotaxislength/432/psdef//plotborder/72/psdef//plotaxislength/432/psdef//plotborder/72/psdef//plotaxislength/432/psdef/nombre/last/plotvalue/+.gif" />
			
			
			
			</div>
			<br clear="none" />
			
			
			
			<div class="dlimgtsbox">
               <img class="dlimgts regionwithinbbox" rel="iridl:hasFigureImage" src="http://ons.snirh.gob.pe/expert/expert/SOURCES/.Peru/.ANA/.Caudales/.Historico/.Caudal/nombre/%28irids:SOURCES:Peru:ANA:Caudales:Historico%40RACACRUMI:ds%29//region/parameter/geoobject/.nombre/.first/VALUE/T/12/splitstreamgrid/dup%5BT2%5Daverage/exch%5BT2%5D0.05/0.5/0.95/0/replacebypercentile/a:/percentile/0.05/VALUE/percentile/removeGRID//fullname/%28Minimo%20Esperado%29def/:a:/percentile/0.5/VALUE/percentile/removeGRID//fullname/%28Normal%29def/:a:/percentile/0.95/VALUE/percentile/removeGRID//fullname/%28Maximo%20esperado%29def/:a/2/index/0.0/mul/dup/6/-1/roll/exch/6/-2/roll/5/index//long_name/%28Caudal%20Mensual%29def//units/%28m3/s%29def/SOURCES/.Peru/.ANA/.Caudales/.Historico/.Caudal/nombre/%28irids:SOURCES:Peru:ANA:Caudales:Historico%40RACACRUMI:ds%29//region/parameter/geoobject/.nombre/.first/VALUE/T/last/dup/18/sub/exch/RANGE//fullname/%28Caudal%20Observado%29def/DATA/0/AUTO/RANGE/6/5/roll/pop/7/-6/roll/6/array/astore/%7B%5BT%5DregridLinear%7Dforall/7/-1/roll/5/-4/roll/T/fig-/white/deltabars/grey/deltabars/solid/medium/green/line/blue/line/red/line/-fig//framelabel/%28Caudal%20Observado%20y%20Climatologia%29psdef//plotborderbottom/40/psdef//antialias/true/def/nombre/last/plotvalue/+.gif" />
               
               
               
            </div>
			
			
			
         </fieldset>
         
         	
         
         <fieldset class="dlimage" id="content" about="">            
			<a rel="iridl:hasTable" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.ANA/.Caudales/.Historico/location/%28bb:-82:-19:-65:0:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/%28Historico%29//anal/parameter/%28Historico%29eq/%7B.Caudal//long_name/%28Caudales%20Historicos%29def//long_name/%28Station%20Name%29def//units/%28m3/s%29def%7Dif//anal/get_parameter/%28IndiceCaudalesDifNorm%29eq/%7B.IndiceCaudalesDifNorm//long_name/%28Indice%20de%20Caudales%20de%20Diferencia%20Normalizada%29def//units/%28-%29def%7Dif//anal/get_parameter/%28IndiceCaudalesEstan%29eq/%7B.IndiceCaudalesEstan//long_name/%28Indice%20de%20Caudal%20Estandarizado%29def//units/%28-%29def%7Dif%5Bnombre%5DREORDER/mark/exch/T//full_name/%28Tiempo%29def/exch%5Bnombre%5Dtable:/mark/:table/" shape="rect"></a> 
			
            <link rel="iridl:hasFigure" href="http://ons.snirh.gob.pe/expert/%28Historico%29//anal/parameter/%28Historico%29/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Caudales/.Historico/lon/lat/2/copy/Caudal//units/%28m3/s%29/def/6/-1/roll/pop//long_name/%28Caudal%20historico%29/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/%28IndiceCaudalesDifNorm%29/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Caudales/.Historico/lon/lat/2/copy/IndiceCaudalesDifNorm/6/-1/roll/pop//long_name/%28Indice%20de%20Caudales%20de%20Diferencia%20Normalizada%29/def/correlationcolorscale/DATA/-1/1/RANGE/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/%28IndiceCaudalesEstan%29/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Caudales/.Historico/lon/lat/2/copy/IndiceCaudalesEstan/6/-1/roll/pop//long_name/%28Indice%20de%20Caudal%20Estandarizado%29/def/correlationcolorscale/DATA/-2/2/RANGE/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//Y/-19/0/plotrange%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef/" />
            <img class="dlimg" src="http://ons.snirh.gob.pe/expert/%28Historico%29//anal/parameter/%28Historico%29/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Caudales/.Historico/lon/lat/2/copy/Caudal//units/%28m3/s%29/def/6/-1/roll/pop//long_name/%28Caudal%20historico%29/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/%28IndiceCaudalesDifNorm%29/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Caudales/.Historico/lon/lat/2/copy/IndiceCaudalesDifNorm/6/-1/roll/pop//long_name/%28Indice%20de%20Caudales%20de%20Diferencia%20Normalizada%29/def/correlationcolorscale/DATA/-1/1/RANGE/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/%28IndiceCaudalesEstan%29/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Caudales/.Historico/lon/lat/2/copy/IndiceCaudalesEstan/6/-1/roll/pop//long_name/%28Indice%20de%20Caudal%20Estandarizado%29/def/correlationcolorscale/DATA/-2/2/RANGE/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//Y/-19/0/plotrange%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef/+.gif" border="0" alt="image" /><br clear="none" />
            <img class="dlauximg" src="http://ons.snirh.gob.pe/expert/%28Historico%29//anal/parameter/%28Historico%29/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Caudales/.Historico/lon/lat/2/copy/Caudal//units/%28m3/s%29/def/6/-1/roll/pop//long_name/%28Caudal%20historico%29/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/%28IndiceCaudalesDifNorm%29/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Caudales/.Historico/lon/lat/2/copy/IndiceCaudalesDifNorm/6/-1/roll/pop//long_name/%28Indice%20de%20Caudales%20de%20Diferencia%20Normalizada%29/def/correlationcolorscale/DATA/-1/1/RANGE/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/%28IndiceCaudalesEstan%29/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Caudales/.Historico/lon/lat/2/copy/IndiceCaudalesEstan/6/-1/roll/pop//long_name/%28Indice%20de%20Caudal%20Estandarizado%29/def/correlationcolorscale/DATA/-2/2/RANGE/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//Y/-19/0/plotrange%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef/+.auxfig/+.gif" />
            
         </fieldset>
         
         
         <div id="tabs-1" class="ui-tabs-panel" about="">
            
            <h3 align="center" property="term:title">Caudal Histórico</h3>
            
            <p align="left" property="term:description">Este  mapa muestra el caudal histórico registrado de las estaciones del Perú
               desde enero de 1914 (solo para la estacion RACARUMI) a abril del 2013 (solo para la estación CHOSICA).
            </p>
            
            <p align="left">Seleccione Indice de Caudales de Dif. Normalizada o Indice de Caudal Estandarizado en el menú&gt;<b>análisis</b> 
			   para visualizar el índice requerido. En menú&gt; <b>región</b> se puede seleccionar la región de interés. 
			   Utiliza el navegador de tiempo para consultar el mes de interés. Haga click en cualquier parte del mapa para tener
			   información más detallada de la estación más cercana al punto seleccionado.	
            </p>
            
            <p align="left"><b>Histórico:</b>
               Este mapa muestra los caudales históricos mensuales registrados en las diferentes estaciones del SENAMHI. 
			   Las observaciones tienen la unidad [m<sup>3</sup>/s].
            </p>
            
            <p align="left"><b>Índice de Caudales de Diferencia Normalizada (ICDN):</b>
               Este mapa muestra el Índice de Caudales de Diferencia Normalizada en cada mes. Este valor nos muestra
			   la variabilidad de los caudales respecto a su media histórica.
            </p> <!-- Seria bueno agregar una tabla con la que se interprete los valores-->
            
            <p align="left"><b>Índice de Caudal Estandarizado (ICE):</b>
               Este mapa muestra el Índice de Caudal Estandarizado en cada mes. Este valor representa el número de desviaciones 
			   estándar que cada registro de caudal se desvía del promedio histórico. Bajo este contexto, registros superiores 
			   al promedio histórico darán un ICE positivo, mientras que valores debajo del promedio histórico darán un ICE negativo.
            </p> <!-- Seria bueno agregar una tabla con la que se interprete los valores-->
            
            <p align="left">Los datos usados para el cálculo provienen de estaciones meteorológicas del Servicio
               Nacional de Meterología e Hidrología del Perú  (SENAMHI).
            </p>
            
         </div>
         
         
         <div id="tabs-2" class="ui-tabs-panel">
            
            <h3 align="center">¿Cómo se calcula el ICDN?</h3>
            
            <p>
               Para el cáluclo del ICDN se necesita del caudal registrado en un mes y el caudal medio histórico para el mismo
			   mes. El ICDN consiste en la división entre la diferencia del caudal registrado y el caudal medio histórico, con 
			   la suma del caudal registrado y la media histórica, todo esto para un mes determinado.
               
            </p>
            
            <h3 align="center">¿Cómo se calcula el ICD?</h3>
            
            <p>
			   El ICD se determinó siguiendo la metodología para el cálculo del SPI descrito por McKee et al (1993). Esta 
			   metodología se fundamenta en el ajuste de la serie histórica del caudal medio mensual a la función 
			   probabilista tipo Gamma y en la transformación de los datos resultantes a la función normal estandarizada.
               
            </p>
            
         </div>
         
         
         
         <div id="tabs-3" class="ui-tabs-panel">
            
            <h2 align="center">Fuente de Datos</h2>
            
            <p align="left"><a href="http://ons.snirh.gob.pe/SOURCES/.Peru/.ANA/.Caudales/.Historico/" shape="rect">Caudales registrados en estaciones</a>, entregado por SENAMHI <a href="http://www.senamhi.gob.pe/" shape="rect">(SENAMHI)</a>
            
			</p>
			
         </div>
         
         
         <div id="tabs-4" class="ui-tabs-panel">
            
            <h3 align="center">Soporte</h3>
            
            <p>
               Sus consultas a: <a href="mailto:snirh@ana.gob.pe?subject=Caudal Histiórico Per%C3%BA" shape="rect">snirh@ana.gob.pe</a>
               
            </p>
            
         </div>
                
         <div id="tabs-5" class="ui-tabs-panel">
            
            <h3 align="center">Instrucciones</h3>
            
            <div class="buttonInstructions"></div>
            
         </div>
                  
      </div>
      
      <div class="optionsBar">
         
         <fieldset class="navitem" id="share">
            <legend>Compartir</legend>
         </fieldset>
         
      </div>
      
   </body>
</html>