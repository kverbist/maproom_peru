<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:wms="http://www.opengis.net/wms#" xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#" xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#" xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#" xmlns:xs="http://www.w3.org/2001/XMLSchema#" version="XHTML+RDFa 1.0">
<head>
      <meta name="viewport" content="width=device-width; initial-scale=1.0;" />
      <meta xml:lang="" property="maproom:Entry_Id" content="Historical" />
      <title>Precipitación Máxima Esperada</title>
      <link rel="stylesheet" type="text/css" href="../../unesco.css" />
      <script type="text/javascript" src="../../../../uicore/uicore.js" xml:space="preserve"></script><script type="text/javascript" src="../../../maproom/unesco.js" xml:space="preserve"></script><link class="altLanguage" rel="alternate" hreflang="en" href="MaximumPrecip.html?Set-Language=en" />
      <link class="share" rel="canonical" href="MaximumPrecip.html" />
      <meta xml:lang="" property="maproom:Entry_Id" content="Historical" lang="" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Historical_Droughts_term" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
      <link rel="term:icon" href="http://ons.snirh.gob.pe/expert/(5YR_ReturnPeriod)//var/parameter/(5YR_ReturnPeriod)/eq/%7BSOURCES/.Peru/.CAZALAC/.MaxExpectedPrecip/.5YR//long_name/(Precipitacion%20Maxima%20Esperada%20cada%205%20anios)/def/X/Y/fig%3A/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/%3Afig//5YR_ReturnPeriod/1/6132.261/plotrange%7Dif//var/get_parameter/(10YR_ReturnPeriod)/eq/%7BSOURCES/.Peru/.CAZALAC/.MaxExpectedPrecip/.10YR//long_name/(Precipitacion%20Maxima%20Esperada%20cada%2010%20anios)/def/X/Y/fig%3A/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/%3Afig//10YR_ReturnPeriod/1/6132.261/plotrange%7Dif//var/get_parameter/(50YR_ReturnPeriod)/eq/%7BSOURCES/.Peru/.CAZALAC/.MaxExpectedPrecip/.50YR//long_name/(Precipitacion%20Maxima%20Esperada%20cada%2050%20anios)/def/X/Y/fig%3A/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/%3Afig//50YR_ReturnPeriod/1/6132.261/plotrange%7Dif//var/get_parameter/(100YR_ReturnPeriod)/eq/%7BSOURCES/.Peru/.CAZALAC/.MaxExpectedPrecip/.100YR//long_name/(Precipitacion%20Maxima%20Esperada%20cada%20100%20anios)/def/X/Y/fig%3A/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/%3Afig//100YR_ReturnPeriod/1/6132.261/plotrange%7Dif/X/-82/-68/plotrange/Y/-17/0/plotrange/(antialias)/true/psdef/(fntsze)/20/psdef/Y/-18.33336/-0.0333297/plotrange//plotborder/72/psdef//plotaxislength/432/psdef+.gif" />
   </head>
   <body xml:lang="es">
      
      <form name="pageform" id="pageform" class="carryup carry dlimg dlauximg share" method="get" enctype="application/x-www-form-urlencoded">
         <input class="carryLanguage carryup carry" name="Set-Language" type="hidden" /> 
         	<input class="carry dlimg dlimgloc admin share" name="bbox" type="hidden" />
         <input class="dlimg dlauximg onlyvar share" name="var" type="hidden" />
         	<input class="dlimg" name="plotaxislength" type="hidden" />
         	<input class="carry share dlimgloc dlimgts" name="region" type="hidden" />
         <input class="carry share dlimgloc dlimgts admin" name="resolution" type="hidden" data-default="irids:SOURCES:Features:Hydrological:Cuencas:Mayores:ds" />
         
      </form>
      
      <div class="controlBar">
         
         <fieldset class="navitem" id="toSectionList">
            
            <legend>Maproom</legend> <a rev="section" class="navlink carryup" href="/maproom/Historical/" shape="rect">Histórica</a>
            
         </fieldset>
         
         <fieldset class="navitem" id="chooseSection"> 
            
            <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Historical_Droughts_term"><span property="term:label">Frecuencias de Sequías Históricas</span></legend>
            
         </fieldset> 
         
         <fieldset class="navitem">
            
            <legend>Región</legend>
            <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsPeru.json" shape="rect"></a>
            <select class="RegionMenu" name="bbox">
               <option value="">Peru</option>
               <optgroup class="template" label="Región">
                  <option class="irigaz:hasPart irigaz:id@value term:label"></option>
               </optgroup></select>
            
         </fieldset>
         
         <fieldset class="navitem">
            
            <legend>Análisis</legend><span class="selectvalue"></span><select class="pageformcopy" name="var">
               <option value="">Periodo de retorno de 5 años</option>
               <option value="10YR_ReturnPeriod">Periodo de retorno de 10 años</option>
               <option value="50YR_ReturnPeriod">Periodo de retorno de 50 años</option>
               <option value="100YR_ReturnPeriod">Periodo de retorno de 100 años</option></select>
            
         </fieldset>
         
         <fieldset class="navitem">
            
            <legend>Promedio Espacial Sobre</legend><span class="selectvalue"></span><select class="pageformcopy" name="resolution">
               <option value="0.0125">Ubicación puntual</option>
               <option value="irids:SOURCES:Features:Political:Peru:Departamento:ds">Departamento</option>
               <option value="irids:SOURCES:Features:Political:Peru:Provincia:ds">Provincia</option>
               <option value="irids:SOURCES:Features:ANA:AmbAdm:AAA:ds">Autoridad Administrativa del Agua  (AAA)</option>
               <option value="irids:SOURCES:Features:ANA:AmbAdm:ALA:ds">Administración Local de Agua (ALA)</option>
               <option value="irids:SOURCES:Features:Hydrological:Cuencas:Mayores:ds">Unidad Hidrográfica</option></select>       	     
            
            <link class="admin" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/(irids%3ASOURCES%3AFeatures%3AHydrological%3ACuencas%3AMayores%3Ads)//resolution/parameter/geoobject/(bb%3A-83%3A-17%3A-67%3A0%3Abb)//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
            <select class="pageformcopy" name="region">
               <optgroup class="template" label="Label">
                  <option class="iridl:values region@value label"></option>
               </optgroup></select>
            
         </fieldset>	
         	
         
      </div>
      
      
      <div class="ui-tabs">
         
         <ul class="ui-tabs-nav">
            <li><a href="#tabs-1" shape="rect">Descripción</a></li>
            <li><a href="#tabs-2" shape="rect">Más información</a></li>
            <li><a href="#tabs-3" shape="rect">Fuente</a></li>
            <li><a href="#tabs-4" shape="rect">Soporte</a></li>
            <li><a href="#tabs-5" shape="rect">Instrucciones</a></li>
         </ul>
         	
         
         <fieldset class="dlimage regionwithinbbox">
            <a class="dlimgts" rel="iridl:hasTable" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.CAZALAC/.MaxExpectedPrecip/%7B5YR/10YR/50YR/100YR%7Dgrouptogrid//long_name/%28Precipitacion%20Maxima%20Esperada%29def//units/%28mm/anio%29def/M/%28Periodo%20de%20Retorno%29renameGRID/%28bb:-76%2C-8%2C-76.2%2C-8.2%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/DATA/0/AUTO/RANGE/table:/1/:table/" shape="rect"></a>
            
            <div style="float: left;">
               <img class="dlimgloc" src="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.CAZALAC/.MaxExpectedPrecip/.5YR/X/Y/%28bb:-82%2C-17%2C-68%2C0%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-76%2C-8%2C-76.2%2C-8.2%29dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index%5BX/Y%5Dweighted-average/exch/Y/exch%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborder/0/psdef//plotaxislength/120/psdef+.gif" />
               
            </div>	
            
            
            <div style="float: left;">
               
               
               <div class="valid" style="display: inline-block; text-align: top;">
                  <a class="dlimgts" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/(bb%3A1%3A2%3A3%3A4%3Abb)//region/parameter/geoobject/info.json" shape="rect"></a>
                  
                  <div class="template"> Observaciones para <span class="bold iridl:long_name"></span>
                     
                  </div>
                  
               </div>
               
               
               
               <div class="valid" style="text-align: top;">
                  <a class="dlimgloc" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.CAZALAC/.MaxExpectedPrecip/%7B%7D/%7B/(bb%3A-76%2C-8%2C-76.2%2C-8.2)//region/parameter/geoobject%5BX/Y%5Dweighted-average/toi4/name/cvntos/(YR)/search/pop/nip/nip//long_name/exch/def%7Dforalldatasets2/%7Ba4/a2/a3/a1%7Dds/info.json" shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"

}
}
}
</script>
                  <div>
                     
                     <table class="valid template">
                        <tr style="color : black">
                           <td rowspan="1" colspan="1">Precipitación Máxima Esperada para un Periodo de Retorno de</td>
                           <td class="name " rowspan="1" colspan="1"> prueba </td>
                           <td align="right" class="value " rowspan="1" colspan="1"> mm</td>
                        </tr>
                     </table>
                     
                     
                  </div>
                  
                  
               </div>
               
            </div>
            
            
            <br clear="none" />
            <img class="dlimgts regionwithinbbox" src="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.CAZALAC/.MaxExpectedPrecip/%7B5YR/10YR/50YR/100YR%7Dgrouptogrid//long_name/%28Precipitacion%20Maxima%20Esperada%29def//units/%28mm/anio%29def/M/%28Periodo_de_Retorno%29renameGRID/%28bb:-76%2C-8%2C-76.2%2C-8.2%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/dup/Periodo_de_Retorno/fig-/colorbars2/-fig//plotborder/72/psdef//plotaxislength/432/psdef//plotborder/72/psdef//plotaxislength/432/psdef+.gif" />
            
         </fieldset>
         
         	
         
         <fieldset class="dlimage" id="content" about="">
            
            <link rel="iridl:hasFigure" href="http://ons.snirh.gob.pe/expert/%285YR_ReturnPeriod%29//var/parameter/%285YR_ReturnPeriod%29/eq/%7BSOURCES/.Peru/.CAZALAC/.MaxExpectedPrecip/.5YR//long_name/%28Precipitacion%20Maxima%20Esperada%20cada%205%20anios%29/def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig//5YR_ReturnPeriod/1/6132.261/plotrange%7Dif//var/get_parameter/%2810YR_ReturnPeriod%29/eq/%7BSOURCES/.Peru/.CAZALAC/.MaxExpectedPrecip/.10YR//long_name/%28Precipitacion%20Maxima%20Esperada%20cada%2010%20anios%29/def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig//10YR_ReturnPeriod/1/6132.261/plotrange%7Dif//var/get_parameter/%2850YR_ReturnPeriod%29/eq/%7BSOURCES/.Peru/.CAZALAC/.MaxExpectedPrecip/.50YR//long_name/%28Precipitacion%20Maxima%20Esperada%20cada%2050%20anios%29/def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig//50YR_ReturnPeriod/1/6132.261/plotrange%7Dif//var/get_parameter/%28100YR_ReturnPeriod%29/eq/%7BSOURCES/.Peru/.CAZALAC/.MaxExpectedPrecip/.100YR//long_name/%28Precipitacion%20Maxima%20Esperada%20cada%20100%20anios%29/def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig//100YR_ReturnPeriod/1/6132.261/plotrange%7Dif/X/-82/-68/plotrange/Y/-17/0/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef/Y/-18.33336/-0.0333297/plotrange//plotborder/72/psdef//plotaxislength/432/psdef/" />
            <img class="dlimg" src="http://ons.snirh.gob.pe/expert/%285YR_ReturnPeriod%29//var/parameter/%285YR_ReturnPeriod%29/eq/%7BSOURCES/.Peru/.CAZALAC/.MaxExpectedPrecip/.5YR//long_name/%28Precipitacion%20Maxima%20Esperada%20cada%205%20anios%29/def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig//5YR_ReturnPeriod/1/6132.261/plotrange%7Dif//var/get_parameter/%2810YR_ReturnPeriod%29/eq/%7BSOURCES/.Peru/.CAZALAC/.MaxExpectedPrecip/.10YR//long_name/%28Precipitacion%20Maxima%20Esperada%20cada%2010%20anios%29/def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig//10YR_ReturnPeriod/1/6132.261/plotrange%7Dif//var/get_parameter/%2850YR_ReturnPeriod%29/eq/%7BSOURCES/.Peru/.CAZALAC/.MaxExpectedPrecip/.50YR//long_name/%28Precipitacion%20Maxima%20Esperada%20cada%2050%20anios%29/def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig//50YR_ReturnPeriod/1/6132.261/plotrange%7Dif//var/get_parameter/%28100YR_ReturnPeriod%29/eq/%7BSOURCES/.Peru/.CAZALAC/.MaxExpectedPrecip/.100YR//long_name/%28Precipitacion%20Maxima%20Esperada%20cada%20100%20anios%29/def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig//100YR_ReturnPeriod/1/6132.261/plotrange%7Dif/X/-82/-68/plotrange/Y/-17/0/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef/Y/-18.33336/-0.0333297/plotrange//plotborder/72/psdef//plotaxislength/432/psdef/+.gif" border="0" alt="image" /><br clear="none" />
            <img class="dlauximg" src="http://ons.snirh.gob.pe/expert/%285YR_ReturnPeriod%29//var/parameter/%285YR_ReturnPeriod%29/eq/%7BSOURCES/.Peru/.CAZALAC/.MaxExpectedPrecip/.5YR//long_name/%28Precipitacion%20Maxima%20Esperada%20cada%205%20anios%29/def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig//5YR_ReturnPeriod/1/6132.261/plotrange%7Dif//var/get_parameter/%2810YR_ReturnPeriod%29/eq/%7BSOURCES/.Peru/.CAZALAC/.MaxExpectedPrecip/.10YR//long_name/%28Precipitacion%20Maxima%20Esperada%20cada%2010%20anios%29/def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig//10YR_ReturnPeriod/1/6132.261/plotrange%7Dif//var/get_parameter/%2850YR_ReturnPeriod%29/eq/%7BSOURCES/.Peru/.CAZALAC/.MaxExpectedPrecip/.50YR//long_name/%28Precipitacion%20Maxima%20Esperada%20cada%2050%20anios%29/def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig//50YR_ReturnPeriod/1/6132.261/plotrange%7Dif//var/get_parameter/%28100YR_ReturnPeriod%29/eq/%7BSOURCES/.Peru/.CAZALAC/.MaxExpectedPrecip/.100YR//long_name/%28Precipitacion%20Maxima%20Esperada%20cada%20100%20anios%29/def/X/Y/fig:/colors/lightgrey/verythin/states_gaz/black/verythin/countries_gaz/black/verythin/coasts_gaz/:fig//100YR_ReturnPeriod/1/6132.261/plotrange%7Dif/X/-82/-68/plotrange/Y/-17/0/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef/Y/-18.33336/-0.0333297/plotrange//plotborder/72/psdef//plotaxislength/432/psdef/+.auxfig/+.gif" />
            
         </fieldset>
         
         
         <div id="tabs-1" class="ui-tabs-panel" about="">
            
            <h2 align="center" property="term:title">Precipitación Máxima Esperada</h2>
            
            <p align="left" property="term:description">Este mapa muestra la precipitación máxima anual esperada para múltiples periodos de
               retorno para Perú.
            </p>
            
            <p align="left">En el menú&gt;<b>análisis</b> puedes seleccionar el periodo de retorno: 5, 10, 50 o 100 años.
               En el menú&gt;<b>región</b> puedes seleccionar la región de interés.
            </p>
            
            <p align="left">Los diferentes periodos de retorno indican la recurrencia de un evento extremo. Por
               ejemplo, si el mapa indica que por un periodo de retorno de 5 años la precipitación
               máxima esperada es 5000 mm significa que en ese punto se puede esperar una precipitación
               anual de 5000 mm cada 5 años.
            </p>
            
            <p>El método usado es un análisis de frecuencia de eventos extremos históricos. Significa
               que se usa datos de precipitación históricos para ajustar una distribución probabilística.
               Esa distribución probabilística es usada para identificar la frecuencia con la cual
               esperamos tener eventos con una cierta magnitud. 
            </p>
            
            <p align="left">El atlas de sequías es proporcionado por el Centro del Agua para Zonas Áridas en América
               Latina y el Caribe  (CAZALAC).
            </p>
            
            <p align="left">  <img src="../../icons/Logo_cazalac" alt="Logo CAZALAC" /></p>
            
            <p align="left"><b>Referencias</b> 
            </p>
            
            <p align="left">Nunez, J.H., K. Verbist, J. Wallis, M. Schaeffer, L. Morales, and W.M. Cornelis. 2011.
               Regional frequency analysis for mapping drought events in north-central Chile. <i>J. Hydrol. </i> <b>405</b> 352-366.
               
            </p>
            
         </div>
         
         
         <div id="tabs-2" class="ui-tabs-panel">
            
            <h3 align="center">¿Qué método fue usada para calcular la precipitación máxima esperada?</h3>
            
            <p>
               El método usado toma en consideración la baja intensidad de datos, los datos son agrupados
               en regiones que son climatológicamente homogéneos cual permite de aplicar estadísticas
               más robustos.
               
            </p>
            
            <p>Para poder debilitar el efecto de eventos extremos se usa L-momentos en vez de momentos
               normales. De esta forma eventos extremos no influencian la selección de la distribución.
               Este método es seleccionado por que es más apropiado en regiones con una variabilidad
               interanual significativa y series de datos de corta duración.
            </p>
            
            <p>Información detallada sobre el método usado se puede encontrar <a href="http://www.cazalac.org/documentos/Guia_Metodologia_Atlas_de_Sequia.pdf" shape="rect">aquí</a>.
            </p> 
            
         </div>
         
         
         
         <div id="tabs-3" class="ui-tabs-panel">
            
            <h2 align="center">Fuente de Datos</h2>
            
            <p><a href="http://www.climatedatalibrary.cl/SOURCES/.CAZALAC/.DroughtAtlas/.Peru/" shape="rect">Atlas de Sequía de America Latina y el Caribe</a>, entregado por el Centro <a href="http://www.climatedatalibrary.cl/SOURCES/.CAZALAC/.DroughtAtlas/" shape="rect">CAZALAC</a></p>
            
         </div>
         
         
         <div id="tabs-4" class="ui-tabs-panel">
            
            <h3 align="center">Soporte</h3>
            
            <p>
               Sus consultas a: <a href="mailto:snirh@ana.gob.pe?subject=Precipitacion Observada Per%C3%BA" shape="rect">snirh@ana.gob.pe</a>
               
            </p>
            
         </div>
         
         
         <div id="tabs-5" class="ui-tabs-panel">
            
            <h3 align="center">Instrucciones</h3>
            
            <div class="buttonInstructions"></div>
            
         </div>
         
         
      </div>
      
      <div class="optionsBar">
         
         <fieldset class="navitem" id="share">
            <legend>Compartir</legend>
         </fieldset>
         
      </div>
      
   </body>
</html>