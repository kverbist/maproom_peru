<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
 xml:lang="es"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<title>Frecuencia de Sequias Historicas</title>
<link rel="stylesheet" type="text/css" href="../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="index.html?Set-Language=en" />
<link rel="canonical" href="index.html" />
<meta property="maproom:Sort_Id" content="a04" />
      <link class="" rel="home" href="http://www.ana.gob.pe/" title="ANA" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="../../maproom//navmenu.json" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#historical" />
<link rel="term:icon" href="http://www.climatedatalibrary.cl/expert/%285YR_ReturnPeriod%29//var/parameter/%285YR_ReturnPeriod%29/eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Peru/.5YR_ReturnPeriod/precip_colors/X/Y/fig:/colors/thin/solid/countries_gaz/coasts_gaz/:fig%7Dif//var/get_parameter/%2810YR_ReturnPeriod%29/eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Peru/.10YR_ReturnPeriod/precip_colors/X/Y/fig:/colors/thin/solid/countries_gaz/coasts_gaz/:fig%7Dif//var/get_parameter/%2850YR_ReturnPeriod%29/eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Peru/.50YR_ReturnPeriod/precip_colors/X/Y/fig:/colors/thin/solid/countries_gaz/coasts_gaz/:fig%7Dif//var/get_parameter/%28100YR_ReturnPeriod%29/eq/%7BSOURCES/.CAZALAC/.DroughtAtlas/.Peru/.100YR_ReturnPeriod/precip_colors/X/Y/fig:/colors/thin/solid/countries_gaz/coasts_gaz/:fig%7Dif//5YR_ReturnPeriod/91.46422/6132.261/plotrange/X/-81.3/-68.7/plotrange/Y/-18.2/-0.07/plotrange//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef/+.gif" />
<script type="text/javascript" src="../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../maproom/unesco.js"></script>
</head>
<body>

<form name="pageform" id="pageform">
<input class="carryLanguage carryup" name="Set-Language" type="hidden" />
</form>
<div class="controlBar">
           <fieldset class="navitem"> 
                <legend>Peru</legend> 
                    <a rev="section" class="navlink carryup" href="/maproom/">Observatorio de Sequ&#237;a</a>
            </fieldset> 
           <fieldset class="navitem"> 
                <legend>Peru</legend> 
                     <span class="navtext">Frecuencia de Sequ&#237;as Hist&#243;ricas</span>
            </fieldset> 
</div>
<div>
 <div id="content" class="searchDescription">
<h2 property="term:title">Informaci&#243;n de Sequ&#237;as Hist&#243;ricas</h2>
<p align="left" property="term:description">
Informaci&#243;n hist&#243;rica para la caracterizaci&#243;n de la sequ&#237;a en el Perú. En esta sala de mapas se visualizan datos hist&#243;ricos y los resultados del An&#225;lisis Regional de Frecuencia usando L-Momentos para Perú. </p>
<p>El an&#225;lisis fue realizado usando la metodolog&#237;a descrito en N&#250;&#241;ez et al. (2010). 
</p> <p> N&#250;&#241;ez, J.H., K. Verbist, J. Wallis, M. Schaeffer, L. Morales, and W.M. Cornelis. 2011. Regional frequency analysis for mapping drought events in north-central Peru. J. Hydrol. 405 352-366..
</p>
</div>
<div class="rightcol tabbedentries" about="/maproom/Historical/" >
  <link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Historical_Droughts_term" />
  <link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Historical_Analysis_term" />
</div>

</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend>

</fieldset>
</div>
 </body>

 </html>