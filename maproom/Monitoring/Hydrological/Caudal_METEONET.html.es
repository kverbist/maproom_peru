<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      xmlns:og="http://ogp.me/ns#" 
      version="XHTML+RDFa 1.0"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<meta xml:lang="" property="maproom:Entry_Id" content="Monitoring_Hydrological" />
      <title>Niveles de R&#237;os Observados - METEONET</title>
      <link rel="stylesheet" type="text/css" href="../../unesco.css" />
      <link class="altLanguage" rel="alternate" hreflang="en" href="Caudal_METEONET.html?Set-Language=en" />
      <link class="share" rel="canonical" href="Caudal_METEONET.html" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Hydrological_term" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
      <link rel="term:icon" href="http://ons.snirh.gob.pe/expert/%28Observed%29//anal/parameter/%28Observed%29eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Tiempo_real/.Nivel/.Mensual/lon/lat/2/copy/Nivel_mensual//units/%28m%29def/6/-1/roll/pop//long_name/%28Niveles%20de%20los%20Rios%29def/X/Y/fig-/colors/||/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef+//plotborder+0+psdef//plotaxislength+432+psdef//fntsze+20+psdef//antialias+true+psdef//color_smoothing+1+psdef+.gif" />
      <script type="text/javascript" src="../../../../uicore/uicore.js" xml:space="preserve">
      </script><script type="text/javascript" src="../../../maproom/unesco.js" xml:space="preserve"></script>
</head>
   <body xml:lang="es">
      
      <form name="pageform" id="pageform" class="carryup carry dlimg dlauximg share" method="get" enctype="application/x-www-form-urlencoded">
         <input class="carryLanguage carryup carry " name="Set-Language" type="hidden" /> 
		 <input class="dlimg dlimgloc admin maptable share" name="bbox" type="hidden" />
		 <input class="dlimg share" name="T" type="hidden" />
		 <input class="dlimg" name="plotaxislength" type="hidden" />
		 <input class="share dlimgts dlimgloc dlimglocclick station" name="region" type="hidden" />
		 <input class="transformRegion dlimglocclick" name="clickpt" type="hidden" />
		 <input class="dlimg dlimgts dlauximg maptable share" name="anal" type="hidden" />		 
		 <!--input class="dlimg dlimgts dlauximg maptable share" name="anal" type="hidden" /-->		 
         <!--input class="share dlimgloc dlimgts" name="region" type="hidden" /-->
         <input class="pickarea dlimgts admin" name="resolution" type="hidden" value="irids:SOURCES:Peru:ANA:Tiempo_real:Nivel:Mensual:ds" />
         
      </form>
      
      <div class="controlBar">
         
         <fieldset class="navitem" id="toSectionList">
            
                <legend>Maproom</legend> 
            		<a rev="section" class="navlink carryup" href="/maproom/Monitoring/" shape="rect">Monitoreo</a>
            </fieldset> 
         
         <fieldset class="navitem" id="chooseSection"> 
            
            <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Hydrological_term"><span property="term:label">Sequía Hidrológica</span></legend>
            
         </fieldset> 
         
         <fieldset class="navitem">
            
            <legend>Región</legend>
            <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsPeru.json" shape="rect"></a>
            <select class="RegionMenu" name="bbox">
               <option value="">Peru</option>
               <optgroup class="template" label="Región">
                  <option class="irigaz:hasPart irigaz:id@value term:label"></option>
               </optgroup></select>
            
         </fieldset>
         
         <fieldset class="navitem">
            
            <legend>Análisis</legend>
			   <span class="selectvalue"></span><select class="pageformcopy" name="anal">
               <option value="">Observado</option></select>
           
         </fieldset>
         
         <fieldset class="navitem">
            
            <legend>Selección de Estación</legend>	
            
            <link class="admin" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/%28irids%3ASOURCES%3APeru%3AANA%3ATiempo_real%3ANivel%3AMensual%3Ads%29//resolution/parameter/geoobject/%28bb%3A-82%3A-19%3A-65%3A0%3Abb%29//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
            <select class="pageformcopy" name="region">
               <optgroup class="template" label="Label">
                  <option class="iridl:values region@value label"></option>
               </optgroup></select>
            
         </fieldset>
         	
         
      </div>      
      
      <div class="ui-tabs">
         
         <ul class="ui-tabs-nav">
            <li><a href="#tabs-1" shape="rect">Descripción</a></li>
            <li><a href="#tabs-2" shape="rect">Más información</a></li>
            <li><a href="#tabs-3" shape="rect">Fuente</a></li>
            <li><a href="#tabs-4" shape="rect">Soporte</a></li>
            <li><a href="#tabs-5" shape="rect">Instrucciones</a></li>
         </ul>
         	
         
         <fieldset class="regionwithinbbox dlimage" about="">
            <a class="dlimgts" rel="iridl:hasTable" href="http://ons.snirh.gob.pe/expert/expert/SOURCES/.Peru/.ANA/.Tiempo_real/.Nivel/.Mensual/.Nivel_mensual//long_name/%28Nivel%20de%20R%C3%ADo%29/def//units/%28m%29/def/nombre/%28irids%3ASOURCES%3APeru%3AANA%3ATiempo_real%3ANivel%3AMensual%3Anombre%40LAGUNA%20ARICOTA%3Ads%29//region/parameter/geoobject/.nombre/.first/VALUE/T/exch/table%3A/2/%3Atable/" shape="rect"></a>
            
            <div style="float: left;">
               <img class="dlimgloc" src="http://ons.snirh.gob.pe/SOURCES/.WORLDBATH/.bath/X/Y/%28bb:-82%2C-19%2C-65%2C0%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28pt:-75:-5:pt%29//region/parameter/geoobject/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/geometrypushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0.1/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif" />
               
            </div>	
            
            
            <div style="float: left;">
               
               
               <!--div class="valid" style="display: inline-block; text-align: top;"-->
                <div class="valid" style="text-align: top;">
                  <a class="station" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/%28pt:-75:-5:pt%29//region/parameter/geoobject/info.json" shape="rect"></a>
                  
                  <div class="template" style="color : black"> 
				     Estacion METEONET <b><span class="iridl:long_name"></span></b>
                     
                  </div>
                  
                </div>
               
               
               
                <div class="valid" style="text-align: top;">
				  <a class="station" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.ANA/.Tiempo_real/.Nivel/.Mensual/nombre/%28irids%3ASOURCES%3APeru%3AANA%3ATiempo_real%3ANivel%3AMensual%3Anombre%40LAGUNA%20ARICOTA%3Ads%29//region/parameter/geoobject/.nombre/.first/VALUE/lon/lat/2/ds/info.json" shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"
}
}
}
</script>
                  <div>
                     
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="name " rowspan="1" colspan="1"></td>
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                        </tr>
                     </table>
                     
                     
                  </div>
                  
                  
               </div>
			   
			   <div class="template"> <b>Observaciones para el mes actual: </b></div>
               
			   <div class="valid" style="text-align: top;">
                  <a class="dlimgloc" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.ANA/.Tiempo_real/.Nivel/.Mensual/.Nivel_mensual/T/last/VALUE/nombre/%28irids%3ASOURCES%3APeru%3AANA%3ATiempo_real%3ANivel%3AMensual%3Anombre%40LAGUNA%20ARICOTA%3Ads%29//region/parameter/geoobject/.nombre/.first/VALUE/1/ds/info.json" shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"

}
}
}
</script>
                  <div>
                     
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="name " rowspan="1" colspan="1"> m</td>
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                        </tr>
                     </table>
                     
                  </div>
                  
               </div>
			   
            </div>
            
            <br clear="none" />
			<div class="dlimgtsbox">
				<img class="dlimgts regionwithinbbox" rel="iridl:hasFigureImage" src="http://ons.snirh.gob.pe/expert/%28Observed%29//anal/parameter/%28Observed%29/eq/%7BSOURCES/.Peru/.ANA/.Tiempo_real/.Nivel/.Mensual/.Nivel_mensual/nombre/%28irids:SOURCES:Peru:ANA:Tiempo_real:Nivel:Mensual:nombre%40LAGUNA%20ARICOTA:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Nivel%20del%20R%C3%ADo%29/def/DATA/0/AUTO/RANGE/dup/T/fig-/colorbars2/-fig%7Dif/nombre/last/plotvalue/+.gif" />
						
			</div>
			<br clear="none" />
			
			
			
         </fieldset>
         
         	
         
         <fieldset class="dlimage" id="content" about="">            
            <link rel="iridl:hasFigure" href="http://ons.snirh.gob.pe/expert/%28Observed%29//anal/parameter/%28Observed%29eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Tiempo_real/.Nivel/.Mensual/lon/lat/2/copy/Nivel_mensual//units/%28m%29def/6/-1/roll/pop//long_name/%28Niveles%20de%20los%20R%C3%ADos%29def/X/Y/fig-/colors/||/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef/" />
            <img class="dlimg" src="http://ons.snirh.gob.pe/expert/%28Observed%29//anal/parameter/%28Observed%29eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Tiempo_real/.Nivel/.Mensual/lon/lat/2/copy/Nivel_mensual//units/%28m%29def/6/-1/roll/pop//long_name/%28Niveles%20de%20los%20R%C3%ADos%29def/X/Y/fig-/colors/||/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef/+.gif" border="0" alt="image" /><br clear="none" />
            <img class="dlauximg" src="http://ons.snirh.gob.pe/expert/%28Observed%29//anal/parameter/%28Observed%29eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Tiempo_real/.Nivel/.Mensual/lon/lat/2/copy/Nivel_mensual//units/%28m%29def/6/-1/roll/pop//long_name/%28Niveles%20de%20los%20R%C3%ADos%29def/X/Y/fig-/colors/||/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef/+.auxfig/+.gif" />
            
         </fieldset>
         
         
         <div id="tabs-1" class="ui-tabs-panel" about="">
            
            <h3 align="center" property="term:title">Niveles de R&#237;os Observados - METEONET</h3>
            
            <p align="left" property="term:description">Este mapa muestra los datos de nivel de algunos ríos de la vertiente del Pacífico, el cual está directamente relacionado al caudal de los ríos.</p>
			<p align="left" property="term:description">Las mediciones están en metros (m).</p>
            <p align="left" property="term:description">Esta información es recopilada por parte del METEONET del ANA.</p>
            
         </div>
         
         
         <div id="tabs-2" class="ui-tabs-panel">
            
            <p>
               Los datos provienen de los niveles de rio de la Autoridad Nacional del Agua de
               Peru (ANA).
               
            </p>
            
         </div>
         
         
         
         <div id="tabs-3" class="ui-tabs-panel">
            
            <h2 align="center">Fuente de Datos</h2>
            
            <p align="left"><a href="http://ons.snirh.gob.pe/SOURCES/.Peru/.ANA/.Tiempo_real/.Nivel/.Mensual/" shape="rect">Niveles de los r&#237;s registrados en estaciones</a>, entregado por METEONET<a href="http://ana.gob.pe/" shape="rect">(ANA)</a>
            
			</p>
			
         </div>
         
         
         <div id="tabs-4" class="ui-tabs-panel">
            
            <h3 align="center">Soporte</h3>
            
            <p>
               Sus consultas a: <a href="mailto:snirh@ana.gob.pe?subject=Niveles METEONET" shape="rect">snirh@ana.gob.pe</a>
               
            </p>
            
         </div>
                
         <div id="tabs-5" class="ui-tabs-panel">
            
            <h3 align="center">Instrucciones</h3>
            
            <div class="buttonInstructions"></div>
            
         </div>
                  
      </div>
      
      <div class="optionsBar">
         
         <fieldset class="navitem" id="share">
            <legend>Compartir</legend>
         </fieldset>
         
      </div>
      
   </body>
</html>