<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      xmlns:og="http://ogp.me/ns#" 
      version="XHTML+RDFa 1.0"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<meta xml:lang="" property="maproom:Entry_Id" content="Monitoring_Hydrological" />
<title>Volumen de Embalses Observados</title>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="DischargeDGA.html?Set-Language=en" />
<link class="share" rel="canonical" href="DischargeDGA.html" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Hydrological_term"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
<link rel="term:icon" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Embalses/.Monthly/lon/lat/2/copy/Monthly_level_percentage_average/-20/masklt/DATA/-4/4/RANGE/6/-1/roll/pop//long_name/%28Nivel%20embalse%20-Porcentaje%20del%20Normal%29/def//name/%28st_precip%29/def/pdsi_colorbars//symmetric/false/def/DATA/0/100/RANGE/X/Y/fig-/colors/||/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//Y/-19/0/plotrange//st_precip/-3/3/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef+//antialias+true+psdef//fntsze+20+psdef//plotaxislength+432+psdef//color_smoothing+1+psdef//plotborder+0+psdef+.gif" />
<script type="text/javascript" src="../../../uicore/uicore.js" xml:space="preserve"></script>
<script type="text/javascript" src="../../../maproom/unesco.js" xml:space="preserve"></script>
<style xml:space="preserve">
.dlimage.ver2 {
        width: 24%;
        float: right;
}
.dlimagei.ver3 {
        width: 99%;
}
.dlimage.ver4 {
        width: 33%;
}
.dlimgtsbox {
width: 99%;
display: inline-block
 }
</style>
</head>
<body  xml:lang="es">
<form name="pageform" id="pageform" class="carryup carry dlimg dlauximg dlimgts share" method="get" enctype="application/x-www-form-urlencoded"> 
<input class="carryLanguage carryup carry " name="Set-Language" type="hidden" />
<input class="carry dlimg dlimgloc admin maptable share" name="bbox" type="hidden" />
<input class="dlimg share" name="T" type="hidden" />
<input class="dlimg" name="plotaxislength" type="hidden" />
<input class="dlimg dlauximg dlimgts maptable  share" name="anal" type="hidden" />
<input class="share dlimgts dlimgloc dlimglocclick station" name="region" type="hidden" />
<input class="transformRegion dlimglocclick" name="clickpt" type="hidden" />
<input class="pickarea dlimgts admin " name="resolution" type="hidden" value="irids:SOURCES:Peru:ANA:Embalses:Monthly:ds" />
<!-- list of layers form with names corresponding to different layers of the image so that they can be un/checked by default. I leave it commented out since as of now scatterlabel is not considered a layer
<input class="dlimg share" name="layers" value="Discharge" checked="checked" type="checkbox" />
<input class="dlimg share" name="layers" value="aprod" checked="checked" type="checkbox" />
<input class="dlimg share" name="layers" value="label" checked="unchecked" type="checkbox" />
<input class="dlimg share" name="layers" value="rivers_gaz" type="checkbox" />
<input class="dlimg share" name="layers" value="coasts_gaz" checked="checked" type="checkbox" />
<input class="dlimg share" name="layers" value="countries_gaz" type="checkbox" checked="checked" />
-->
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>Maproom</legend> 
            		<a rev="section" class="navlink carryup" href="/maproom/Monitoring/" shape="rect">Monitoreo</a>
            </fieldset> 
            <fieldset class="navitem" id="chooseSection"> 
                <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Hydrological_term"><span property="term:label">Sequía Hidrológica</span></legend>
            </fieldset> 
            <fieldset class="navitem">
                <legend>Región</legend>
          	<a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsPeru.json" shape="rect"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Peru</option>
                <optgroup class="template" label="Región">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup>
		</select>
           </fieldset>
           <fieldset class="navitem">
	    <legend>Análisis</legend>
	     <span class="selectvalue"></span><select class="pageformcopy" name="anal">
	     <option value="">Porcentaje</option>
	     <option value="Observed">Mediciones</option>
	     <option value="Anomaly">Anomalía</option></select>
           </fieldset>

          <!-- Menu generated by json to list stations according to their variable 'label'. The function labelgeoIdinteresects is directly defined in the link. When you have a more recent version of ingrid, you will be able to take it out. The function has 2 inputs: a resolution in the same format as resolution form/parameter and bbox (optional: by default the World) that constrains to list only labels that fall into bbox, also same format as bbox form/parameter. You may want to adjust default bbox so that it is consistent to the whole Maproom. The rest is fixed. The link must have the class admin (or you can name it otherwise but use same name in the forms declaration. -->
          <fieldset class="navitem">
          <legend>Selección de Estación</legend>	      
          <link class="admin" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/%28irids:SOURCES:Peru:ANA:Embalses:Monthly:ds%29//resolution/parameter/geoobject/%28bb:-80:-55:-67:-17:bb%29//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
          <select class="pageformcopy" name="region">
          <optgroup class="template" label="Label">
          <option class="iridl:values region@value label"></option>
          </optgroup></select>
          </fieldset> 
</div>

<div class="ui-tabs">
    <ul class="ui-tabs-nav">
            <li><a href="#tabs-1" shape="rect">Descripción</a></li>
            <li><a href="#tabs-2" shape="rect">Más información</a></li>
            <li><a href="#tabs-3" shape="rect">Fuente</a></li>
            <li><a href="#tabs-4" shape="rect">Soporte</a></li>
            <li><a href="#tabs-5" shape="rect">Instrucciones</a></li>
    </ul>

         <fieldset class="regionwithinbbox dlimage" about="">
            <a class="dlimgts" rel="iridl:hasTable" href="http://ons.snirh.gob.pe/expert/expert/SOURCES/.Peru/.ANA/.Embalses/.Monthly/.Monthly_level//long_name/%28Volumen%20Observado%29def/SOURCES/.Peru/.ANA/.Embalses/.Monthly/.Monthly_level_percentage_average/%28percent%29unitconvert//long_name/%28Porcentaje%20del%20Promedio%29def//units/%28%25%29def/SOURCES/.Peru/.ANA/.Embalses/.Monthly/.Monthly_level_st_anomaly//long_name/%28Anomalia%20Estandarizada%29def/3/array/astore/%7BT/last/dup/12.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Peru:ANA:Embalses:Monthly:nombre%40Poechos:ds%29//region/parameter/geoobject/.nombre/.first/VALUE%7Dforall//name/%28st_precip%29def/T//long_name/%28Tiempo%29def/4/-3/roll/table:/4/:table/" shape="rect"></a>
            
            
            <div style="float: left;">
               <img class="dlimgloc" src="http://ons.snirh.gob.pe/SOURCES/.WORLDBATH/.bath/X/Y/%28bb:-82%2C-19%2C-65%2C0%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28pt:-75:-5:pt%29//region/parameter/geoobject/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/geometrypushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0.1/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif" />
               
               
            </div>
            
            
            
            <div style="float: left;">
               
               
               
               <div class="valid" style="text-align: top;">
                  <a class="station" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/%28pt:-75:-5:pt%29//region/parameter/geoobject/info.json" shape="rect"></a>
                  
                  
                  <div class="template" style="color : black">
                     Estación <b><span class="iridl:long_name"></span></b>
                     
                     
                  </div>
                  
                  
               </div>
               
               
               
               <div class="valid" style="text-align: top;">
                  <a class="station" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.ANA/.Embalses/.Monthly/nombre/%28irids:SOURCES:Peru:ANA:Embalses:Monthly:nombre%40Poechos:ds%29//region/parameter/geoobject/.nombre/.first/VALUE/lon/lat/2/ds/info.json" shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"
}
}
}
</script>
                  <div>
                     
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="name " rowspan="1" colspan="1"></td>
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                        </tr>
                     </table>
                     
                     
                  </div>
                  
                  
                  
               </div>
               
               
               <div class="template"> <b>Observaciones para el mes actual: </b></div> 
               
               
               <div class="valid" style="text-align: top;">
                  <a class="station" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.ANA/.Embalses/.Monthly/.Monthly_level/T/last/VALUE/nombre/(irids%3ASOURCES%3APeru%3AANA%3AEmbalses%3AMonthly%3Anombre%3Ads)//region/parameter/geoobject/.nombre/.first/VALUE//long_name/(Volumen%20Observado%20(hm3)%3A%20)/def/SOURCES/.Peru/.ANA/.Embalses/.Monthly/.Monthly_level_st_anomaly/T/last/VALUE/nombre/(irids%3ASOURCES%3AChile%3AAnalysis%3APeru%3AANA%3AEmbalses%3AMonthly%3Anombre%3Ads)//region/parameter/geoobject/.nombre/.first/VALUE//long_name/(Anomalia%20del%20Volumen%20Estandarizado%20(-)%3A%20)/def/SOURCES/.Peru/.ANA/.Embalses/.Monthly/.Monthly_level_percentage_average/(percent)/unitconvert/T/last/VALUE/nombre/(irids%3ASOURCES%3AChile%3AAnalysis%3APeru%3AANA%3AEmbalses%3AMonthly%3Anombre%3Ads)//region/parameter/geoobject/.nombre/.first/VALUE//long_name/(Voluemn%20-%20Porcentaje%20del%20Promedio%20(%25)%3A%20)/def/3/ds/info.json" shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"
}
}
}
</script>
                  <div>
                     
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="name " rowspan="1" colspan="1"></td>
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                        </tr>
                     </table>
                     
                     
                  </div>
                  
                  
                  
               </div>
               
               
               
            </div>
            
            <br clear="none" />
            
            
            <div class="dlimgtsbox">
               <img class="dlimgts regionwithinbbox" rel="iridl:hasFigureImage" src="http://ons.snirh.gob.pe/expert/(Percentage)//anal/parameter/(Anomaly)/eq/%7BSOURCES/.Peru/.ANA/.Embalses/.Monthly/.Monthly_level_st_anomaly/T/last/dup/18.0/sub/exch/RANGE/nombre/(irids%3ASOURCES%3APeru%3AANA%3AEmbalses%3AMonthly%3Anombre%40Poechos%3Ads)//region/parameter/geoobject/.nombre/.first/VALUE//long_name/(Anomalia%20del%20Volumen%20Estandarizado)/def//name/(st_discharge)/def/pdsi_colorbars/DATA/AUTO/AUTO/RANGE/dup/T/fig-/colorbars2/-fig%7Dif//anal/get_parameter/(Observed)/eq/%7BSOURCES/.Peru/.ANA/.Embalses/.Monthly/.Monthly_level/T/last/dup/18.0/sub/exch/RANGE/nombre/(irids%3ASOURCES%3APeru%3AANA%3AEmbalses%3AMonthly%3Anombre%40Poechos%3Ads)//region/parameter/geoobject/.nombre/.first/VALUE//long_name/(Volumen%20de%20Embalse%20Observado)/def//name/(st_discharge)/def/dup/T/fig-/colorbars2/-fig%7Dif//anal/get_parameter/(Percentage)/eq/%7BSOURCES/.Peru/.ANA/.Embalses/.Monthly/.Monthly_level_percentage_average/(percent)/unitconvert/100.0/min/T/last/dup/18.0/sub/exch/RANGE/nombre/(irids%3ASOURCES%3APeru%3AANA%3AEmbalses%3AMonthly%3Anombre%40Poechos%3Ads)//region/parameter/geoobject/.nombre/.first/VALUE//long_name/(Volumen%20-%20Porcentaje%20del%20Promedio)/def/name/(st_discharge)/def/pdsi_colorbars/DATA/0.0/100.0/RANGE/dup/T/fig-/colorbars2/-fig%7Dif//plotborder/72/psdef//plotaxislength/432/psdef//plotborder/72/psdef//plotaxislength/432/psdef//plotborder/72/psdef//plotaxislength/432/psdef//nombre/last/plotvalue//plotborder/72/psdef//plotaxislength/432/psdef+.gif" />
               
               
            </div>
            <br clear="none" />
            
            
            
         </fieldset>
         
         
         
         <fieldset class="dlimage" id="content" about="">
            <a class="maptable" rel="iridl:hasTable" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.ANA/.Embalses/.Monthly/location/%28bb:-82:-19:-65:0:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/%28Percentage%29//anal/parameter/%28Anomaly%29eq/%7B.Monthly_level_st_anomaly//long_name/%28Anomalia%20del%20Nivel%20Estandarizado%29def//units/%28-%29def%7Dif//anal/get_parameter/%28Observed%29eq/%7B.Monthly_level//long_name/%28Nivel%20Observado%29def//units/%28m3/s%29def%7Dif//anal/get_parameter/%28Percentage%29eq/%7B.Monthly_level_percentage_average//long_name/%28Nivel%20de%20embalse%20-%20Porcentaje%20del%20Promedio%29def//units/%28%25%29def%7Dif%5Bnombre%5DREORDER/mark/exch/T//long_name/%28Tiempo%29def/exch%5Bnombre%5Dtable:/mark/:table/" shape="rect"></a>
            
            
            <link rel="iridl:hasFigure" href="http://ons.snirh.gob.pe/expert/%28Percentage%29//anal/parameter/%28Percentage%29/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Embalses/.Monthly/lon/lat/2/copy/Monthly_level_percentage_average/-20/masklt/DATA/-4/4/RANGE/6/-1/roll/pop//long_name/%28Volumen%20embalse%20-Porcentaje%20del%20Normal%29/def//name/%28st_precip%29/def/pdsi_colorbars//symmetric/false/def/DATA/0/100/RANGE/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//Y/-19/0/plotrange//st_precip/-3/3/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif//anal/get_parameter/%28Observed%29/eq/%7B/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Embalses/.Monthly/lon/lat/2/copy/Monthly_level//units/%28hm3%29/def/6/-1/roll/pop//long_name/%28Volumen%20de%20Embalse%20Observado%29/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange/%7Dif//anal/get_parameter/%28Anomaly%29/eq/%7B/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Embalses/.Monthly/lon/lat/2/copy/Monthly_level_st_anomaly/-20/masklt/DATA/-4/4/RANGE/6/-1/roll/pop//long_name/%28Anomalia%20del%20Volumen%20del%20Embalse%20Estandarizado%29/def//name/%28st_precip%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//Y/-19/0/plotrange//st_precip/-3/3/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef/" />
            <img class="dlimg" src="http://ons.snirh.gob.pe/expert/%28Percentage%29//anal/parameter/%28Percentage%29/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Embalses/.Monthly/lon/lat/2/copy/Monthly_level_percentage_average/-20/masklt/DATA/-4/4/RANGE/6/-1/roll/pop//long_name/%28Volumen%20embalse%20-Porcentaje%20del%20Normal%29/def//name/%28st_precip%29/def/pdsi_colorbars//symmetric/false/def/DATA/0/100/RANGE/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//Y/-19/0/plotrange//st_precip/-3/3/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif//anal/get_parameter/%28Observed%29/eq/%7B/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Embalses/.Monthly/lon/lat/2/copy/Monthly_level//units/%28hm3%29/def/6/-1/roll/pop//long_name/%28Volumen%20de%20Embalse%20Observado%29/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange/%7Dif//anal/get_parameter/%28Anomaly%29/eq/%7B/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Embalses/.Monthly/lon/lat/2/copy/Monthly_level_st_anomaly/-20/masklt/DATA/-4/4/RANGE/6/-1/roll/pop//long_name/%28Anomalia%20del%20Volumen%20del%20Embalse%20Estandarizado%29/def//name/%28st_precip%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//Y/-19/0/plotrange//st_precip/-3/3/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef/+.gif" border="0" alt="image" /><br clear="none" />
            <img class="dlauximg" src="http://ons.snirh.gob.pe/expert/%28Percentage%29//anal/parameter/%28Percentage%29/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Embalses/.Monthly/lon/lat/2/copy/Monthly_level_percentage_average/-20/masklt/DATA/-4/4/RANGE/6/-1/roll/pop//long_name/%28Volumen%20embalse%20-Porcentaje%20del%20Normal%29/def//name/%28st_precip%29/def/pdsi_colorbars//symmetric/false/def/DATA/0/100/RANGE/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//Y/-19/0/plotrange//st_precip/-3/3/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif//anal/get_parameter/%28Observed%29/eq/%7B/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Embalses/.Monthly/lon/lat/2/copy/Monthly_level//units/%28hm3%29/def/6/-1/roll/pop//long_name/%28Volumen%20de%20Embalse%20Observado%29/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange/%7Dif//anal/get_parameter/%28Anomaly%29/eq/%7B/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Embalses/.Monthly/lon/lat/2/copy/Monthly_level_st_anomaly/-20/masklt/DATA/-4/4/RANGE/6/-1/roll/pop//long_name/%28Anomalia%20del%20Volumen%20del%20Embalse%20Estandarizado%29/def//name/%28st_precip%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//Y/-19/0/plotrange//st_precip/-3/3/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef/+.auxfig/+.gif" />
            
            
         </fieldset>
         
         <div id="tabs-1" class="ui-tabs-panel" about="">
            
            <h3 align="center" property="term:title">Volumen de Embalses Observados</h3>
            
            <p align="left" property="term:description">Este mapa muestra los volúmenes de embalses observados en las cuencas principales del
               Perú. 
            </p>
            
            <p align="left">Selecciona la variable de interés en el menú&gt;<b>análisis</b>: mediciones, porcentaje o anomalía. En el
               menú&gt;<b>región</b> se puede seleccionar la región de interés.
            </p>
            
            
            <p align="left"><b>Mediciones:</b>
               Este mapa muestra elvolumen de embalses observados en las principales cuencas de
               Perú. Las mediciones están en hm3 y están disponibles por cada mes.
            </p>
            
            <p align="left"><b>Porcentaje:</b>
               Este mapa muestra el volumen como porcentaje del volumen normalmente esperado en cada
               mes. El porcentaje indica si hay un déficit o superávit comparada con una situación
               normal.
            </p>
            
            <p align="left"><b>Anomalía estandarizada:</b>
               El mapa muestra los volúmenes observados como anomalía estandarizada. La anomalía estandarizada
               es la diferencia entre el volumen observado en un mes específico y el volumen esperado
               normalmente en el mismo mes, y permite identificar condiciones de déficit y de superávit
               con respecto a lo normal (Tabla 1).
            </p>
            
            <p align="left">
               <h6> Tabla 1: Interpretación de la Anomalía Estandarizada</h6>
            </p>
            
            <p align="left"><img src="Escala_anomalias_caudal_esp.jpg" alt="Leyenda de la Anomalía Estandarizada"> </img></p>
            
            <p align="left">Los datos provienen de los niveles de embalses de la Autoridad Nacional del Agua de
               Perú (ANA).
            </p>
            
            <p align="left">  <img src="../../icons/LogoANA_Azul.png" alt="Logo ANA" /></p>
            
         </div>
         
         
         <div id="tabs-2" class="ui-tabs-panel">
            
            <h3 align="center">¿Cómo se calcula el porcentaje?</h3>
            
            <p>
               El porcentaje se calcula dividiendo el caudal observada en un mes con el caudal normalmente
               esperada en este mes (promedio) multiplicado con cien. 
            </p>
            
            <h3 align="center">¿Cómo se calcula la anomalía?</h3>
            
            <p>La anomalía es el nivel mensual observado en una estación específica menos el promedio
               del caudal mensual (calculado usando datos históricos de la estación específica) dividido
               por la desviación estándar.
               
            </p>
            
         </div>
         
         <div id="tabs-3" class="ui-tabs-panel">
            
            <h3 align="center">Fuente de los Datos</h3>
            
            <p><a href="http://ons.snirh.gob.pe/SOURCES/.Peru/.ANA/.Embalses/.Monthly/" shape="rect">Caudales observados</a>, entregado por la Autoridad Nacional del Agua de Peru, <a href="http://www.ana.gob.pe/" shape="rect">(ANA)</a></p>
            
         </div>
         
         
         <div id="tabs-4" class="ui-tabs-panel">
            
            <h3 align="center">Soporte</h3>
            
            <p>
               Sus consultas a: <a href="mailto:snirh@ana.gob.pe?subject=Nivel Observado Per%C3%BA" shape="rect">snirh@ana.gob.pe</a>
               
            </p>
            
         </div>
         
         <div id="tabs-5" class="ui-tabs-panel">
            
            <h3 align="center">Instrucciones</h3>
            
            <div class="buttonInstructions"></div> 
            
         </div>
         
      </div>
      
      <div class="optionsBar">
         
         <fieldset class="navitem" id="share">
            <legend>Compartir</legend>
         </fieldset>
         
      </div>
      
   </body>
</html>