<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<meta xml:lang="" property="maproom:Entry_Id" content="Monitoring" />
<title>Cantidad de Dias Secos</title>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="DiasSecos.html?Set-Language=en" />
<link class="share" rel="canonical" href="DiasSecos.html" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Meteorological_term"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Example"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_Precipitation_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#monthly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded" />
<link rel="term:icon" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.UnifiedPrecipitation/.Monthly/.NrDaysWithoutRain_anomaly//long_name/%28Anomal%C3%ADa%20de%20d%C3%ADas%20secos%20comparado%20con%20lo%20normal%29/def//units/%28dias%29/def/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//NrDaysWithoutRain_anomaly/-10.0/10.0/plotrange//T/last/plotvalue//Y/-18.5/0/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef//T/last/plotvalue//Y/-18.5/0/plotrange//plotaxislength/432/psdef//plotborder/72/psdef/+.gif" />
<script type="text/javascript" src="../../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../../maproom/unesco.js"></script>
</head>
<body  xml:lang="es">
<form name="pageform" id="pageform" class="carryup carry dlimg dlauximg share">
<input class="carryLanguage carryup carry " name="Set-Language" type="hidden" />
<input class="dlimg admin share" name="bbox" type="hidden" />
<input class="dlimg dlauximg onlyvar dlimgts share" name="anal" type="hidden" />
<input class="dlimg share" name="T" type="hidden" />
<input class="dlimg" name="plotaxislength" type="hidden" />
<input class="share dlimgloc dlimgts" name="region" type="hidden" />
<input class="share dlimgloc dlimgts admin" name="resolution" type="hidden" data-default="irids:SOURCES:Features:Hydrological:Cuencas:Mayores:ds" />
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>Maproom</legend> 
                      <a rev="section" class="navlink carryup" href="/maproom/Monitoring/">Monitoreo</a>
            </fieldset> 
            <fieldset class="navitem" id="chooseSection"> 
                <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Meteorological_term"><span property="term:label">Sequ&#237;a Meteorol&#243;gica</span></legend>
            </fieldset> 
         <fieldset class="navitem">
            <legend>Región</legend>
            <a class="carryLanguage" rel="iridl:hasJSON"
               href="/maproom/globalregionsPeru.json"
               shape="rect"></a>
            <select class="RegionMenu" name="bbox">
               <option value="">Peru</option>
               <optgroup class="template" label="Región">
                  <option class="irigaz:hasPart irigaz:id@value term:label"></option>
               </optgroup></select>
         </fieldset>

         <fieldset class="navitem">
            
            <legend>Análisis</legend><span class="selectvalue"></span><select class="pageformcopy" name="anal">
               <option value="">Cuantos días mas de lo normal fueron secos?</option>
               <option value="Observed">Cual es el número de días sin lluvia este mes?</option></select>
            
         </fieldset>	
         
         <fieldset class="navitem">
            
            <legend>Promedio Espacial Sobre</legend><span class="selectvalue"></span><select class="pageformcopy" name="resolution">
               <option value="0.5">Ubicación puntual</option>
               <option value="irids:SOURCES:Features:Political:Peru:Departamento:ds">Departamento</option>
               <option value="irids:SOURCES:Features:Political:Peru:Provincia:ds">Provincia</option>
               <option value="irids:SOURCES:Features:ANA:AmbAdm:AAA:ds">Autoridad Administrativa del Agua  (AAA)</option>
               <option value="irids:SOURCES:Features:ANA:AmbAdm:ALA:ds">Administración Local de Agua (ALA)</option>
               <option value="irids:SOURCES:Features:Hydrological:Cuencas:Mayores:ds">Unidad Hidrográfica</option></select>       	    
           <link class="admin" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/%28irids:SOURCES:Features:Hydrological:Cuencas:Mayores:ds%29//resolution/parameter/geoobject/%28bb:-85:-19:-65:0:bb%29//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
            <select class="pageformcopy" name="region">
               <optgroup class="template" label="Label">
                  <option class="iridl:values region@value label"></option>
               </optgroup></select>
            
         </fieldset>	
 </div>
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
      <li><a href="#tabs-1" >Descripci&#243;n</a></li>
      <li><a href="#tabs-2" >M&#225;s informaci&#243;n</a></li>
      <li><a href="#tabs-3" >Fuente</a></li>
      <li><a href="#tabs-4" >Soporte</a></li>
      <li><a href="#tabs-5" >Instrucciones</a></li>
    </ul>

         <fieldset class="dlimage regionwithinbbox">
            <a class="dlimgts" rel="iridl:hasTable" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.UnifiedPrecipitation/.SPI%5B%28.%29%28SPI1%29//var/parameter%5Dconcat/interp/%28bb:-70%2C-5.0%2C-69%2C-4.5%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//long_name/%28SPI%29def//units/%28-%29def/T/exch/table-/text/text/skipanyNaN/-table/.html/" shape="rect"></a>
            
            
            <div style="float: left;">
             <!--  <img class="dlimgloc" src="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.Analysis/.MODIS/.Countrymask/.mask_coarse/X/Y/%28bb:-82%2C-19%2C-68%2C1%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-76%2C-8%2C-76.2%2C-8.2%29dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index%5BX/Y%5Dweighted-average/exch/Y/exch%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborder/0/psdef//plotaxislength/120/psdef/+//plotborder+0+psdef//plotaxislength+120+psdef+.gif" /> -->
             <img class="dlimgloc" src="http://ons.snirh.gob.pe/SOURCES/.Peru/.CAZALAC/.MinExpectedPrecip/.5YR/X/Y/%28bb:-82%2C-17%2C-68%2C0%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-76%2C-8%2C-76.2%2C-8.2%29/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborder/0/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotaxislength/120/psdef/+.gif" />  
               
            </div>	
            
            
            
            <div style="float: left;">
               	
               
               
               <div class="valid" style="display: inline-block; text-align: top;">
                  <a class="dlimgts" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json" shape="rect"></a>
                  
                  
                  <div class="template"> Observaciones para <span class="bold iridl:long_name"></span>
                     
                     
                  </div>
                  
                  
               </div>
               
               
               
               
               <div class="valid" style="text-align: top;">
                  
                  
                  <div>Valores para el &#250;ltimo mes: </div>				  
				  <a class="dlimgloc" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.UnifiedPrecipitation/.Monthly/.NrDaysWithoutRain/T/last/VALUE/%28bb:-76%2C-5.0%2C-75.0%2C-4.0%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//long_name/%28N%C3%BAmero%20de%20d%C3%ADas%20sin%20precipitaci%C3%B3n:%20%29def/SOURCES/.Peru/.UnifiedPrecipitation/.Monthly/.NrDaysWithoutRain_anomaly/T/last/VALUE/%28bb:-76%2C-5.0%2C-75.0%2C-4.0%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//long_name/%28Anomal%C3%ADa%20del%20n%C3%BAmero%20de%20d%C3%ADas%20secos:%20%29def/2/ds/info.json" shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "+td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"

}
}
}
</script>
                  <div>
                     
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="name " rowspan="1" colspan="1"></td>
                           <td align="right" class="value " rowspan="1" colspan="1"> dias</td>
                        </tr>
                     </table>
                     
                     
                  </div>
                  
                  
                  
               </div>
               
               
            </div>
            
            <br clear="none" />
            <img class="dlimgts regionwithinbbox" src="http://ons.snirh.gob.pe/expert/%28Anomaly%29//anal/parameter/%28Observed%29/eq/%7B/SOURCES/.Peru/.UnifiedPrecipitation/.Monthly/.NrDaysWithoutRain//long_name/%28N%C3%BAmero%20de%20d%C3%ADas%20sin%20lluvia%29/def//units/%28dias%29/def/%28irids:SOURCES:Features:Hydrological:Cuencas:Mayores:gid%401:ds%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/T/last/dup/18.0/sub/exch/RANGE/dup/T//long_name/%28Tiempo%29/def/fig-/colorbars2/-fig%7Dif//anal/get_parameter/%28Anomaly%29/eq/%7BSOURCES/.Peru/.UnifiedPrecipitation/.Monthly/.NrDaysWithoutRain_anomaly//long_name/%28Anomal%C3%ADa%20de%20d%C3%ADas%20secos%20comparado%20con%20lo%20normal%29/def//units/%28dias%29/def/%28irids:SOURCES:Features:Hydrological:Cuencas:Mayores:gid%401:ds%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/T/last/dup/18.0/sub/exch/RANGE/dup/T//long_name/%28Tiempo%29/def/fig-/colorbars2/-fig%7Dif/+.gif" />  
            
            
         </fieldset> 
         
         
		 
         <fieldset class="dlimage" id="content" about="">
            
            
            <link rel="iridl:hasFigure" href="http://ons.snirh.gob.pe/expert/%28Anomaly%29//anal/parameter/%28Observed%29/eq/%7B/SOURCES/.Peru/.UnifiedPrecipitation/.Monthly/.NrDaysWithoutRain//long_name/%28N%C3%BAmero%20de%20d%C3%ADas%20sin%20lluvia%29/def/SOURCES/.Peru/.UnifiedPrecipitation/.Mask_Peru/.DaysWithoutRain_mask/mul/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//T/last/plotvalue//Y/-18.5/0/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef/%7Dif//anal/get_parameter/%28Anomaly%29/eq/%7B/SOURCES/.Peru/.UnifiedPrecipitation/.Monthly/.NrDaysWithoutRain_anomaly//long_name/%28Anomal%C3%ADa%20de%20d%C3%ADas%20secos%20comparado%20con%20lo%20normal%29/def//units/%28dias%29/def/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//NrDaysWithoutRain_anomaly/-10.0/10.0/plotrange//T/last/plotvalue//Y/-18.5/0/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef%7Dif//T/last/plotvalue//Y/-18.5/0/plotrange//plotaxislength/432/psdef//plotborder/72/psdef/" />
            <img class="dlimg" src="http://ons.snirh.gob.pe/expert/%28Anomaly%29//anal/parameter/%28Observed%29/eq/%7B/SOURCES/.Peru/.UnifiedPrecipitation/.Monthly/.NrDaysWithoutRain//long_name/%28N%C3%BAmero%20de%20d%C3%ADas%20sin%20lluvia%29/def/SOURCES/.Peru/.UnifiedPrecipitation/.Mask_Peru/.DaysWithoutRain_mask/mul/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//T/last/plotvalue//Y/-18.5/0/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef/%7Dif//anal/get_parameter/%28Anomaly%29/eq/%7B/SOURCES/.Peru/.UnifiedPrecipitation/.Monthly/.NrDaysWithoutRain_anomaly//long_name/%28Anomal%C3%ADa%20de%20d%C3%ADas%20secos%20comparado%20con%20lo%20normal%29/def//units/%28dias%29/def/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//NrDaysWithoutRain_anomaly/-10.0/10.0/plotrange//T/last/plotvalue//Y/-18.5/0/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef%7Dif//T/last/plotvalue//Y/-18.5/0/plotrange//plotaxislength/432/psdef//plotborder/72/psdef/+.gif" border="0" alt="image" /><br clear="none" />
            <img class="dlauximg" src="http://ons.snirh.gob.pe/expert/%28Anomaly%29//anal/parameter/%28Observed%29/eq/%7B/SOURCES/.Peru/.UnifiedPrecipitation/.Monthly/.NrDaysWithoutRain//long_name/%28N%C3%BAmero%20de%20d%C3%ADas%20sin%20lluvia%29/def/SOURCES/.Peru/.UnifiedPrecipitation/.Mask_Peru/.DaysWithoutRain_mask/mul/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//T/last/plotvalue//Y/-18.5/0/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef/%7Dif//anal/get_parameter/%28Anomaly%29/eq/%7B/SOURCES/.Peru/.UnifiedPrecipitation/.Monthly/.NrDaysWithoutRain_anomaly//long_name/%28Anomal%C3%ADa%20de%20d%C3%ADas%20secos%20comparado%20con%20lo%20normal%29/def//units/%28dias%29/def/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//NrDaysWithoutRain_anomaly/-10.0/10.0/plotrange//T/last/plotvalue//Y/-18.5/0/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef%7Dif//T/last/plotvalue//Y/-18.5/0/plotrange//plotaxislength/432/psdef//plotborder/72/psdef/+.auxfig/+.gif" />
            
            
         </fieldset>         

 <div id="tabs-1" class="ui-tabs-panel" about="">
<h3 align="center"  property="term:title" >N&#250;mero de D&#237;as sin Lluvia</h3>
<p align="left" property="term:description">Este mapa muestra el n&#250;mero de d&#237;as sin lluvia en un mes. Se considera que un d&#237;a fue lluvioso si la fuente detect&#243; una precipitaci&#243;n m&#237;nima de 1 mm.</p>
<p align="left">Uno puede utilizar las opciones en la parte superior para  observar la informaci&#243;n a nivel de provincial con la opci&#243;n "Regi&#243;n", o realizar un promedio espacial sobre una unidad geografica con la opci&#243;n "Promedio Espacial Sobre"</p>
</div>

<div id="tabs-2"  class="ui-tabs-panel">
<h3  align="center">&#191;C&#243;mo se calcula ?</h3>
<p align="left">Se calcula acumulando los d&#237;as en los que llovi&#243; por lo menos 1 mm para cada grilla y se acumula hasta el fin del mes.</p>
</div>

<div id="tabs-3" class="ui-tabs-panel">
<h3  align="center">Fuente de los Datos</h3>
<p align="left"><a href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.UnifiedPrecipitation/.SPI/">Unified Precipitation</a>, entregado por <a href="http://www.esrl.noaa.gov/psd/data/gridded/data.unified.html">NOAA NCEP Climate Prediction Center</a></p>
</div>

<div id="tabs-4"  class="ui-tabs-panel">
<h3  align="center">Soporte</h3>
<p>
Sus consultas a: <a href="mailto:snirh@ana.gob.pe?subject=Precipitacion Observada Per&#250;">snirh@ana.gob.pe</a>
 </p>
</div>
<div id="tabs-5"  class="ui-tabs-panel">
<h3  align="center">Instrucciones</h3>
<div class="buttonInstructions"></div>
</div>

</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartit</legend></fieldset>
</div>
 </body>
 </html>
