<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      xmlns:og="http://ogp.me/ns#" 
      version="XHTML+RDFa 1.0"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<meta xml:lang="" property="maproom:Entry_Id" content="Monitoring" />
      <title>Precipitación Observada METEONET</title>
      <link rel="stylesheet" type="text/css" href="../../unesco.css" />
      <link class="altLanguage" rel="alternate" hreflang="en" href="Precipitation_METEONET.html?Set-Language=en" />
      <link class="share" rel="canonical" href="Precipitation_METEONET.html" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Meteorological_term" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
      <link rel="term:icon" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Tiempo_real/.Precipitacion/.Mensual/lon/lat/2/copy/Monthly_precip/-20/masklt/DATA/0/AUTO/RANGE/6/-1/roll/pop//long_name/(Precipitacion%20observada)/def//name/(st_precip)/def/precip_colors/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//Y/-19/0/plotrange//st_precip/-3/3/plotrange/(antialias)/true/psdef/(fntsze)/20/psdef//antialias/true/psdef//fntsze/20/psdef//plotaxislength/432/psdef//color_smoothing/1/psdef//plotborder/0/psdef+.gif" />
     <script type="text/javascript" src="../../../uicore/uicore.js"></script>
     <script type="text/javascript" src="../../../maproom/unesco.js"></script>
      </head>
   <body xml:lang="es">
      
      
      <form name="pageform" id="pageform" class="carryup carry dlimg dlauximg share" method="get" enctype="application/x-www-form-urlencoded">
         <input class="carryLanguage carryup carry " name="Set-Language" type="hidden" />
         <input class="dlimg dlimgloc admin maptable share" name="bbox" type="hidden" />
         <input class="dlimg share" name="T" type="hidden" />
         <input class="dlimg" name="plotaxislength" type="hidden" />
         <input class="share dlimgts dlimgloc dlimglocclick station" name="region" type="hidden" />
         <input class="transformRegion dlimglocclick" name="clickpt" type="hidden" />
         <input class="dlimg dlimgts dlauximg maptable share" name="anal" type="hidden" />
         <input class="pickarea dlimgts admin" name="resolution" type="hidden" value="irids:SOURCES:Peru:ANA:Tiempo_real:Precipitacion:Mensual:ds" />
         
         
         <!-- list of layers form with names corresponding to different layers of the image so that they can be un/checked by default. I leave it commented out since as of now scatterlabel is not considered a layer
<input class="dlimg share" name="layers" value="Discharge" checked="checked" type="checkbox" />
<input class="dlimg share" name="layers" value="aprod" checked="checked" type="checkbox" />
<input class="dlimg share" name="layers" value="label" checked="unchecked" type="checkbox" />
<input class="dlimg share" name="layers" value="rivers_gaz" type="checkbox" />
<input class="dlimg share" name="layers" value="coasts_gaz" checked="checked" type="checkbox" />
<input class="dlimg share" name="layers" value="countries_gaz" type="checkbox" checked="checked" />
-->
         
         
      </form>
      
      
      <div class="controlBar">
         
         
         <fieldset class="navitem" id="toSectionList"> 
            
            
            <legend>Maproom</legend> 
            <a rev="section" class="navlink carryup" href="/maproom/Monitoring/" shape="rect">Monitoreo</a>
            
            
         </fieldset> 
         
         
         <fieldset class="navitem" id="chooseSection"> 
            
            
            <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Meteorological_term"><span property="term:label">Sequía Meteorológica</span></legend>
            
            
         </fieldset> 
         
         
         <fieldset class="navitem">
            
            
            <legend>Región</legend>
            <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsPeru.json" shape="rect"></a>
            <select class="RegionMenu" name="bbox">
               
               <option value="">Peru</option>
               
               <optgroup class="template" label="Región">
                  
                  <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                  
               </optgroup></select>
            
            
         </fieldset>
         
         
         <fieldset class="navitem">
            		
            
            <legend>Analysis</legend>
            	<span class="selectvalue"></span><select class="pageformcopy" name="anal">
               
               <option value="">Mediciones</option>
               
               <!-- <option value="Percentage">Porcentaje</option>
               
               <option value="Anomaly">Anomalía</option> HASTA QUE SE SOLUCIONE EL PROBLEMA CON EL CALCULO DEL PORCENTAJE Y ANOMALIA -->
         </select></fieldset>
         
         
         <!-- Menu generated by json to list stations according to their variable 'label'. The function labelgeoIdinteresects is directly defined in the link. When you have a more recent version of ingrid, you will be able to take it out. The function has 2 inputs: a resolution in the same format as resolution form/parameter and bbox (optional: by default the World) that constrains to list only labels that fall into bbox, also same format as bbox form/parameter. You may want to adjust default bbox so that it is consistent to the whole Maproom. The rest is fixed. The link must have the class admin (or you can name it otherwise but use same name in the forms declaration. -->
         
         
         <!--fieldset class="navitem">
            
            
            <legend>Selección de Estación</legend>	
            
            
            <link class="admin" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/%28irids%3ASOURCES%3APeru%3AANA%3ATiempo_real%3APrecipitacion%3AMensual%3Ads%29//resolution/parameter/geoobject/%28bb%3A-82%3A-19%3A-65%3A0%3Abb%29//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
            <select class="pageformcopy" name="region">
               
               <optgroup class="template" label="Label">
                  
                  <option class="iridl:values region@value label"></option>
                  
               </optgroup></select>
            
            
         </fieldset--> 
		 
		 <fieldset class="navitem">
            
            <legend>Selección de Estación</legend>	      
            
            <link class="admin" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/%28irids:SOURCES:Peru:ANA:Embalses:Monthly:ds%29//resolution/parameter/geoobject/%28bb:-80:-55:-67:-17:bb%29//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
            <select class="pageformcopy" name="region">
               <optgroup class="template" label="Label">
                  <option class="iridl:values region@value label"></option>
               </optgroup></select>
            
         </fieldset>
         
         
         
      </div>
      
      
      <div class="ui-tabs">
         
         
         <ul class="ui-tabs-nav">
            
            <li><a href="#tabs-1" shape="rect">Descripción</a></li>
            
            <li><a href="#tabs-2" shape="rect">Más información</a></li>
            
            <li><a href="#tabs-3" shape="rect">Fuente</a></li>
            
            <li><a href="#tabs-4" shape="rect">Soporte</a></li>
            
            <li><a href="#tabs-5" shape="rect">Instrucciones</a></li>
            
         </ul>
         
         
         
         
         <fieldset class="regionwithinbbox dlimage" about="">
		<!--Problema-->
            <a class="dlimgts" rel="iridl:hasTable" href="http://ons.snirh.gob.pe/expert/expert/SOURCES/.Peru/.ANA/.Tiempo_real/.Precipitacion/.Mensual/.Monthly_precip//long_name/%28Precipitacion%20Observada%29def//units/%28mm/mes%29def/SOURCES/.Peru/.ANA/.Tiempo_real/.Precipitacion/.Mensual/.monthly_climatology//long_name/%28Precipitacion%20Promedio%29def//units/%28mm/mes%29def/SOURCES/.Peru/.ANA/.Tiempo_real/.Precipitacion/.Mensual/.Monthly_precip_percentage/%28percent%29unitconvert/dup/dataflag/0.0/maskle/exch/100.0/min/mul//long_name/%28Porcentaje%20del%20Promedio%29def//units/%28%25%29def/SOURCES/.Peru/.ANA/.Tiempo_real/.Precipitacion/.Mensual/.Monthly_precip/yearly-anomalies//long_name/%28Anomalia%20Estandarizada%29def/4/array/astore/%7Bnombre/%28irids:SOURCES:Peru:ANA:Tiempo_real:Precipitacion:Mensual:nombre%40CAPLINA:ds%29//region/parameter/geoobject/.nombre/.first/VALUE%7Dforall//name/%28st_precip%29def/T/5/-4/roll/table:/5/:table/" shape="rect"></a>
            
            
            
            
            <div style="float: left;">
               <img class="dlimgloc" src="http://ons.snirh.gob.pe/SOURCES/.Peru/.CAZALAC/.MinExpectedPrecip/.5YR/X/Y/%28bb:-82%2C-17%2C-68%2C0%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-76%2C-8%2C-76.2%2C-8.2%29/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborder/0/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotaxislength/120/psdef/+.gif" />
               
               
               
               
            </div>
            
            
            
            
            
            <div style="float: left;">
               
               
               
               
               
               <div class="valid" style="text-align: top;">
                  <a class="station" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/%28pt:-75:-5:pt%29//region/parameter/geoobject/info.json" shape="rect"></a>
                  
                  
                  
                  
                  <div class="template" style="color : black">
                     Estación METEONET <b><span class="iridl:long_name"></span></b>
                     
                     
                     
                     
                  </div>
                  
                  
                  
                  
               </div>
               
               
               
               
               
               <div class="valid" style="text-align: top;">
                  <a class="station" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.ANA/.Tiempo_real/.Precipitacion/.Mensual/nombre/%28irids:SOURCES:Peru:ANA:Tiempo_real:Precipitacion:Mensual:nombre%40CAPLINA:ds%29//region/parameter/geoobject/.nombre/.first/VALUE/lon/lat/2/ds/info.json" shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"
}
}
}
</script>
                  <div>
                     
                     
                     
                     <table class="valid template">
                        
                        <tr style="color : black">
                           
                           <td class="name " rowspan="1" colspan="1"></td>
                           
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                           
                        </tr>
                        
                     </table>
                     
                     
                     
                     
                  </div>
                  
                  
                  
                  
                  
               </div>
               
               
               
               
               <div class="template"> <b>Observaciones para el mes actual: </b></div> 
               
               
               
               
               <div class="valid" style="text-align: top;">
                  <a class="dlimgloc" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.ANA/.Tiempo_real/.Precipitacion/.Mensual/.Monthly_precip/yearly-anomalies/T/last/VALUE/nombre/%28irids:SOURCES:Peru:ANA:Tiempo_real:Precipitacion:Mensual:nombre%40CAPLINA:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Anomal%C3%ADa%20de%20la%20Precipitaci%C3%B3n%20Estandarizada%20%28-%29:%20%29def/SOURCES/.Peru/.ANA/.Tiempo_real/.Precipitacion/.Mensual/.Monthly_precip_percentage/%28percent%29unitconvert/dup/dataflag/0.0/maskle/exch/100.0/min/mul/T/last/VALUE/nombre/%28irids:SOURCES:Peru:ANA:Tiempo_real:Precipitacion:Mensual:nombre%40CAPLINA:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Precipitaci%C3%B3n%20-%20Porcentaje%20del%20Promedio%20%28%25%29:%20%29def/SOURCES/.Peru/.ANA/.Tiempo_real/.Precipitacion/.Mensual/.Monthly_precip/T/last/VALUE/nombre/%28irids:SOURCES:Peru:ANA:Tiempo_real:Precipitacion:Mensual:nombre%40CAPLINA:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Precipitaci%C3%B3n%20Observada%20%28mm/mes%29:%20%29def/3/ds/info.json" shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"

}
}
}
</script>
                  <div>
                     
                     
                     
                     <table class="valid template">
                        
                        <tr style="color : black">
                           
                           <td class="name " rowspan="1" colspan="1"></td>
                           
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                           
                        </tr>
                        
                     </table>
                     
                     
                     
                     
                  </div>
                  
                  
                  
                  
                  
               </div>
               
               
               
               
               
            </div>
            
            <br clear="none" />
            
            
            
            
            <div class="dlimgtsbox">
               <img class="dlimgts regionwithinbbox" rel="iridl:hasFigureImage" src="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.ANA/.Tiempo_real/.Precipitacion/.Mensual/.Monthly_precip//units/%28mm/mes%29def/T/last/dup/18.0/sub/exch/RANGE/nombre/%28irids:SOURCES:Peru:ANA:Tiempo_real:Precipitacion:Mensual:nombre%40CAPLINA:ds%29//region/parameter/geoobject/.nombre/.first/VALUE//long_name/%28Precipitacion%20Observada%29def/dup/T/fig-/colorbars2/-fig/nombre/last/plotvalue+.gif" />
               
               
               
               
            </div>
            <br clear="none" />
            
            
            
            <!--TAL VEZ PARA OTRA OPORTUNIDAD-->
            <div class="dlimgtsbox">
               <img class="dlimgts regionwithinbbox" rel="iridl:hasFigureImage" src="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.SENAMHI/.Monthly/.precip/ESTID/%28irids:SOURCES:Peru:SENAMHI:Monthly:ds%29//region/parameter/geoobject/.ESTID/.first/VALUE/T/12/splitstreamgrid/dup%5BT2%5Daverage/exch%5BT2%5D0.05/0.5/0.95/0/replacebypercentile/a:/percentile/0.05/VALUE/percentile/removeGRID//fullname/%28Minimo%20Esperado%29def/:a:/percentile/0.5/VALUE/percentile/removeGRID//fullname/%28Normal%29def/:a:/percentile/0.95/VALUE/percentile/removeGRID//fullname/%28Maximo%20esperado%29def/:a/2/index/0.0/mul/dup/6/-1/roll/exch/6/-2/roll/5/index//long_name/%28Precipitacion%20Mensual%29def//units/%28mm/mes%29def/SOURCES/.Peru/.SENAMHI/.Monthly/.precip/ESTID/%28irids:SOURCES:Peru:SENAMHI:Monthly:ds%29//region/parameter/geoobject/.ESTID/.first/VALUE/T/last/dup/18/sub/exch/RANGE//fullname/%28Precipitacion%20Observada%29def/DATA/0/AUTO/RANGE/6/5/roll/pop/7/-6/roll/6/array/astore/%7B%5BT%5DregridLinear%7Dforall/7/-1/roll/5/-4/roll/T/fig-/white/deltabars/grey/deltabars/solid/medium/green/line/blue/line/red/line/-fig//framelabel/%28Precipitacion%20Observada%20y%20Climatologia%29psdef//plotborderbottom/40/psdef//antialias/true/def/ESTID/last/plotvalue/+.gif" />
               
               
               
               
            </div>
            
            
            
            
         </fieldset>
         
         
         
         
         <fieldset class="dlimage" id="content" about="">
            <a rel="iridl:hasTable" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.ANA/.Tiempo_real/.Precipitacion/.Mensual/location/%28bb:-82:-19:-65:0:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/%28Percentage%29//anal/parameter/%28Anomaly%29eq/%7B.Monthly_precip/yearly-anomalies//long_name/%28Anomalia%20Estadarizada%29def//long_name/%28Station%20Name%29def//units/%28-%29def%7Dif//anal/get_parameter/%28Observed%29eq/%7B.Monthly_precip//long_name/%28Precipitacion%20Observada%29def//units/%28mm/mes%29def%7Dif//anal/get_parameter/%28Percentage%29eq/%7B.Monthly_precip_percentage//long_name/%28Porcentaje%20del%20Promedio%29def//units/%28%25%29def%7Dif%5Bnombre%5DREORDER/mark/exch/T//full_name/%28Tiempo%29def/exch%5Bnombre%5Dtable:/mark/:table/" shape="rect"></a> 
            
            
            
            
            <link rel="iridl:hasFigure" href="http://ons.snirh.gob.pe/expert/%28Observed%29//anal/parameter/%28Percentage%29/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Tiempo_real/.Precipitacion/.Mensual/lon/lat/2/copy/Monthly_precip_percentage/-20/masklt/DATA/-4/4/RANGE/6/-1/roll/pop//long_name/%28Precipitacion-Porcentaje%20del%20Promedio%29/def//name/%28st_precip%29/def/pdsi_colorbars//symmetric/false/def/DATA/0/100/RANGE/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//Y/-19/0/plotrange//st_precip/-3/3/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif//anal/get_parameter/%28Observed%29/eq/%7B/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Tiempo_real/.Precipitacion/.Mensual/lon/lat/2/copy/Monthly_precip//units/%28mm/mes%29/def/6/-1/roll/pop//long_name/%28Precipitacion%20Observada%29/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange/%7Dif//anal/get_parameter/%28Anomaly%29/eq/%7B/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Tiempo_real/.Precipitacion/.Mensual/lon/lat/2/copy/.Monthly_precip/yearly-anomalies/-20/masklt/DATA/-4/4/RANGE/6/-1/roll/pop//long_name/%28Anomalia%20de%20la%20%20Precipitacion%20Estadarizada%29/def//name/%28st_precip%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//Y/-19/0/plotrange//st_precip/-3/3/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef/" />
            <img class="dlimg" src="http://ons.snirh.gob.pe/expert/%28Observed%29//anal/parameter/%28Percentage%29/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Tiempo_real/.Precipitacion/.Mensual/lon/lat/2/copy/Monthly_precip_percentage/-20/masklt/DATA/-4/4/RANGE/6/-1/roll/pop//long_name/%28Precipitacion-Porcentaje%20del%20Promedio%29/def//name/%28st_precip%29/def/pdsi_colorbars//symmetric/false/def/DATA/0/100/RANGE/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//Y/-19/0/plotrange//st_precip/-3/3/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif//anal/get_parameter/%28Observed%29/eq/%7B/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Tiempo_real/.Precipitacion/.Mensual/lon/lat/2/copy/Monthly_precip//units/%28mm/mes%29/def/6/-1/roll/pop//long_name/%28Precipitacion%20Observada%29/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange/%7Dif//anal/get_parameter/%28Anomaly%29/eq/%7B/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Tiempo_real/.Precipitacion/.Mensual/lon/lat/2/copy/.Monthly_precip/yearly-anomalies/-20/masklt/DATA/-4/4/RANGE/6/-1/roll/pop//long_name/%28Anomalia%20de%20la%20%20Precipitacion%20Estadarizada%29/def//name/%28st_precip%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//Y/-19/0/plotrange//st_precip/-3/3/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef/+.gif" border="0" alt="image" /><br clear="none" />
            <img class="dlauximg" src="http://ons.snirh.gob.pe/expert/%28Observed%29//anal/parameter/%28Percentage%29/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Tiempo_real/.Precipitacion/.Mensual/lon/lat/2/copy/Monthly_precip_percentage/-20/masklt/DATA/-4/4/RANGE/6/-1/roll/pop//long_name/%28Precipitacion-Porcentaje%20del%20Promedio%29/def//name/%28st_precip%29/def/pdsi_colorbars//symmetric/false/def/DATA/0/100/RANGE/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//Y/-19/0/plotrange//st_precip/-3/3/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif//anal/get_parameter/%28Observed%29/eq/%7B/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Tiempo_real/.Precipitacion/.Mensual/lon/lat/2/copy/Monthly_precip//units/%28mm/mes%29/def/6/-1/roll/pop//long_name/%28Precipitacion%20Observada%29/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange/%7Dif//anal/get_parameter/%28Anomaly%29/eq/%7B/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Tiempo_real/.Precipitacion/.Mensual/lon/lat/2/copy/.Monthly_precip/yearly-anomalies/-20/masklt/DATA/-4/4/RANGE/6/-1/roll/pop//long_name/%28Anomalia%20de%20la%20%20Precipitacion%20Estadarizada%29/def//name/%28st_precip%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//Y/-19/0/plotrange//st_precip/-3/3/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef/+.auxfig/+.gif" />
            
            
            
            
         </fieldset>
         
         
         
         <div id="tabs-1" class="ui-tabs-panel" about="">
            
            
            <h3 align="center" property="term:title">Precipitación Observada - METEONET</h3>
            
            
            <p align="left" property="term:description">Este mapa muestra la precipitación observada en las estaciones meteorológicas del
               METEONET del Perú  (ANA).  Las observaciones tienen la unidad [mm/mes].
               
            </p>
            
            
            <!-- <p align="left">Seleccione porcentaje o mediciones en el menú&gt;análisis para visualizar la variable
               requerida mediciones o porcentaje. En menú &gt; <b>región</b> se puede seleccionar la región
               de interés. Utiliza el navegador de tiempo para consultar el mes de interés.
               
            </p>
            
            
            <p align="left"><b>Mediciones:</b>
               Este mapa muestra la precipitación, observada en las diferentes estaciones automáticas
               de la DGA. Las observaciones tienen la unidad [mm/mes].
               
            </p>
            
            
            <p align="left"><b>Porcentaje:</b>
               Este mapa muestra la precipitación normal esperada en cada mes como porcentaje. Este
               valor indica si hay un déficit o superávit comparada con una situación normal.
               
            </p>
            
            
            <p align="left"><b>Anomalía estandarizada:</b>
               El mapa muestra la precipitación observada como anomalía estandarizada. La anomalía
               estandarizada es la diferencia entre la precipitación observada en un mes específico
               y la precipitación esperada normalmente en el mismo mes, y permite identificar condiciones
               de déficit y de superávit con respecto a lo normal (Tabla 1).
               
            </p>
            
            
            <p align="left">
               
               <h6> Tabla 1: Interpretación de la Anomalía Estandarizada</h6>
               
            </p>
            
            
            <p align="left"><img src="Escala_anomalias_esp.jpg" alt="Legend of the Standardized Anomalies"> </img></p> -->
            
            
            <p align="left"><b>Fuente de Datos</b> </p>
            
            <p align="left">Los datos usados para el cálculo provienen de estaciones meteorológicas del METEONET (ANA). </p>
            
            <p align="left"><a href="http://meteonet.ana.gob.pe/webview/login.asp" shape="rect"><img src="../../icons/LogoANA_Azul.png" alt="Logo ANA" /></a></p>
                        
            
         </div>
         
         
         
         <div id="tabs-2" class="ui-tabs-panel">
            
            
            <!-- <h3 align="center">¿Cómo se calcula el porcentaje?</h3>
            
            
            <p>El porcentaje se calcula dividiendo la precipitación observada en un mes con la precipitación
               normalmente esperada en este mes (promedio) multiplicado con cien. 
               
            </p>
            
            
            <h3 align="center">¿Cómo se calcula la anomalía?</h3>
            
            
            <p>La anomalía es la precipitación mensual observada en una estación específica menos
               el promedio de la precipitación mensual (calculado usando datos históricos de la estación
               específica) dividido por la desviación estándar.
               
               
            </p> -->
            <p>Para tener más información sobre las estaciones del METEONET, escribir al correo snirh@ana.gob.pe 
               
            </p>			
            
            
         </div>
         
         
         
         <div id="tabs-3" class="ui-tabs-panel">
            
            
            <h3 align="center">Fuente de los Datos</h3>
            
            
            <p align="left"><a href="http://ons.snirh.gob.pe/SOURCES/.Peru/.ANA/.Tiempo_real/.Precipitacion/.Mensual/.Monthly_precip/" shape="rect">Precipitación observada en estaciones</a>, entregado por METEONET <a href="http://ana.gob.pe/" shape="rect">(ANA)</a>
               
               
            </p>
            
            
         </div>
         
         
         
         <div id="tabs-4" class="ui-tabs-panel">
            
            
            <h3 align="center">Soporte</h3>
            
            
            <p>
               Sus consultas a: <a href="mailto:snirh@ana.gob.pe?subject=Precipitacion Observada Peru" shape="rect">snirh@ana.gob.pe</a>
               
               
            </p>
            
            
         </div>
         
         
         <div id="tabs-5" class="ui-tabs-panel">
            
            
            <h3 align="center">Instrucciones</h3>
            
            
            <div class="buttonInstructions"></div>
            
            
         </div>
         
         
      </div>
      
      
      <div class="optionsBar">
         
         
         <fieldset class="navitem" id="share">
            
            <legend>Share</legend>
            
         </fieldset>
         
         
      </div>
      
      
   </body>
</html>