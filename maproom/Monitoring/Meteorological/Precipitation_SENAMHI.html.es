<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<meta xml:lang="" property="maproom:Entry_Id" content="Monitoring" />
      <title>Precipitación Observada SENAMHI</title>
      <link rel="stylesheet" type="text/css" href="../../unesco.css" />
      <link class="altLanguage" rel="alternate" hreflang="en" href="Precipitation_SENAMHI.html?Set-Language=en" />
      <link class="share" rel="canonical" href="Precipitation_SENAMHI.html" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Meteorological_term" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
      <link rel="term:icon" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/.precip_percentage/SOURCES/.Peru/.SENAMHI/.PISCO/.Peru_mask/.SPI_mask/mul//long_name/%28Precipitacion-Porcentaje%20del%20Promedio%29/def/pdsi_colorbars//symmetric/false/def/DATA/0/100/RANGE/T/last/VALUE/X/Y/fig-/colors/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange+//plotborder+0+psdef//plotaxislength+432+psdef//fntsze+20+psdef//antialias+true+psdef//color_smoothing+1+psdef+.gif" />
<script type="text/javascript" src="../../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../../maproom/unesco.js"></script>
<style>
.dlimage.ver2 {
        width: 24%;
        float: right;
}
.dlimagei.ver3 {
        width: 99%;
}
.dlimage.ver4 {
        width: 33%;
}
.dlimgtsbox {
width: 99%;
display: inline-block
 }
</style>
</head>
   <body xml:lang="es">
      
<form name="pageform" id="pageform" class="carryup carry dlimg dlauximg share">
<input class="carryLanguage carryup carry " name="Set-Language" type="hidden" />
<input class="dlimg dlimgloc admin maptable share" name="bbox" type="hidden" />
<input class="dlimg dlimgts dlauximg maptable share" name="anal" type="hidden" />
<input class="dlimg share" name="T" type="hidden" />
<input class="dlimg" name="plotaxislength" type="hidden" />
<input class="share dlimgloc dlimgts" name="region" type="hidden" />
<input class="share dlimgloc dlimgts admin" name="resolution" type="hidden" data-default="irids:SOURCES:Features:Hydrological:Cuencas:Mayores:ds" />
</form>
      
      <div class="controlBar">
         
         <fieldset class="navitem" id="toSectionList"> 
            
            <legend>Maproom</legend> 
            <a rev="section" class="navlink carryup" href="/maproom/Monitoring/" shape="rect">Monitoreo</a>
            
         </fieldset> 
         
         <fieldset class="navitem" id="chooseSection"> 
            
            <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Meteorological_term"><span property="term:label">Sequía Meteorológica</span></legend>
            
         </fieldset> 
         
         <fieldset class="navitem">
            
            <legend>Región</legend>
            <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsPeru.json" shape="rect"></a>
            <select class="RegionMenu" name="bbox">
               <option value="">Peru</option>
               <optgroup class="template" label="Región">
                  <option class="irigaz:hasPart irigaz:id@value term:label"></option>
               </optgroup></select>
            
         </fieldset>
         
<fieldset class="navitem">
            		
            
            <legend>Analysis</legend>
            	<span class="selectvalue"></span><select class="pageformcopy" name="anal">
               <option value="">Porcentaje</option>
               <option value="Observed">Mediciones</option>
               <option value="Anomaly">Anomalía</option></select>
	  </fieldset>
         
	  <fieldset class="navitem">
            
            
            <legend>Promedio Espacial Sobre</legend><span class="selectvalue"></span><select class="pageformcopy" name="resolution">
               <option value="0.5">Ubicación puntual</option>
               <option value="irids:SOURCES:Features:Political:Peru:Departamento:ds">Departamento</option>
               <option value="irids:SOURCES:Features:Political:Peru:Provincia:ds">Provincia</option>
               <option value="irids:SOURCES:Features:ANA:AmbAdm:AAA:ds">Autoridad Administrativa del Agua  (AAA)</option>
               <option value="irids:SOURCES:Features:ANA:AmbAdm:ALA:ds">Administración Local de Agua (ALA)</option>
               <option value="irids:SOURCES:Features:Hydrological:Cuencas:Mayores:ds">Unidad Hidrográfica</option></select>       	    
            <link class="admin" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/%28irids:SOURCES:Features:Hydrological:Cuencas:Mayores:ds%29//resolution/parameter/geoobject/%28bb:-85:-19:-65:0:bb%29//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
            <select class="pageformcopy" name="region">
               <optgroup class="template" label="Label">
                  <option class="iridl:values region@value label"></option>
               </optgroup></select>            
         </fieldset>	
         
         
      </div>
      
      <div class="ui-tabs">
         
         <ul class="ui-tabs-nav">
            <li><a href="#tabs-1" shape="rect">Descripción</a></li>
            <li><a href="#tabs-2" shape="rect">Más información</a></li>
            <li><a href="#tabs-3" shape="rect">Fuente</a></li>
            <li><a href="#tabs-4" shape="rect">Soporte</a></li>
            <li><a href="#tabs-5" shape="rect">Instrucciones</a></li>
         </ul>
         
         
         
         <fieldset class="dlimage regionwithinbbox">
         
            
            
            <div style="float: left;">
               <img class="dlimgloc" src="http://ons.snirh.gob.pe/SOURCES/.Peru/.CAZALAC/.MinExpectedPrecip/.5YR/X/Y/%28bb:-82%2C-17%2C-68%2C0%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-76%2C-8%2C-76.2%2C-8.2%29/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborder/0/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotaxislength/120/psdef/+.gif" />
               
               
               
            </div>	
            
            
            
            
            <div style="float: left;">
               	
               
               
               
               <div class="valid" style="display: inline-block; text-align: top;">
                  <a class="dlimgts" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json" shape="rect"></a>
                  
                  
                  
                  <div class="template"> Observacioness para <span class="bold iridl:long_name"></span>
                     
                     
                     
                  </div>
                  
                  
                  
               </div>
               
               
               
               
               
               <div class="valid" style="text-align: top;">
                  
                  
                  
                  <div>Valores para el mes actual: </div> 
                  <a class="dlimgloc" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/.Precipitation/T/last/VALUE/(bb%3A-71.5%2C-5.0%2C-71.0%2C-4.5)//region/parameter/geoobject/%5BX/Y%5Dweighted-average//long_name/(Precipitaci%C3%B3n%20Observada%3A%20)/def/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/.precip_st_anomaly/T/last/VALUE/(bb%3A-71.5%2C-5.0%2C-71.0%2C-4.5)//region/parameter/geoobject/%5BX/Y%5Dweighted-average//long_name/(Anomal%C3%ADa%20de%20la%20%20Precipitaci%C3%B3n%20Estadarizada%3A%20)/def/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/.precip_percentage/T/last/VALUE/(bb%3A-71.5%2C-5.0%2C-71.0%2C-4.5)//region/parameter/geoobject/%5BX/Y%5Dweighted-average//long_name/(Precipitaci%C3%B3n-Porcentaje%20del%20Promedio%3A%20)/def/3/ds/info.json" shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "+td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"

}
}
}
</script>
                  <div>
                     
                     
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="name " rowspan="1" colspan="1"></td>
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                        </tr>
                     </table>
                     
                     
                     
                  </div>
                  
                  
                  
                  
               </div>
               
               
               
            </div>
            
            <br clear="none" />
            <img class="dlimgts regionwithinbbox" src="http://ons.snirh.gob.pe/expert/%28Percentage%29//anal/parameter/%28Percentage%29/eq/%7B/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/precip_percentage/100/min/%28bb:-71.5%2C-5.0%2C-71.0%2C-4.5%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average//long_name/%28Precipitaci%C3%B3n-Porcentaje%20del%20Promedio%29/def//name/%28st_precip%29/def/pdsi_colorbars//symmetric/false/def/DATA/0/AUTO/RANGE/T/last/dup/18/sub/exch/RANGE/dup/T/fig-/colorbars2/-fig%7Dif//anal/get_parameter/%28Observed%29/eq/%7B/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/.Precipitation/%28bb:-71.5%2C-5.0%2C-71.0%2C-4.5%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average//long_name/%28Precipitacin%20%C3%93bservada%29/def/T/last/dup/18/sub/exch/RANGE/DATA/AUTO/AUTO/RANGE/dup/T/fig-/colorbars2/-fig/%7Dif//anal/get_parameter/%28Anomaly%29/eq/%7B/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/.precip_st_anomaly/%28bb:-71.5%2C-5.0%2C-71.0%2C-4.5%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average//name/%28st_precip%29/def//long_name/%28Anomalia%20de%20la%20%20Precipitacion%20Estadarizada%29/def/T/last/dup/18/sub/exch/RANGE/pdsi_colorbars/dup/T/fig-/colorbars2/-fig//st_precip/-3/3/plotrange//plotlast/3/def//plotfirst/-3/def/%7Dif/+.gif" />  
            
            
            
         </fieldset> 
         
         
         
         
         <fieldset class="dlimage" id="content" about="">
            <a rel="iridl:hasTable" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.SENAMHI/.Monthly/location/%28bb:-82:-19:-65:0:bb%29//bbox/parameter/geoobject/geometryintersects/0/maskle/SELECT/%28Percentage%29//anal/parameter/%28Anomaly%29eq/%7B.precip_st_anomaly//long_name/%28Anomalia%20Estadarizada%29def//long_name/%28Station%20Name%29def//units/%28-%29def%7Dif//anal/get_parameter/%28Observed%29eq/%7B.precip//long_name/%28Precipitacion%20Observada%29def//units/%28mm/mes%29def%7Dif//anal/get_parameter/%28Percentage%29eq/%7B.precip_percentage//long_name/%28Porcentaje%20del%20Promedio%29def//units/%28%25%29def%7Dif%5BESTID%5DREORDER/mark/exch/T//full_name/%28Tiempo%29def/exch%5BESTID%5Dtable:/mark/:table/" shape="rect"></a> 
            
            
            
            
            <link rel="iridl:hasFigure" href="http://ons.snirh.gob.pe/expert/%28Percentage%29//anal/parameter/%28Percentage%29/eq/%7B/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/.precip_percentage/SOURCES/.Peru/.SENAMHI/.PISCO/.Peru_mask/.SPI_mask/mul//long_name/%28Precipitacion-Porcentaje%20del%20Promedio%29/def/pdsi_colorbars//symmetric/false/def/DATA/0/100/RANGE/X/Y/fig-/colors/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/%28Observed%29/eq/%7B/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/.Precipitation/SOURCES/.Peru/.SENAMHI/.PISCO/.Peru_mask/.SPI_mask/mul//long_name/%28Precipitaci%C3%B3n%29/def/DATA/0/100/RANGE/X/Y/fig-/colors/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange/%7Dif//anal/get_parameter/%28Anomaly%29/eq/%7B/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/.precip_st_anomaly/SOURCES/.Peru/.SENAMHI/.PISCO/.Peru_mask/.SPI_mask/mul//long_name/%28Anomalia%20de%20la%20%20Precipitacion%20Estadarizada%29/def//name/%28st_precip%29/def/X/Y/fig-/colors/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//st_precip/-2.5/2.5/plotrange//T/last/plotvalue//Y/-19/0/plotrange%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef/" />
            <img class="dlimg" src="http://ons.snirh.gob.pe/expert/%28Percentage%29//anal/parameter/%28Percentage%29/eq/%7B/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/.precip_percentage/SOURCES/.Peru/.SENAMHI/.PISCO/.Peru_mask/.SPI_mask/mul//long_name/%28Precipitacion-Porcentaje%20del%20Promedio%29/def/pdsi_colorbars//symmetric/false/def/DATA/0/100/RANGE/X/Y/fig-/colors/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/%28Observed%29/eq/%7B/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/.Precipitation/SOURCES/.Peru/.SENAMHI/.PISCO/.Peru_mask/.SPI_mask/mul//long_name/%28Precipitaci%C3%B3n%29/def/DATA/0/100/RANGE/X/Y/fig-/colors/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange/%7Dif//anal/get_parameter/%28Anomaly%29/eq/%7B/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/.precip_st_anomaly/SOURCES/.Peru/.SENAMHI/.PISCO/.Peru_mask/.SPI_mask/mul//long_name/%28Anomalia%20de%20la%20%20Precipitacion%20Estadarizada%29/def//name/%28st_precip%29/def/X/Y/fig-/colors/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//st_precip/-2.5/2.5/plotrange//T/last/plotvalue//Y/-19/0/plotrange%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef/+.gif" border="0" alt="image" /><br clear="none" />
            <img class="dlauximg" src="http://ons.snirh.gob.pe/expert/%28Percentage%29//anal/parameter/%28Percentage%29/eq/%7B/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/.precip_percentage/SOURCES/.Peru/.SENAMHI/.PISCO/.Peru_mask/.SPI_mask/mul//long_name/%28Precipitaci%C3%B3n-Porcentaje%20del%20Promedio%29/def//units/%28%25%29/def/pdsi_colorbars//symmetric/false/def/DATA/0/100/RANGE/X/Y/fig-/colors/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//anal/get_parameter/%28Observed%29/eq/%7B/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/.Precipitation/SOURCES/.Peru/.SENAMHI/.PISCO/.Peru_mask/.SPI_mask/mul//long_name/%28Precipitaci%C3%B3n%29/def//units/%28mm/mes%29/def/DATA/0/100/RANGE/X/Y/fig-/colors/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange/%7Dif//anal/get_parameter/%28Anomaly%29/eq/%7B/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/.precip_st_anomaly/SOURCES/.Peru/.SENAMHI/.PISCO/.Peru_mask/.SPI_mask/mul//long_name/%28Anomal%C3%ADa%20de%20la%20%20Precipitaci%C3%B3n%20Estadarizada%29/def//name/%28st_precip%29/def/pdsi_colorbars/X/Y/fig-/colors/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//st_precip/-2.5/2.5/plotrange//T/last/plotvalue//Y/-19/0/plotrange%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef/+.auxfig/+.gif" />
           
            
            
            
         </fieldset>
         
         
         <div id="tabs-1" class="ui-tabs-panel" about="">
            
            <h3 align="center" property="term:title">Precipitación Observada - SENAMHI</h3>
            
            <p align="left" property="term:description">Este mapa muestra la precipitación observada en las estaciones meteorológicas del
               Servicio Nacional de Meterología e Hidrología del Perú  (SENAMHI).
            </p>
            
            <p align="left">Seleccione porcentaje, mediciones o anomalía en el menú &gt; <b>análisis</b> para visualizar la variable
               requerida. En menú &gt; <b>región</b> se puede seleccionar la región
               de interés. Utiliza el navegador de tiempo para consultar el mes de interés.
            </p>
            
            <p align="left"><b>Mediciones:</b>
               Este mapa muestra la precipitación observada por parte del producto PISCO
               del SENAMHI. Las observaciones tienen la unidad [mm/mes].
            </p>
            
            <p align="left"><b>Porcentaje:</b>
               Este mapa muestra la precipitación normal esperada en cada mes como porcentaje. Este
               valor indica si hay un déficit o superávit comparada con una situación normal.
            </p>
            
            <p align="left"><b>Anomalía estandarizada:</b>
               El mapa muestra la precipitación observada como anomalía estandarizada. La anomalía
               estandarizada es la diferencia entre la precipitación observada en un mes específico
               y la precipitación esperada normalmente en el mismo mes, y permite identificar condiciones
               de déficit y de superávit con respecto a lo normal (Tabla 1).
            </p>
            
            <p align="left">
               <h6> Tabla 1: Interpretación de la Anomalía Estandarizada</h6>
            </p>
            
            <p align="left"><img src="Escala_anomalias_esp.jpg" alt="Legend of the Standardized Anomalies"> </img></p>
            
            <p align="left"><b>Fuente de Datos</b> 
               
            </p>
            
            
            <p align="left">Los datos usados para el cálculo provienen de estaciones meteorológicas del Servicio
               Nacional de Meterología e Hidrología del Perú  (SENAMHI) y la base de datos PISCO
               ('Peruvian Interpolation of the SENAMHIs Climatological and Hydrological Stations').
            <p align="left"><b>NOTA:</b> La base de datos de PISCO se encuentra validada hasta diciembre 2013, mientras que los valores posteriores a esa fecha son valores preliminares que ser&#225;n validados posteriormente. </p>
               
            </p>
          
            <p align="left"><b>Referencias</b> 
            </p>
            
            <p align="left">Waldo Lavado Casimiro, Carlos Fernandez, Fiorella Vega, Tania Caycho, Sofia Endara, Adrian Huerta y Oscar Felipe Obando, 2015. <a href="PISCO_reporte.pdf">
 			PISCO: Peruvian Interpolated data of the SENAMHIs Climatological and hydrological Observations. Precipitación v1.0. </a><i>Servicio Nacional de Meteorolog&#237;a e Hidrolog&#237;a</i>.               
            </p>
            <p align="left">Waldo Lavado Casimiro, Carlos Fernandez, Cesar Aybar, Tania Caycho, Sofia Endara, Fiorella Vega, 
               Adrian Huerta, Julia Acu&#241;a y Oscar Felipe Obando, 2016. <a href="reportePISCO2016.pdf" shape="rect">
                  			PISCO: Peruvian Interpolated data of the SENAMHIs Climatological and hydrological Observations. Precipitation v1.1
             </a><i>Servicio Nacional de Meteorolog&#237;a e Hidrolog&#237;a</i>.               
               
               
            </p>           

<p align="left"><a href="http://www.peruclima.pe/?p=Indicadores-de-sequias" shape="rect"><img src="../../icons/Logo_Senamhi.jpg" alt="Logo SENAMHI" /></a></p>
                        
         </div>
         
         
         <div id="tabs-2" class="ui-tabs-panel">
            
            <h3 align="center">¿Cómo se calcula el porcentaje?</h3>
            
            <p>El porcentaje se calcula dividiendo la precipitación observada en un mes con la precipitación
               normalmente esperada en este mes (promedio) multiplicado con cien. 
            </p>
            
            <h3 align="center">¿Cómo se calcula la anomalía?</h3>
            
            <p>La anomalía es la precipitación mensual observada en una estación específica menos
               el promedio de la precipitación mensual (calculado usando datos históricos de la estación
               específica) dividido por la desviación estándar.
               
            </p>
            
         </div>
         
         
         <div id="tabs-3" class="ui-tabs-panel">
            
            <h3 align="center">Fuente de los Datos</h3>
            
            <p align="left"><a href="http://ons.snirh.gob.pe/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/" shape="rect">Precipitación observada en estaciones</a>, entregado por SENAMHI <a href="http://www.senamhi.gob.pe/" shape="rect">(SENAMHI)</a> </p>
	     <p align="left">Los datos mensuales de las variables de precipitaci&#243;n del periodo 1981 al 2013 con un n&#250;mero de estaciones variables desde 180 hasta 448, despu&#233;s de este a&#241;o hasta la actualidad son utilizados para la elaboraci&#243;n del producto PISCO, datos de informaci&#243;n pasando esa fecha no tiene control de calidad y el uso de ellos vienen con la incertidumbre si van a ser modificados al hacer dicho control.</p>
	     <p align="left">Nota: Estos datos pueden variar por indicaci&#243;n de la Direcci&#243;n Regional del SENAMHI o la comparaci&#243;n con la planilla meteorol&#243;gica.</p>
	     <p align="left">En la Figura se muestra el n&#250;mero de estaciones utilizado cada a&#241;o. </p>
	     <p align="left"><img src="PISCO_estaciones.png" alt="numero de estaciones" /></p>              
           
            <p align="left"><b>Referencias</b> </p>
            <p align="left">Waldo Lavado Casimiro, Carlos Fernandez, Fiorella Vega, Tania Caycho, Sofia Endara, Adrian Huerta y Oscar Felipe Obando, 2015. <a href="PISCO_reporte.pdf">
 			PISCO: Peruvian Interpolated data of the SENAMHIs Climatological and hydrological Observations. Precipitación v1.0. </a><i>Servicio Nacional de Meteorolog&#237;a e Hidrolog&#237;a</i>. </p>
            <p align="left">Waldo Lavado Casimiro, Carlos Fernandez, Cesar Aybar, Tania Caycho, Sofia Endara, Fiorella Vega, 
               Adrian Huerta, Julia Acu&#241;a y Oscar Felipe Obando, 2016. <a href="reportePISCO2016.pdf" shape="rect">
                  			PISCO: Peruvian Interpolated data of the SENAMHIs Climatological and hydrological Observations. Precipitation v1.1
             </a><i>Servicio Nacional de Meteorolog&#237;a e Hidrolog&#237;a</i>.               
               
               
            </p>                   
<p align="left"><a href="http://www.peruclima.pe/?p=Indicadores-de-sequias" shape="rect"><img src="../../icons/Logo_Senamhi.jpg" alt="Logo SENAMHI" /></a></p>
         </div>
         
         
         <div id="tabs-4" class="ui-tabs-panel">
            
            <h3 align="center">Soporte</h3>
            
            <p>
               Sus consultas a: <a href="mailto:snirh@ana.gob.pe?subject=Precipitacion Observada Peru" shape="rect">snirh@ana.gob.pe</a>
               
            </p>
            
         </div>
         
         <div id="tabs-5" class="ui-tabs-panel">
            
            <h3 align="center">Instrucciones</h3>
            
            <div class="buttonInstructions"></div>
            
         </div>
         
      </div>
      
      <div class="optionsBar">
         
         <fieldset class="navitem" id="share">
            <legend>Share</legend>
         </fieldset>
         
      </div>
      
   </body>
</html>