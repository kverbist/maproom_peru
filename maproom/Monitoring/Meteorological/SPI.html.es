<?xml version="1.0" encoding="utf-8"?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:wms="http://www.opengis.net/wms#" xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#" xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#" xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#" xmlns:xs="http://www.w3.org/2001/XMLSchema#" xmlns:og="http://ogp.me/ns#" version="XHTML+RDFa 1.0">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="description" xml:lang="es" content="Este mapa muestra el índice de Precipitación Estandarizado (SPI por sus siglas en inglés) para múltiples periodos de acumulación." />
      <meta name="twitter:card" content="summary_large_image" />
      <meta property="og:title" content="Índice de Precipitación Estandarizado" />
      <meta property="og:description" content="Este mapa muestra el índice de Precipitación Estandarizado (SPI por sus siglas en inglés) para múltiples periodos de acumulación." />
      <meta property="og:image" content="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.SENAMHI/.PISCO/.SPI/.SPI1/SOURCES/.Peru/.SENAMHI/.PISCO/.Peru_mask/.SPI_mask/mul//plotlast/3/def//plotfirst/-3/def//name//SPI/def//long_name/%28Indice%20de%20Precipitacion%20Estandarizado%29/def/T/last/VALUE/%5BT%5Daverage/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//Y/-18.5/0/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef+//plotborder+0+psdef//plotaxislength+432+psdef//antialias+true+psdef//color_smoothing+1+psdef+.gif" />
      <meta name="viewport" content="width=device-width; initial-scale=1.0;" />
      <meta xml:lang="" property="maproom:Entry_Id" content="Monitoring" />
      <title>Índice de Precipitación Estandarizado</title>
      <link rel="stylesheet" type="text/css" href="../../unesco.css" />
      <link class="altLanguage" rel="alternate" hreflang="en" href="SPI.html?Set-Language=en" />
      <link class="share" rel="canonical" href="SPI.html" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Meteorological_term" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_term" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Example" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_Precipitation_term" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#monthly" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded" />
      <link rel="term:icon" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.SENAMHI/.PISCO/.SPI/.SPI1/SOURCES/.Peru/.SENAMHI/.PISCO/.Peru_mask/.SPI_mask/mul//plotlast/3/def//plotfirst/-3/def//name//SPI/def//long_name/%28Indice%20de%20Precipitacion%20Estandarizado%29/def/T/last/VALUE/%5BT%5Daverage/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//Y/-18.5/0/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef+//plotborder+0+psdef//plotaxislength+432+psdef//antialias+true+psdef//color_smoothing+1+psdef+.gif" /><script type="text/javascript" src="../../../uicore/uicore.js" xml:space="preserve"></script><script type="text/javascript" src="../../../maproom/unesco.js" xml:space="preserve"></script></head>
   <body xml:lang="es">
      
      <form name="pageform" id="pageform" class="carryup carry dlimg dlauximg share" method="get" enctype="application/x-www-form-urlencoded">
         <input class="carryLanguage carryup carry " name="Set-Language" type="hidden" />
         <input class="dlimg admin share" name="bbox" type="hidden" />
         <input class="dlimg dlauximg onlyvar dlimgts share" name="var" type="hidden" />
         <input class="dlimg share" name="T" type="hidden" />
         <input class="dlimg" name="plotaxislength" type="hidden" />
         <input class="share dlimgloc dlimgts" name="region" type="hidden" />
         <input class="share dlimgloc dlimgts admin" name="resolution" type="hidden" data-default="irids:SOURCES:Features:Hydrological:Cuencas:Mayores:ds" />
         
      </form>
      
      <div class="controlBar">
         
         <fieldset class="navitem" id="toSectionList"> 
            
            <legend>Maproom</legend> 
            		<a rev="section" class="navlink carryup" href="/maproom/Monitoring/" shape="rect">Monitoreo</a>
            
         </fieldset> 
         
         <fieldset class="navitem" id="chooseSection"> 
            	
            <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_Meteorological_term"><span property="term:label">Sequía Meteorológica</span></legend>
            
         </fieldset> 
         
         <fieldset class="navitem">
            
            <legend>Región</legend>
            <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsPeru.json" shape="rect"></a>
            <select class="RegionMenu" name="bbox">
               <option value="">Peru</option>
               <optgroup class="template" label="Región">
                  <option class="irigaz:hasPart irigaz:id@value term:label"></option>
               </optgroup></select>
            
         </fieldset>
         		 
         
         <fieldset class="navitem">
            
            	       
            <legend>Análisis</legend><span class="selectvalue"></span><select class="pageformcopy" name="var">
               <option value="">SPI 1 Mes</option>
               <option value="SPI3">SPI 3 Meses</option>
               <option value="SPI6">SPI 6 Meses</option>
               <option value="SPI9">SPI 9 Meses</option>
               <option value="SPI12">SPI 12 Meses</option></select>
            		   
            
         </fieldset>	
         		 
         		 
         
         <fieldset class="navitem">
            
            
            <legend>Promedio Espacial Sobre</legend><span class="selectvalue"></span><select class="pageformcopy" name="resolution">
               <option value="0.5">Ubicación puntual</option>
               <option value="irids:SOURCES:Features:Political:Peru:Departamento:ds">Departamento</option>
               <option value="irids:SOURCES:Features:Political:Peru:Provincia:ds">Provincia</option>
               <option value="irids:SOURCES:Features:ANA:AmbAdm:AAA:ds">Autoridad Administrativa del Agua  (AAA)</option>
               <option value="irids:SOURCES:Features:ANA:AmbAdm:ALA:ds">Administración Local de Agua (ALA)</option>
               <option value="irids:SOURCES:Features:Hydrological:Cuencas:Mayores:ds">Unidad Hidrográfica</option></select>       	    
            
            
            
            <link class="admin" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/%28irids:SOURCES:Features:Hydrological:Cuencas:Mayores:ds%29//resolution/parameter/geoobject/%28bb:-85:-19:-65:0:bb%29//bbox/parameter/geoobject/labelgeoIdintersects/info.json" />
            <select class="pageformcopy" name="region">
               <optgroup class="template" label="Label">
                  <option class="iridl:values region@value label"></option>
               </optgroup></select>
            
            
         </fieldset>	
         
      </div>
      
      <div class="ui-tabs">
         
         <ul class="ui-tabs-nav">
            <li><a href="#tabs-1" shape="rect">Descripción</a></li>
            <li><a href="#tabs-2" shape="rect">Más información</a></li>
            <li><a href="#tabs-3" shape="rect">Fuente</a></li>
            <li><a href="#tabs-4" shape="rect">Soporte</a></li>
            <li><a href="#tabs-5" shape="rect">Instrucciones</a></li>
         </ul>
         
         
         <fieldset class="dlimage regionwithinbbox">
            <a class="dlimgts" rel="iridl:hasTable" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.SENAMHI/.PISCO/.SPI%5B%28.%29%28SPI1%29//var/parameter%5Dconcat/interp/-5/maskle/%28bb:-70%2C-5.0%2C-69%2C-4.5%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//long_name/%28SPI%29def//units/%28-%29def/T/exch/table-/text/text/skipanyNaN/-table/.html/" shape="rect"></a>
            
            
            
            
            <div style="float: left;">
               <img class="dlimgloc" src="http://ons.snirh.gob.pe/SOURCES/.Peru/.CAZALAC/.MinExpectedPrecip/.5YR/X/Y/%28bb:-82%2C-17%2C-68%2C0%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-76%2C-8%2C-76.2%2C-8.2%29/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborder/0/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotaxislength/120/psdef/+.gif" />
               
               
               
               
            </div>	
            
            
            
            
            
            <div style="float: left;">
               	
               
               
               
               
               <div class="valid" style="display: inline-block; text-align: top;">
                  <a class="dlimgts" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json" shape="rect"></a>
                  
                  
                  
                  
                  <div class="template"> Observacioness para <span class="bold iridl:long_name"></span>
                     
                     
                     
                     
                  </div>
                  
                  
                  
                  
               </div>
               
               
               
               
               
               
               <div class="valid" style="text-align: top;">
                  
                  
                  
                  
                  <div>Valores del SPI para el mes actual: </div> 
                  <a class="dlimgloc" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.SENAMHI/.PISCO/.SPI/SPI1/-5/maskle/T/last/VALUE/%28bb%3A-71.5%2C-5.0%2C-71.0%2C-4.5%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average//long_name/%28SPI1%3A%20%29/def/SPI3/-5/maskle/T/last/VALUE/%28bb%3A-71.5%2C-5.0%2C-71.0%2C-4.5%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average//long_name/%28SPI3%3A%20%29/def/SPI6/-5/maskle/T/last/VALUE/%28bb%3A-71.5%2C-5.0%2C-71.0%2C-4.5%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average//long_name/%28SPI6%3A%20%29/def/SPI9/-5/maskle/T/last/VALUE/%28bb%3A-71.5%2C-5.0%2C-71.0%2C-4.5%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average//long_name/%28SPI9%3A%20%29/def/SPI12/-5/maskle/T/last/VALUE/%28bb%3A-71.5%2C-5.0%2C-71.0%2C-4.5%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average//long_name/%28SPI12%3A%20%29/def/5/ds/info.json" shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "+td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"

}
}
}
</script>
                  <div>
                     
                     
                     
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="name " rowspan="1" colspan="1"></td>
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                        </tr>
                     </table>
                     
                     
                     
                     
                  </div>
                  
                  
                  
                  
                  
               </div>
               
               
               
               
            </div>
            
            <br clear="none" />
            <img class="dlimgts regionwithinbbox" src="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.SENAMHI/.PISCO/.SPI/%5B/%28.%29/%28SPI1%29//var/parameter/%5Dconcat/interp/-5/maskle/%28bb:-71.5%2C-5.0%2C-71.0%2C-4.5%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average/SPI_senamhi_colors/DATA/-2.5/2.5/RANGE//long_name/%28Indice%20de%20Precipitacion%20Estandarizado%29/def/dup/T/fig-/colorbars2/-fig/+.gif" />  
            
            
            
            
         </fieldset> 
         
         
         
         
         
         <fieldset class="dlimage" id="content" about="">
            
            
            
            
            <link rel="iridl:hasFigure" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.SENAMHI/.PISCO/.SPI/%5B/%28.%29/%28SPI1%29//var/parameter/%5Dconcat/interp/-5/maskle/SOURCES/.Peru/.SENAMHI/.PISCO/.Peru_mask/.SPI_mask/mul//plotlast/2.5/def//plotfirst/-2.5/def//name//SPI/def//long_name/%28Indice%20de%20Precipitacion%20Estandarizado%29/def/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//T/last/plotvalue//Y/-18.5/0/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef//layers%5B//SPI//lakes//coasts_gaz//countries_gaz//states_gaz%5Dpsdef/" />
            <img class="dlimg" src="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.SENAMHI/.PISCO/.SPI/%5B/%28.%29/%28SPI1%29//var/parameter/%5Dconcat/interp/-5/maskle/SOURCES/.Peru/.SENAMHI/.PISCO/.Peru_mask/.SPI_mask/mul//plotlast/2.5/def//plotfirst/-2.5/def//name//SPI/def//long_name/%28Indice%20de%20Precipitacion%20Estandarizado%29/def/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//T/last/plotvalue//Y/-18.5/0/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef//layers%5B//SPI//lakes//coasts_gaz//countries_gaz//states_gaz%5Dpsdef/+.gif" border="0" alt="image" /><br clear="none" />
            <img class="dlauximg" src="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.SENAMHI/.PISCO/.SPI/%5B/%28.%29/%28SPI1%29//var/parameter/%5Dconcat/interp/-5/maskle/SOURCES/.Peru/.SENAMHI/.PISCO/.Peru_mask/.SPI_mask/mul//plotlast/2.5/def//plotfirst/-2.5/def//name//SPI/def//long_name/%28Indice%20de%20Precipitacion%20Estandarizado%29/def/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//T/last/plotvalue//Y/-18.5/0/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef//layers%5B//SPI//lakes//coasts_gaz//countries_gaz//states_gaz%5Dpsdef/+.auxfig/+.gif" />
            
            
            
            
         </fieldset>
         
         
         
         <div id="tabs-1" class="ui-tabs-panel" about="">
            
            
            <h3 align="center" property="term:title">Índice de Precipitación Estandarizado</h3>
            
            
            <p align="left" property="term:description">Este mapa muestra el índice de Precipitación Estandarizado (SPI por sus siglas en
               inglés) para múltiples periodos
               de acumulación.
               
            </p>
            
            
            <p align="left">El Índice de Precipitación Estandarizado (SPI; McKee 1993) es el número de desviaciones
               estándar que la precipitación acumulada se desvía del promedio climatológico. Esto
               indica que valores por debajo de un valor -1 indican condiciones de déficit significativos,
               mientras que valores mayores que +1 indican condiciones más húmedos que lo normal.
               
            </p>
            
            
            <p align="left">
               
               <h6> Tabla: Valores de &#237;ndices Normalizados de Precipitaci&#243;n</h6>
               
            </p>
            
            
            <p align="left"><img src="Escala_SPI_esp.JPG" alt="SPI"> </img></p>
            
            
            <p align="left">El SPI está disponible para diferentes periodos de acumulación: 1, 3, 6, 9 o 12 meses,
               lo cual permite evaluar la duración de las condiciones de sequía y superávit para
               diferentes escalas de tiempo. Selecciona la escala de tiempo de interés en el menú&gt;análisis.
               En el menú&gt;Región puedes seleccionar la región de interés. 
               
            </p>
            
            
            <p align="left"><b>Fuente de Datos</b> 
               
               
            </p>
            
            
            
            <p align="left">Los datos usados para el cálculo provienen de estaciones meteorológicas del Servicio
               Nacional de Meterología e Hidrología del Perú  (SENAMHI) y la base de datos PISCO
               ('Peruvian Interpolation of the SENAMHIs Climatological and Hydrological Stations').
               
               <p align="left"><b>NOTA:</b> La base de datos de PISCO se encuentra validada hasta diciembre 2013, mientras que
                  los valores posteriores a esa fecha son valores preliminares que serán validados posteriormente.
                  
               </p>
               
               
            </p>
            
            
            <p align="left"><b>Referencia</b> 
               
            </p>
            
            
            <p align="left">Aybar, C.; Lavado-Casimiro, W.; Huerta, A.; Fernández, C.; Vega, F.; Sabino, E. Felipe-Obando,
               O. (2017). <a href="PISCO-Prec-v20.pdf" shape="rect">
                  			Uso del Producto Grillado "PISCO" de precipitación en Estudios, Investigaciones
                  y Sistemas Operacionales de Monitoreo y Pronóstico Hidrometeorológico.</a><i>Nota Técnica 001 SENAMHI-DHI-2017, Lima-Perú</i>. 
            </p>
            
            <p align="left"><a href="http://www.peruclima.pe/?p=Indicadores-de-sequias" shape="rect"><img src="../../icons/Logo_Senamhi.jpg" alt="Logo SENAMHI" /></a></p>
            
            
         </div>
         
         
         
         <div id="tabs-2" class="ui-tabs-panel">
            
            
            <h3 align="center">¿Cómo se calcula el SPI?</h3>
            
            
            <p align="left">El Índice de Precipitación Estandarizada (SPI; McKee 1993) es el número de desviaciones
               estándar que la precipitación cumulativa se desvía del promedio climatológico. Puede
               ser calculado por cualquier escala de tiempo. Aquí el SPI es calculado por 1, 3, 6,
               9 y 12 meses.
               
            </p>
            
            
            <p align="left">Se requiere una larga serie de tiempo de precipitación acumulada para el cálculo de
               este índice para cada escala de tiempo; con ello se puede estimar la función de densidad
               de probabilidad más apropiada, que para este caso es la distribución de Pearson-III
               (i.e., gamma 3-parámetros), según lo sugerido por Guttman (1999). Esta distribución
               es utilizada para ajustar la serie de tiempo de la precipitación. La distribución
               es posteriormente transformada a una distribución normal acumulada, el resultado es
               el SPI.
               
            </p>
            
            
            <p align="left">En la siguiente figura se presenta el procedimiento de transformación de las observaciones
               de precipitación a través de la función acumulada de probabilidad, distribución Gamma,
               a una variable normal estandarizado, el SPI.
               
            </p>
            
            
            <p align="left"><img src="EPI_calculation.png" alt="EPI"> </img></p>
            
            
            <p align="left">
               
               <h6>Figura 1: Transformación de la precipitación observada a través de la CDF de la distribución
                  Gamma, al Índice de Precipitación Estandarizada (SPI)
                  
               </h6>
               
            </p>
            
            
            <p align="left">El SPI puede ser interpretado como una probabilidad usando una distribución normal
               estandarizada. Por ejemplo, la probabilidad de que el SPI tenga un valor entre -1
               y +1 es del 68%.
               
            </p>
            
            
            
            <p align="left">Los datos usados para el cálculo provienen de estaciones meteorológicas del Servicio
               Nacional de Meterología e Hidrología del Perú  (SENAMHI) y la base de datos PISCO
               ('Peruvian Interpolation of the SENAMHIs Climatological and Hydrological Stations').
               
            </p>
            
            
            <p align="left"><a href="http://www.peruclima.pe/?p=Indicadores-de-sequias" shape="rect"><img src="../../icons/SENAMHI" alt="Logo SENAMHI" /></a></p>
            
            
         </div>
         
         
         
         <div id="tabs-3" class="ui-tabs-panel">
            
            
            <h3 align="center">Fuente de los Datos</h3>
            
            
            <p align="left"><a href="http://ons.snirh.gob.pe/SOURCES/.Peru/.SENAMHI/.PISCO/.SPI/" shape="rect">Índice de Precipitación Estandarizada (SPI)</a>, entregado por SENAMHI <a href="http://www.senamhi.gob.pe/" shape="rect">(SENAMHI)</a></p>
            	     
            <p align="left">Los datos mensuales de las variables de precipitación del periodo 1981 al 2013 con
               un número de estaciones variables desde 180 hasta 448, después de este año hasta la
               actualidad son utilizados para la elaboración del producto PISCO, datos de información
               pasando esa fecha no tiene control de calidad y el uso de ellos vienen con la incertidumbre
               si van a ser modificados al hacer dicho control.
            </p>
            	     
            <p align="left">Nota: Estos datos pueden variar por indicación de la Dirección Regional del SENAMHI
               o la comparación con la planilla meteorológica.
            </p>
            	     
            <p align="left">En la Figura se muestra el número de estaciones utilizado cada año. </p>
            	     
            <p align="left"><img src="PISCO_estaciones.png" alt="numero de estaciones" /></p>              
            
            
            <p align="left"><b>Referencia</b> 
            </p>
            
            <p align="left">Aybar, C.; Lavado-Casimiro, W.; Huerta, A.; Fernández, C.; Vega, F.; Sabino, E.  Felipe-Obando,
               O. (2017). <a href="PISCO-Prec-v20.pdf" shape="rect">
                  			Uso del Producto Grillado -PISCO- de precipitación en Estudios, Investigaciones
                  y Sistemas Operacionales de Monitoreo y Pronóstico Hidrometeorológico.</a><i>Nota Técnica 001 SENAMHI-DHI-2017, Lima-Perú</i>. 
            </p>
            
            <p align="left"><a href="http://www.peruclima.pe/?p=Indicadores-de-sequias" shape="rect"><img src="../../icons/SENAMHI" alt="Logo SENAMHI" /></a></p>
            
         </div>
         
         
         
         <div id="tabs-4" class="ui-tabs-panel">
            
            
            <h3 align="center">Soporte</h3>
            
            
            <p>
               Sus consultas a: <a href="mailto:snirh@ana.gob.pe?subject=SPI Per%C3%BA" shape="rect">snirh@ana.gob.pe</a>
               
               
            </p>
            
            
         </div>
         
         
         <div id="tabs-5" class="ui-tabs-panel">
            
            
            <h3 align="center">Instrucciones</h3>
            
            
            <div class="buttonInstructions"></div>
            
            
         </div>
         
         
         
      </div>
      
      
      <div class="optionsBar">
         
         
         <fieldset class="navitem" id="share">
            
            <legend>Compartit</legend>
            
         </fieldset>
         
         
      </div>
      
      
   </body>
</html>