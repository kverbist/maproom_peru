<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      xmlns:og="http://ogp.me/ns#" 
      version="XHTML+RDFa 1.0"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<meta xml:lang="" property="maproom:Entry_Id" content="Monitoring" />
<title>Agricultural Stress Index  (ASI)</title>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="es" href="ASI.html?Set-Language=es" />
<link class="share" rel="canonical" href="ASI.html" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_NDVI_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Example"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_Precipitation_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#monthly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded" />
<link rel="term:icon" href="http://ons.snirh.gob.pe/SOURCES/.Peru/.CAZALAC/.ASIS/.ASI/.ASI/long_name/%28ASI%29def/X/Y/fig-/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//aprod/0/10/plotrange/X/-71.6/-68.4/plotrange/Y/-12.4/-17.5/plotrange//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef//T/last/plotvalue/Y/-12.4/-17.5/plotrange//plotborder/72/psdef//plotaxislength/432/psdef//XOVY/null/psdef/+.gif?plotaxislength=520" />
<script type="text/javascript" src="../../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../../maproom/unesco.js"></script>
</head>
<body  xml:lang="en">
<form name="pageform" id="pageform" class="carryup carry dlimg dlauximg share" method="get" enctype="application/x-www-form-urlencoded">
<input class="carryLanguage carryup carry " name="Set-Language" type="hidden" />
<input class="dlimg admin share" name="bbox" type="hidden" />
<input class="dlimg dlauximg onlyvar dlimgts share" name="var" type="hidden" />
<input class="dlimg share" name="T" type="hidden" />
<input class="dlimg" name="plotaxislength" type="hidden" />
<input class="share dlimgloc dlimgts" name="region" type="hidden" />
<input class="share dlimgloc dlimgts admin" name="resolution" type="hidden" data-default="irids:SOURCES:Features:Hydrological:Cuencas:Mayores:ds" />
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>Maproom</legend> 
            		<a rev="section" class="navlink carryup" href="/maproom/Monitoring/" shape="rect">Monitoreo</a>
            </fieldset> 
            <fieldset class="navitem" id="chooseSection"> 
            	<legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_NDVI_term"><span property="term:label">Sequía Agrícola</span></legend>
            </fieldset> 

		 
		 <fieldset class="navitem">
            
            
            <legend>Type of Crop</legend><span class="selectvalue"></span><select class="pageformcopy" name="var">
               <option value="avena">Fodder Oats</option>
               <option value="papa">Potato</option>
			   <option value="quinua">Quinoa</option>
			   <option value="cebada">Fodder Barley</option>
               </select>
            
            
         </fieldset>
		 		 		 	
 </div>
 
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
            <li><a href="#tabs-1" shape="rect">Description</a></li>
            <li><a href="#tabs-2" shape="rect">More information</a></li>
            <li><a href="#tabs-3" shape="rect">Dataset Documentation</a></li>
            <li><a href="#tabs-4" shape="rect">Contact Us</a></li>
            <li><a href="#tabs-5" shape="rect">Instructions</a></li>
    </ul>

        <fieldset class="dlimage regionwithinbbox">
            <a class="dlimgts" rel="iridl:hasTable" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.CAZALAC/.ASIS/.ASI/%5B/(.)/(ASI)//var/parameter/%5Dconcat/interp/-5/maskle/(bb%3A-70%2C-5.0%2C-69%2C-4.5)//region/parameter/geoobject/%5BX/Y%5Dweighted-average//long_name/(ASI)/def//units/(-)/def/T/exch/table-/text/text/skipanyNaN/-table/.html/" shape="rect"></a>
            
        <div style="float: left;">
               <img class="dlimgloc" src="http://ons.snirh.gob.pe/SOURCES/.Peru/.CAZALAC/.MinExpectedPrecip/.5YR/X/Y/%28bb:-82%2C-17%2C-68%2C0%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-76%2C-8%2C-76.2%2C-8.2%29/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborder/0/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotaxislength/120/psdef/+.gif" />
               
               
               
               
            </div>    
            
            
            
            
            
            
            <div style="float: left;">
               	
               
               
               
               <div class="valid" style="display: inline-block; text-align: top;">
                  <a class="dlimgts" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json" shape="rect"></a>
                  
                  
                  
                  <div class="template"> Observations for  <span class="bold iridl:long_name"></span>
                     
                     
                     
                  </div>
                  
                  
                  
               </div>
               
               
               
               
               
               <div class="valid" style="text-align: top;">
                  
                  
                  
                  <div>values for the current month:</div> 
                  <a class="dlimgloc" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.CAZALAC/.ASIS/.ASI/avena/100/maskgt/T/last/VALUE/%28bb:-71.5%2C-5.0%2C-71.0%2C-4.5%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//long_name/%28ASI3:%20%29/def/1/ds/info.json" shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "+td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"

}
}
}
</script>
                  <div>
                     
                     
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="name " rowspan="1" colspan="1"></td>
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                        </tr>
                     </table>
                     
                     
                     
                  </div>
                  
                  
                  
                  
               </div>
               
               
               
            </div>
            
            <br clear="none" />
            <img class="dlimgts regionwithinbbox" src="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.CAZALAC/.ASIS/.ASI/%5B/%28.%29/%28avena%29//var/parameter/%5Dconcat/interp/100/maskgt/%28bb:-71.5%2C-5.0%2C-71.0%2C-4.5%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average/asi_colores2/DATA/0/126.5/RANGE//long_name/%28ASI%29/def/dup/T/fig-/colorbars2/-fig/+.gif" />  
            
            
            
         </fieldset> 
         
         
         
         
         <fieldset class="dlimage" id="content" about="">
            
           
            
            <link rel="iridl:hasFigure" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.CAZALAC/.ASIS/.ASI/%5B/(.)/(avena)//var/parameter/%5Dconcat/interp/SOURCES/.Peru/.CAZALAC/.ASIS/.Peru_mask/.ASI_mask2/mul//name//ASIs/def/a-/-a/asi_colores2/X/Y/fig%3A/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/%3Afig//T/last/plotvalue//Y/-17/-13.5/plotrange//X/-72/-68/plotrange//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//XOVY/null/psdef//layers%5B//ASIs//coasts_gaz//lakes//countries_gaz//states_gaz%5Dpsdef/" />
            <img class="dlimg" src="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.CAZALAC/.ASIS/.ASI/%5B/%28.%29/%28avena%29//var/parameter/%5Dconcat/interp/SOURCES/.Peru/.CAZALAC/.ASIS/.Peru_mask/.ASI_mask2/mul//name//ASIs/def/a-/-a/asi_colores2/X/Y/fig:/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//T/last/plotvalue//Y/-17/-13.5/plotrange//X/-72/-68/plotrange//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//XOVY/null/psdef//layers%5B//ASIs//coasts_gaz//lakes//countries_gaz//states_gaz%5Dpsdef/+.gif" border="0" alt="image" /><br clear="none" />
            <img class="dlauximg" src="barra_asis.gif" />
            
            
         </fieldset>
         
         
         <div id="tabs-1" class="ui-tabs-panel" about="">
            
            <h3 align="center" property="term:title">Agricultural Stress Index (ASI)</h3>
            
            <p align="justify" property="term:description">This map shows the actual situation of the vegetation in the Departament of Puno, as reflected by the the Agricultural Stress Index (ASI).</p>
            
            <p align="justify"></p>
			<p align="center">
               <h6 align="center" > Table 1: Range of percentage of affected area</h6>
            </p>
            <p align="center"><img src="escala_asi.png" alt="ASI"> </img></p>
            <p align="justify">This index is available for different types of crops: Fodder Oats, Potato, Quinoa, Fodder Barley. Select the crop type of your interest in the Menu > Type of Crop.</p>

            
            
         </div>
         
         
         <div id="tabs-2" class="ui-tabs-panel">
            
            <h3 align="center">Agricultural Stress Index </h3>
            
            <p align="justify">The Agricultural Stress Index (ASI) is an index based on the integration of the Vegetation Health Index (VHI) in two dimensions that are critical in the assessment of a drought event in agriculture: temporal and spatial. The first step of the ASI calculation is a temporal averaging of the VHI, assessing the intensity and duration of dry periods occurring during the crop cycle at pixel level. The second step determines the spatial extent of drought events by calculating the percentage of pixels in arable areas with a VHI value below 35 percent (this value was identified as a critical threshold in assessing the extent of drought in previous research by Kogan, 1995). Finally, each administrative area is classified according to its percentage of affected area to facilitate the quick interpretation of results by analysts.</p>
            
            
            
         </div>
         
         
         <div id="tabs-3" class="ui-tabs-panel">
            
            <h3 align="center">Dataset Documentation</h3>
            
			<p align="left"><a href="#" shape="rect">METOP-AVHRR</a></p>	     

		 </div>
         
         
         <div id="tabs-4" class="ui-tabs-panel">
            
            <h3 align="center">Helpdesks</h3>
            
            <p>
               Contact : <a href="mailto:snirh@ana.gob.pe?subject=ASI" shape="rect">snirh@ana.gob.pe</a>
               
            </p>
            
         </div>
         
         <div id="tabs-5" class="ui-tabs-panel">
            
            <h3 align="center">Instructions</h3>
            
            <div class="buttonInstructions"></div>
            
         </div>
         
         
      </div>
      
      <div class="optionsBar">
         
         <fieldset class="navitem" id="share">
            <legend>Share</legend>
         </fieldset>
         
      </div>
      
   </body>
</html>