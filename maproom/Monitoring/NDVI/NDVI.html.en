<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<meta xml:lang="" property="maproom:Entry_Id" content="Monitoring" />
<title>Normalized Difference Vegetation index</title>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="es" href="NDVI.html?Set-Language=es" />
<link class="share" rel="canonical" href="NDVI.html" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_NDVI_term"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Example"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_Precipitation_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#monthly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded" />
<link rel="term:icon" href="http://iridl.ldeo.columbia.edu/expert/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlyavg/.NDVI/X/-81.995/0.01/-67.505/0.8/evengridAverage/Y/-18.995/0.01/0.995/0.8/evengridAverage/2/RECHUNK/T//defaultvalue/%7Blast%7Ddef/pop//long_name/%28Normalized%20Difference%20Vegetation%20Index%29/def/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.Countrymask/.mask_coarse/mul/X/Y/fig:/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef+//plotborder+0+psdef//plotaxislength+432+psdef//color_smoothing+1+psdef+.gif" />
<script type="text/javascript" src="../../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../../unesco.js"></script>
<style>
.dlimgtsbox { 
width: 49%;
display: inline-block
 }
body.varreflectance img.dlauximg{display: none}
body.varreflectance .regionwithinbbox{display: none !important}
</style>
</head>
<body  xml:lang="en">
<form name="pageform" id="pageform" class="info carryup carry share dlimg dlauximg dlimgts dlimgloc">
      	<input class="carryLanguage carryup carry" name="Set-Language" type="hidden" />
         <input class="carry dlimg dlimgloc admin share" name="bbox" type="hidden" />
         <input class="dlimg dlauximg dlimgts onlyvar share" name="ana" type="hidden" />
         <input class="share dlimgts dlimgloc" name="region" type="hidden" />
         <input class="dlimg share" name="T" type="hidden" />
         <input class="unused dlimg" name="plotaxislength" type="hidden" value="432" />
         <input class="share dlimgloc dlimgts admin pickarea" name="resolution" type="hidden"
                data-default="0.1" />
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>Maproom</legend> 
                      <a rev="section" class="navlink carryup" href="/maproom/Monitoring/">Monitoring</a>
            </fieldset> 
            <fieldset class="navitem" id="chooseSection"> 
                <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_NDVI_term"><span property="term:label">Agricultural Drought</span></legend>
            </fieldset> 
 	     <fieldset class="navitem">
            <legend>Region</legend>
            <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsPeru.json" shape="rect"></a>
            <select class="RegionMenu" name="bbox">
               <option value="">Peru</option>
               <optgroup class="template" label="Region">
                  <option class="irigaz:hasPart irigaz:id@value term:label"></option>
               </optgroup></select>
           </fieldset>

            <fieldset class="navitem">
               <legend>Analysis</legend><span class="selectvalue"></span><select class="pageformcopy" name="ana"><option value="">Anomaly</option><option value="Observed">Observed</option></select>
            </fieldset>
 </div>

<div class="ui-tabs">
    <ul class="ui-tabs-nav">
      <li><a href="#tabs-1" >Description</a></li>
      <li><a href="#tabs-2" >More information</a></li>
      <li><a href="#tabs-3" >Dataset Documentation</a></li>
      <li><a href="#tabs-4" >Contact Us</a></li>
      <li><a href="#tabs-5" >Instructions</a></li>
    </ul>
<fieldset class="dlimage regionwithinbbox">
           
            
            
            <div style="float: left;">
               <img class="dlimgloc"
                    src="http://iridl.ldeo.columbia.edu/SOURCES/.WORLDBATH/.bath/X/Y/%28bb:-85%2C-19%2C-65%2C1%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-75%2C-5%2C-74.5%2C-4.5%29/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/grey/mews_prov/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif" />
               
               
               
            </div>	
            
            
            
            
            <div style="float: left;">
               	
               
               
               
               <div class="valid" style="display: inline-block; text-align: top;">
                  <a class="dlimgts" rel="iridl:hasJSON"
                     href="http://www.climatedatalibrary.cl/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json"
                     shape="rect"></a>
                  
                  
                  
                  <div class="template"> Observations for <span class="bold iridl:long_name"></span>
                     
                     
                     
                  </div>
                  
                  
                  
               </div>
               
               
               
               
               
               <div class="valid" style="text-align: top;">
                  
                  
                  
                  <div> NDVI values for the current month:</div> 
                  <a class="dlimgloc" rel="iridl:hasJSON"
                     href="http://iridl.ldeo.columbia.edu/expert/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlyavg/.NDVI//long_name/%28NDVI%29def/T/last/VALUE/%28bb:-75%2C-5%2C-74.5%2C-4.5%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlyavg/.NDVI/X/-81.99895/0.01/-67.49897/0.8/evengridAverage/Y/0.9994724/0.01/-19.00036/0.8/evengridAverage/2/RECHUNK/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlyclimcoarse/.NDVI/X/-81.99895/0.01/-67.49897/0.8/evengridAverage/Y/0.9994724/0.01/-19.00036/0.8/evengridAverage/2/RECHUNK/sub/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlysdcoarse/.NDVI/X/-81.99895/0.01/-67.49897/0.8/evengridAverage/Y/0.9994724/0.01/-19.00036/0.8/evengridAverage/2/RECHUNK/div//long_name/%28NDVI%20anomaly%29def%5BT%5DREORDER/T/last/VALUE/%28bb:-75%2C-5%2C-74.5%2C-4.5%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/2/ds/info.json"
                     shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "+td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"

}
}
}
</script>
                  <div>
                     
                     
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="name " rowspan="1" colspan="1"></td>
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                        </tr>
                     </table>
                     
                     
                     
                  </div>
                  
                  
                  
                  
               </div>
               
               
               
            </div>
            
            <br clear="none" />
            <img class="dlimgts regionwithinbbox"
                 src="http://iridl.ldeo.columbia.edu/expert/%28Anomaly%29//ana/parameter/%28Anomaly%29eq/%7BSOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlyavg/.NDVI/X/-81.99895/0.01/-67.49897/0.8/evengridAverage/Y/0.9994724/0.01/-19.00036/0.8/evengridAverage/2/RECHUNK/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlyclimcoarse/.NDVI/X/-81.99895/0.01/-67.49897/0.8/evengridAverage/Y/0.9994724/0.01/-19.00036/0.8/evengridAverage/2/RECHUNK/sub/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlysdcoarse/.NDVI/X/-81.99895/0.01/-67.49897/0.8/evengridAverage/Y/0.9994724/0.01/-19.00036/0.8/evengridAverage/2/RECHUNK/div/T//defaultvalue/%7Blast%7Ddef/pop//long_name/%28NDVI%20anomaly%29def/T/last/dup/18.0/sub/exch/RANGE%5BT%5DREORDER/%28bb:%5B-75%2C-5%2C-74.0%2C-4.0%5D%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/pdsi_colorbars/DATA/-2.5/2.5/RANGE/dup/T/fig-/colorbars2/-fig%7Dif//ana/get_parameter/%28Observed%29eq/%7BSOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlyavg/.NDVI//long_name/%28NDVI%29def/T/last/dup/18.0/sub/exch/RANGE/%28bb:%5B-75%2C-5%2C-74.0%2C-4.0%5D%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/toNaN%5BT%5DREORDER/CopyStream/pdsi_colorbars/DATA/0.0/1.0/RANGE/dup/T/fig-/colorbars2/-fig%7Dif/+.gif" />  
            
            
            
         </fieldset>  

         
 		<fieldset class="dlimage">
            <a class="justsregion" rel="iridl:hasFigure" href="http://iridl.ldeo.columbia.edu/expert/%28Anomaly%29//ana/parameter/%28Observed%29/eq/%7B/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlyavg/.NDVI/X/-81.995/0.01/-67.505/0.8/evengridAverage/Y/-18.995/0.01/0.995/0.8/evengridAverage/2/RECHUNK/T//defaultvalue/%7Blast%7Ddef/pop//long_name/%28Normalized%20Difference%20Vegetation%20Index%29/def/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.Countrymask/.mask_coarse/mul/X/Y/fig:/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef%7Dif//ana/get_parameter/%28Anomaly%29/eq/%7BSOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlystdanomcoarse/.NDVI/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.Countrymask/.mask_coarse/mul/T//defaultvalue/%7Blast%7Ddef/pop//long_name/%28NDVI%20anomaly%29/def//plotlast/3/def//plotfirst/-3/def/std_wasp_colors/X/Y/fig:/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//NDVI/-3.5/3.5/plotrange//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef%7Dif//T/last/plotvalue/Y/-19/1/plotrange//plotaxislength/432/psdef//XOVY/null/psdef//plotborder/72/psdef/" shape="rect"></a>
            <img class="dlimg" src="http://iridl.ldeo.columbia.edu/expert/%28Anomaly%29//ana/parameter/%28Observed%29/eq/%7B/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlyavg/.NDVI/X/-81.995/0.01/-67.505/0.8/evengridAverage/Y/-18.995/0.01/0.995/0.8/evengridAverage/2/RECHUNK/T//defaultvalue/%7Blast%7Ddef/pop//long_name/%28Normalized%20Difference%20Vegetation%20Index%29/def/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.Countrymask/.mask_coarse/mul/X/Y/fig:/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef%7Dif//ana/get_parameter/%28Anomaly%29/eq/%7BSOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlystdanomcoarse/.NDVI/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.Countrymask/.mask_coarse/mul/T//defaultvalue/%7Blast%7Ddef/pop//long_name/%28NDVI%20anomaly%29/def//plotlast/3/def//plotfirst/-3/def/std_wasp_colors/X/Y/fig:/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//NDVI/-3.5/3.5/plotrange//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef%7Dif//T/last/plotvalue/Y/-19/1/plotrange//plotaxislength/432/psdef//XOVY/null/psdef//plotborder/72/psdef/+.gif" border="0" />
            <img class="dlauximg" src="http://iridl.ldeo.columbia.edu/expert/%28Anomaly%29//ana/parameter/%28Observed%29/eq/%7B/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlyavg/.NDVI/X/-81.995/0.01/-67.505/0.8/evengridAverage/Y/-18.995/0.01/0.995/0.8/evengridAverage/2/RECHUNK/T//defaultvalue/%7Blast%7Ddef/pop//long_name/%28Normalized%20Difference%20Vegetation%20Index%29/def/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.Countrymask/.mask_coarse/mul/X/Y/fig:/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef%7Dif//ana/get_parameter/%28Anomaly%29/eq/%7BSOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlystdanomcoarse/.NDVI/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.Countrymask/.mask_coarse/mul/T//defaultvalue/%7Blast%7Ddef/pop//long_name/%28NDVI%20anomaly%29/def//plotlast/3/def//plotfirst/-3/def/std_wasp_colors/X/Y/fig:/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//NDVI/-3.5/3.5/plotrange//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef%7Dif//T/last/plotvalue/Y/-19/1/plotrange//plotaxislength/432/psdef//XOVY/null/psdef//plotborder/72/psdef/+.auxfig/+.gif" />
            
         </fieldset>
 <div id="tabs-1" class="ui-tabs-panel" about="">
 <h3 align="center" property="term:title">Normalized Difference Vegetation Index</h3>
  <p align="left" property="term:description">This map shows the actual situation of the vegetation as reflected by the Normalized Difference Vegetation Index (NDVI).</p>
<p align="left">The NDVI uses information of reflectance of two spectral regions: visible light region and near infrared region. The condition of the vegetation influences the interaction between these two spectral regions and the vegetation. This way the NDVI can be related to the condition of the vegetation. The NDVI has a value between -1 and +1.</p>
<p align="left">It is possible to visualize the NDVI as observed or as its anomaly. Select a variable in menu>variable.</p>
<p align="left"><b>Observed:</b>
The observed NDVI has a value between -1 and +1 and is available on a monthly basis. The closer the NDVI to +1, the better the conditions of the vegetation. For example, a forest will have a NDVI closer to +1 opposed to a dessert area that will have a NDVI value closer to 0.</p>
<p align="left"><b>Anomaly:</b>
The NDVI expressed as anomaly indicates the deviation from the NDVI compared to the average. Positive values indicate that the NDVI is greater than the NDVI expected normally. Negative values indicate that the observed NDVI is smaller than the NDVI expected normally (Table 1).</p>
<p align="left"><h6> Tabel 1: Interpretation of the Standardized Anomalies</h6></p>
<p align="left"><img src="Escala_anomalias_vegetacion_eng.jpg" alt="Legend of the Standardized Anomalies"> </img></p>
<p align="left">The data used to calculate the NDVI are measured by the satellite sensor MODIS &#34;Moderate Resolution Imaging Spectroradiometer&#34;.</p>
<p>It is possible to create local graphs of the NDVI, select the tab &#34;Instructions&#34; for more information.</p>
</div>

<div id="tabs-2"  class="ui-tabs-panel">
<h3  align="center">How is the NDVI calculated?</h3>
<p>The NDVI is calculated as:</p>
<p align="left"><img src="NDVI_formula.png" alt="NDVI"> </img></p>
<p>Where VIS is the spectral reflectance in the visible region (red) expressed as ratio (reflectance/incoming) and NIR is the reflectance in the near infrared region (reflectance/incoming).</p>
<p>The figure below illustrates the calculation of the NDVI:</p>
<p align="left"><img src="NDVI_figura.png" alt="NDVI"> </img></p>
<p align="left"><h6>Figure 1: Example of NDVI calculation</h6></p>
<h3  align="center">What is the interaction between the vegetation and the spectral regions NIR and VIS?</h3>
<p>Plants absorb and reflect particular regions of the light spectrum. Large part of the VIS is absorbed by plants and used in the photosynthesis process. Large part of the NIR is reflected by plants because it cannot be used in the photosynthetic process. When plants are in bad condition there is a change in the absorbance and reflectance of VIS and NIR.</p>
</div>

<div id="tabs-3" class="ui-tabs-panel">
<h3  align="center">Dataset Documentation</h3>

<dl class="datasetdocumentation">
<dt>Data</dt>
<dd>1 month estimates on a 1km lat/lon grid <a href="http://climatedatalibrary.cl/SOURCES/.Chile/.Analysis/.MODIS/.Jan/.NDVI/">dataset</a></dd>
<dt>Data Source</dt><dd> United States Geological Survey, Land Processes Distributed Active Archive Center, 
Moderate Resolution Imaging Spectroradiometer 
<a href="http://iridl.ldeo.columbia.edu/SOURCES/.USGS/.LandDAAC/.MODIS/.version_005/.dataset_documentation.html">USGS LandDAAC MODIS</a></dd>
<b>Note</b>:There is typically a 12- to 16-day delay between the end of the observation period for the latest data and the date when those data are received and displayed on this page.
</dl>
</div>

<div id="tabs-4"  class="ui-tabs-panel">
<h3  align="center">Helpdesks</h3>
<p>
Contact <a href="mailto:mwar_lac@unesco.org?subject=NDVI Chile">mwar_lac@unesco.org</a> with any technical questions or problems with this Map Room, for example, the forecasts not displaying or updating properly.
 </p>
 </div>
<div id="tabs-5"  class="ui-tabs-panel">
<h3  align="center">Instructions</h3>
<p>
The MODIS interface provides users with the ability to make graphs. The interface provides a contextual perspective of recent MODIS images by displaying an interactive map of the Normalized Difference Vegetation Index (NDVI). Time series analyses of NDVI are generated based on user-selected parameters.
 </p>
 <p> Isolating a certain region of interest can be accomplished by clicking and dragging a rectangle across that region. The map will zoom to that area to allow for more precise variable. Any zoom level will allow for the user to click a location and generate 2 separate time series. Changing the area over which the spatial averaging is performed will modify the resolution of variable estimation. 
  </p>
 
 <p> Time series a: monthly estimates of NDVI for the selected region selected for the last 12 months.
  </p>
 <p> Time series b: The monthly estimates of NDVI for the current and five most recent years are plotted for comparison. The thick black line is the same series shown in a. 
  </p>
<div class="buttonInstructions"></div>
</div>
</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Share</legend></fieldset>
</div>
 </body>
 </html>