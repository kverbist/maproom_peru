<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#" xmlns:og="http://ogp.me/ns#" 
      version="XHTML+RDFa 1.0"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<meta xml:lang="" property="maproom:Entry_Id" content="Monitoring" />
<title>Índice de la Diferencia de la Vegetación Normalizada (NDVI)</title>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="NDVI.html?Set-Language=en" />
<link class="share" rel="canonical" href="NDVI.html" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_NDVI_term"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Example"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_Precipitation_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#monthly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded" />
<link rel="term:icon" href="http://iridl.ldeo.columbia.edu/expert/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlyavg/.NDVI/X/-81.995/0.01/-67.505/0.8/evengridAverage/Y/-18.995/0.01/0.995/0.8/evengridAverage/2/RECHUNK/T//defaultvalue/%7Blast%7Ddef/pop//long_name/%28Normalized%20Difference%20Vegetation%20Index%29/def/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.Countrymask/.mask_coarse/mul/X/Y/fig:/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef+//plotborder+0+psdef//plotaxislength+432+psdef//color_smoothing+1+psdef+.gif" />
<script type="text/javascript" src="../../../uicore/uicore.js" xml:space="preserve"></script>
<script type="text/javascript" src="../../../maproom/unesco.js" xml:space="preserve"></script>
<style xml:space="preserve">
.dlimgtsbox { 
width: 49%;
display: inline-block
 }
body.varreflectance img.dlauximg{display: none}
body.varreflectance .regionwithinbbox{display: none !important}
</style>
</head>
<body  xml:lang="es">
      <form name="pageform" id="pageform" class="info carryup carry share dlimg dlauximg dlimgts dlimgloc" method="get" enctype="application/x-www-form-urlencoded">
         <input class="carryLanguage carryup carry" name="Set-Language" type="hidden" />
         <input class="carry dlimg dlimgloc admin share" name="bbox" type="hidden" />
         <input class="dlimg dlauximg dlimgts onlyvar share" name="ana" type="hidden" />
         <input class="share dlimgts dlimgloc" name="region" type="hidden" />
         <input class="dlimg share" name="T" type="hidden" />
         <input class="unused dlimg" name="plotaxislength" type="hidden" value="432" />
         <input class="share dlimgloc dlimgts admin pickarea" name="resolution" type="hidden"
	 data-default="0.1" />
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>Maproom</legend> 
            		<a rev="section" class="navlink carryup" href="/maproom/Monitoring/" shape="rect">Monitoreo</a>
            </fieldset> 
            <fieldset class="navitem" id="chooseSection"> 
            <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_NDVI_term"><span property="term:label">Sequía Agrícola</span></legend>
            </fieldset> 
         <fieldset class="navitem">
            
            <legend>Region</legend>
            <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsPeru.json" shape="rect"></a>
            <select class="RegionMenu" name="bbox">
               <option value="">Peru</option>
               <optgroup class="template" label="Región">
                  <option class="irigaz:hasPart irigaz:id@value term:label"></option>
               </optgroup></select>
            
         </fieldset>
            <fieldset class="navitem">
            <legend>Análisis</legend><span class="selectvalue"></span><select class="pageformcopy" name="ana">
               <option value="">Anomalía</option>
	       <option value="Observed">Observado</option></select>
            </fieldset>
 </div>
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
            <li><a href="#tabs-1" shape="rect">Descripción</a></li>
            <li><a href="#tabs-2" shape="rect">Más información</a></li>
            <li><a href="#tabs-3" shape="rect">Fuente</a></li>
            <li><a href="#tabs-4" shape="rect">Soporte</a></li>
            <li><a href="#tabs-5" shape="rect">Instrucciones</a></li>
    </ul>
<fieldset class="dlimage regionwithinbbox">
           
            
            
            <div style="float: left;">
               <img class="dlimgloc"
                    src="http://iridl.ldeo.columbia.edu/SOURCES/.WORLDBATH/.bath/X/Y/%28bb:-85%2C-19%2C-65%2C1%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-75%2C-5%2C-74.5%2C-4.5%29/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/grey/mews_prov/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif" />
               
               
               
            </div>	
            
            
            
            
            <div style="float: left;">
               	
               
               
               
               <div class="valid" style="display: inline-block; text-align: top;">
                  <a class="dlimgts" rel="iridl:hasJSON" href="http://www.climatedatalibrary.cl/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json" shape="rect"></a>
                  
                  
                  <div class="template"> Observaciones para <span class="bold iridl:long_name"></span>
                     
                     
                     
                  </div>
                  
                  
                  
               </div>
               
               
               
               
               
               <div class="valid" style="text-align: top;">
                  
                  
                  
                  
                  <div> Valores del NDVI para el mes pasado: </div> 
		  <a class="dlimgloc" rel="iridl:hasJSON" href="http://iridl.ldeo.columbia.edu/expert/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlyavg/.NDVI//long_name/%28NDVI%29/def/T/last/VALUE/%28bb%3A-75%2C-5%2C-74.5%2C-4.5%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlyavg/.NDVI/X/-81.99895/0.01/-67.49897/0.8/evengridAverage/Y/0.9994724/0.01/-19.00036/0.8/evengridAverage/2/RECHUNK/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlyclimcoarse/.NDVI/X/-81.99895/0.01/-67.49897/0.8/evengridAverage/Y/0.9994724/0.01/-19.00036/0.8/evengridAverage/2/RECHUNK/sub/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlysdcoarse/.NDVI/X/-81.99895/0.01/-67.49897/0.8/evengridAverage/Y/0.9994724/0.01/-19.00036/0.8/evengridAverage/2/RECHUNK/div//long_name/%28Anomal%C3%ADa%20de%20la%20NDVI%3A%20%29/def/%5BT%5DREORDER/T/last/VALUE/%28bb%3A-75%2C-5%2C-74.5%2C-4.5%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average/2/ds/info.json" shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "+td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"

}
}
}
</script>
                  <div>
                     
                     
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="name " rowspan="1" colspan="1"></td>
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                        </tr>
                     </table>
                     
                     
                     
                  </div>
                  
                  
                  
                  
               </div>
               
               
               
            </div>
            
            <br clear="none" />
            <img class="dlimgts regionwithinbbox" src="http://iridl.ldeo.columbia.edu/expert/%28Anomaly%29//ana/parameter/%28Anomaly%29eq/%7BSOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlyavg/.NDVI/X/-81.99895/0.01/-67.49897/0.8/evengridAverage/Y/0.9994724/0.01/-19.00036/0.8/evengridAverage/2/RECHUNK/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlyclimcoarse/.NDVI/X/-81.99895/0.01/-67.49897/0.8/evengridAverage/Y/0.9994724/0.01/-19.00036/0.8/evengridAverage/2/RECHUNK/sub/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlysdcoarse/.NDVI/X/-81.99895/0.01/-67.49897/0.8/evengridAverage/Y/0.9994724/0.01/-19.00036/0.8/evengridAverage/2/RECHUNK/div/T//defaultvalue/%7Blast%7Ddef/pop//long_name/%28NDVI%20anomaly%29def/T/last/dup/18.0/sub/exch/RANGE%5BT%5DREORDER/%28bb:%5B-75%2C-5%2C-74.0%2C-4.0%5D%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/pdsi_colorbars/DATA/-2.5/2.5/RANGE/dup/T/fig-/colorbars2/-fig%7Dif//ana/get_parameter/%28Observed%29eq/%7BSOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlyavg/.NDVI//long_name/%28NDVI%29def/T/last/dup/18.0/sub/exch/RANGE/%28bb:%5B-75%2C-5%2C-74.0%2C-4.0%5D%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/toNaN%5BT%5DREORDER/CopyStream/pdsi_colorbars/DATA/0.0/1.0/RANGE/dup/T/fig-/colorbars2/-fig%7Dif/+.gif" />  
            
            
            
         </fieldset>  

         
 		<fieldset class="dlimage">
            <a class="justsregion" rel="iridl:hasFigure" href="http://iridl.ldeo.columbia.edu/expert/%28Anomaly%29//ana/parameter/%28Observed%29/eq/%7B/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlyavg/.NDVI/X/-81.995/0.01/-67.505/0.8/evengridAverage/Y/-18.995/0.01/0.995/0.8/evengridAverage/2/RECHUNK/T//defaultvalue/%7Blast%7Ddef/pop//long_name/%28Normalized%20Difference%20Vegetation%20Index%29/def/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.Countrymask/.mask_coarse/mul/X/Y/fig:/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef%7Dif//ana/get_parameter/%28Anomaly%29/eq/%7BSOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlystdanomcoarse/.NDVI/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.Countrymask/.mask_coarse/mul/T//defaultvalue/%7Blast%7Ddef/pop//long_name/%28NDVI%20anomaly%29/def//plotlast/3/def//plotfirst/-3/def/std_wasp_colors/X/Y/fig:/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//NDVI/-3.5/3.5/plotrange//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef%7Dif//T/last/plotvalue/Y/-19/1/plotrange//plotaxislength/432/psdef//XOVY/null/psdef//plotborder/72/psdef/" shape="rect"></a>
            <img class="dlimg" src="http://iridl.ldeo.columbia.edu/expert/%28Anomaly%29//ana/parameter/%28Observed%29/eq/%7B/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlyavg/.NDVI/X/-81.995/0.01/-67.505/0.8/evengridAverage/Y/-18.995/0.01/0.995/0.8/evengridAverage/2/RECHUNK/T//defaultvalue/%7Blast%7Ddef/pop//long_name/%28Normalized%20Difference%20Vegetation%20Index%29/def/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.Countrymask/.mask_coarse/mul/X/Y/fig:/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef%7Dif//ana/get_parameter/%28Anomaly%29/eq/%7BSOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlystdanomcoarse/.NDVI/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.Countrymask/.mask_coarse/mul/T//defaultvalue/%7Blast%7Ddef/pop//long_name/%28NDVI%20anomaly%29/def//plotlast/3/def//plotfirst/-3/def/std_wasp_colors/X/Y/fig:/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//NDVI/-3.5/3.5/plotrange//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef%7Dif//T/last/plotvalue/Y/-19/1/plotrange//plotaxislength/432/psdef//XOVY/null/psdef//plotborder/72/psdef/+.gif" border="0" />
            <img class="dlauximg" src="http://iridl.ldeo.columbia.edu/expert/%28Anomaly%29//ana/parameter/%28Observed%29/eq/%7B/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlyavg/.NDVI/X/-81.995/0.01/-67.505/0.8/evengridAverage/Y/-18.995/0.01/0.995/0.8/evengridAverage/2/RECHUNK/T//defaultvalue/%7Blast%7Ddef/pop//long_name/%28Normalized%20Difference%20Vegetation%20Index%29/def/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.Countrymask/.mask_coarse/mul/X/Y/fig:/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef%7Dif//ana/get_parameter/%28Anomaly%29/eq/%7BSOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlystdanomcoarse/.NDVI/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.Countrymask/.mask_coarse/mul/T//defaultvalue/%7Blast%7Ddef/pop//long_name/%28NDVI%20anomaly%29/def//plotlast/3/def//plotfirst/-3/def/std_wasp_colors/X/Y/fig:/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//NDVI/-3.5/3.5/plotrange//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef%7Dif//T/last/plotvalue/Y/-19/1/plotrange//plotaxislength/432/psdef//XOVY/null/psdef//plotborder/72/psdef/+.auxfig/+.gif" />
            
         </fieldset>
         
         
         <div id="tabs-1" class="ui-tabs-panel" about="">
            
            <h3 align="center" property="term:title">Índice de la Diferencia de la Vegetación Normalizada (NDVI)</h3>
            
            <p align="left" property="term:description">Este mapa muestra la condición de la vegetación actual, como reflejado por el Índice
               de la Diferencia de la Vegetación Normalizada (NDVI).
            </p>
            
            <p>El NDVI usa información de reflejo de dos regiones espectrales: región de luz visible
               y región de infrarrojo cercano. La condición de la vegetación influya la interacción
               de estas dos regiones espectrales y la vegetación. Así el NDVI da información sobre
               la condición de la vegetación. El NDVI tiene un valor entre -1 y +1. 
               
            </p>
            
            <p>Se puede visualizar  el NDVI observado y su anomalía. Selecciona la variable de interés
               en el menú&gt;análisis. En el menú&gt;región se puede seleccionar la región de interés.
            </p>
            
            <p align="left"><b>Observado:</b>
               El NDVI observado tiene un valor entre -1 y +1 y está calculado mensualmente. Cuando
               el NDVI está cerca de +1 indica abundancia de la vegetación. Por ejemplo, un área
               de bosque resulta en un NDVI más cercano a +1 en comparación con un valor 0 para el
               desierto. 
            </p>
            
            <p align="left"><b>Anomalía:</b>
               La anomalía del NDVI indica la deviación del NDVI comparado con el promedio. Valores
               positivos indican que el NDVI es mayor que lo normal en este mes y lugar. Valores
               negativos indican que el NDVI es menor a lo esperado normalmente. 
            </p>
            
            <p align="left">
               <h6> Tabla 1: Interpretación de la Anomalía Estandarizada</h6>
            </p>
            
            <p align="left"><img src="Escala_anomalias_vegetacion_esp.jpg" alt="Leyenda de la Anomalía Estandarizada"> </img></p>
            
            <p>Se puede generar gráficos del NDVI en cada punto de interés. Selecciona la pestaña
               "Instrucciones" para mayor información.
            </p>
            
            
         </div>
         
         
         <div id="tabs-2" class="ui-tabs-panel">
            
            <h3 align="center">¿Cómo se calcula el NDVI?</h3>
            
            
            <p>
               El NDVI es calculado de la siguiente forma:
               
            </p>
            
            <p align="left"><img src="NDVI_formula.png" alt="NDVI"> </img></p>
            
            <p>Donde el VIS es el reflejo espectral en la región visible (rojo) expresado como ratio
               (reflejo/entrante) y el NIR es el reflejo espectral en la región infrarrojo cercano
               expresado como ratio (reflejo/entrante).
            </p>
            
            
            <p>En la figura abajo vez una ilustración de una calculación de NDVI.</p>
            
            <p align="left"><img src="NDVI_figura.png" alt="NDVI"> </img></p>
            
            <p align="left">
               <h6>Figura 1: Ejemplo de calculación del NDVI</h6>
            </p>
            
            <h3 align="center">¿Qué es la interacción entre la vegetación y el NIR y VIS?</h3>
            
            <p>Plantas absorben y reflejan regiones particulares del espectro. Gran parte del VIS
               es absorbido por plantas como energía para la fotosíntesis. Gran parte del NIR es
               reflejado por plantas por qué no puede ser utilizado en la fotosíntesis. Cuando plantas
               están en malas condiciones hay un cambio en el reflejo y absorbencia del VIS y NIR.
            </p>
            
         </div>
         
         
         <div id="tabs-3" class="ui-tabs-panel">
            
            <h3 align="center">Fuente de los Datos</h3>
            
            <p align="left"> <a href="http://climatedatalibrary.cl/SOURCES/.Chile/.Analysis/.MODIS/.Jan/.NDVI/" shape="rect">NDVI</a>, entregado por United States Geological Survey, Land Processes Distributed Active
               Archive Center, 
               Moderate Resolution Imaging Spectroradiometer <a href="http://iridl.ldeo.columbia.edu/SOURCES/.USGS/.LandDAAC/.MODIS/.version_005/.dataset_documentation.html" shape="rect">USGS LandDAAC MODIS</a></p>
            
         </div>
         
         
         <div id="tabs-4" class="ui-tabs-panel">
            
            <h3 align="center">Soporte</h3>
            
            <p>
               Sus consultas a: <a href="mailto:snirh@ana.gob.pe?subject=Precipitacion Observada Per%C3%BA" shape="rect">snirh@ana.gob.pe</a>
               
            </p>
            
         </div>
         
         <div id="tabs-5" class="ui-tabs-panel">
            
            <h3 align="center">Instrucciones</h3>
            
            <p>
               El interface con datos de MODIS permite a usuarios de crear gráficos. El interface
               entrega los más recientes imagenes de MODIS, entregando mapas interactivos de NDVI.
               Análisis de la series de tiempo de NDVI están generados basados en parámetros seleccionados
               por parte del usuario.
               
            </p>
            
            <p> La seleccion de una región de interés se realiza con la selección de un rectangulo
               sobre la region de interes. El mapa se centra sobre esta region para mejor seleccion.
               El usuario puede indicar el punto de interés por lo cual se genera 2 series de tiempo:
               
               
            </p> 
            
            <p>
               a) Estimaciones de 1 mes de NDVI para la zona seleccionado durante los últimos 12
               meses.  
               
            </p> 
            <p>
               b) Estimaciones de 1 mes de NDVI para el año actual, comparado con los 5 años más
               recientes. La linea negra es la misma serie de tiempo que mostrado en la figura a).
               
               
            </p>
            
            <div class="buttonInstructions"></div>
            
         </div>
         
      </div>
      
      <div class="optionsBar">
         
         <fieldset class="navitem" id="share">
            <legend>Compartir</legend>
         </fieldset>
         
      </div>
      
   </body>
</html>