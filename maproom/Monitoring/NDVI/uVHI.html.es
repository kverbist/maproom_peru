<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      xmlns:og="http://ogp.me/ns#" 
      version="XHTML+RDFa 1.0"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<meta xml:lang="" property="maproom:Entry_Id" content="Monitoring" />
<title>Categorías de Sequía Agrícola (u*VHI)</title>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="uVHI.html?Set-Language=en" />
<link class="share" rel="canonical" href="uVHI.html" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_NDVI_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Example"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_Precipitation_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#monthly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded" />
<link rel="term:icon" href="http://ons.snirh.gob.pe/SOURCES/.Peru/.CAZALAC/.ASIS/.uVHI/.uVHI/long_name/%28uV%20H%20I%29def/X/Y/fig-/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//aprod/0/10/plotrange/X/-71.6/-68.4/plotrange/Y/-12.4/-17.5/plotrange//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef//T/last/plotvalue/Y/-12.4/-17.5/plotrange//plotborder/72/psdef//plotaxislength/432/psdef//XOVY/null/psdef/+.gif?plotaxislength=520" />
<script type="text/javascript" src="../../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../../maproom/unesco.js"></script>
</head>
<body  xml:lang="es">
<form name="pageform" id="pageform" class="carryup carry dlimg dlauximg share" method="get" enctype="application/x-www-form-urlencoded">
<input class="carryLanguage carryup carry " name="Set-Language" type="hidden" />
<input class="dlimg admin share" name="bbox" type="hidden" />
<input class="dlimg dlauximg onlyvar dlimgts share" name="var" type="hidden" />
<input class="dlimg share" name="T" type="hidden" />
<input class="dlimg" name="plotaxislength" type="hidden" />
<input class="share dlimgloc dlimgts" name="region" type="hidden" />
<input class="share dlimgloc dlimgts admin" name="resolution" type="hidden" data-default="irids:SOURCES:Features:Hydrological:Cuencas:Mayores:ds" />
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>Maproom</legend> 
            		<a rev="section" class="navlink carryup" href="/maproom/Monitoring/" shape="rect">Monitoreo</a>
            </fieldset> 
            <fieldset class="navitem" id="chooseSection"> 
            	<legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Monitoring_NDVI_term"><span property="term:label">Sequía Agrícola</span></legend>
            </fieldset> 

		 
		 <fieldset class="navitem">
            
            
            <legend>Tipo de Cultivo</legend><span class="selectvalue"></span><select class="pageformcopy" name="var">
               <option value="avena">Avena Forrajera</option>
               <option value="papa">Papa</option>
			   <option value="quinua">Quinua</option>
			   <option value="cebada">Cebada Forrajera</option>
               </select>
            
            
         </fieldset>
		 

		 		 	
 </div>
 
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
            <li><a href="#tabs-1" shape="rect">Descripción</a></li>
            <li><a href="#tabs-2" shape="rect">Más información</a></li>
            <li><a href="#tabs-3" shape="rect">Fuente</a></li>
            <li><a href="#tabs-4" shape="rect">Soporte</a></li>
            <li><a href="#tabs-5" shape="rect">Instrucciones</a></li>
    </ul>

        <fieldset class="dlimage regionwithinbbox">
            <a class="dlimgts" rel="iridl:hasTable" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.CAZALAC/.ASIS/.uVHI/%5B/(.)/(uVHI)//var/parameter/%5Dconcat/interp/-5/maskle/(bb%3A-70%2C-5.0%2C-69%2C-4.5)//region/parameter/geoobject/%5BX/Y%5Dweighted-average//long_name/(ASI)/def//units/(-)/def/T/exch/table-/text/text/skipanyNaN/-table/.html/" shape="rect"></a>
            
        <div style="float: left;">
               <img class="dlimgloc" src="http://ons.snirh.gob.pe/SOURCES/.Peru/.CAZALAC/.MinExpectedPrecip/.5YR/X/Y/%28bb:-82%2C-17%2C-68%2C0%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-76%2C-8%2C-76.2%2C-8.2%29/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborder/0/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotaxislength/120/psdef/+.gif" />
               
               
               
               
            </div>    
            
            
            
            
            
            
            <div style="float: left;">
               	
               
               
               
               <div class="valid" style="display: inline-block; text-align: top;">
                  <a class="dlimgts" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json" shape="rect"></a>
                  
                  
                  
                  <div class="template"> Observaciones para <span class="bold iridl:long_name"></span>
                     
                     
                     
                  </div>
                  
                  
                  
               </div>
               
               
               
               
               
               <div class="valid" style="text-align: top;">
                  
                  
                  
                  <div>Valores del uVHI para el mes actual: </div> 
                  <a class="dlimgloc" rel="iridl:hasJSON" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.CAZALAC/.ASIS/.uVHI/avena/1/maskgt/T/last/VALUE/%28bb:-71.5%2C-5.0%2C-71.0%2C-4.5%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//long_name/%28uVHI:%20%29/def/1/ds/info.json" shape="rect"></a>
                  <script type="application/json" property="iridl:hasPUREdirective" xml:space="preserve">
{"tr": {
 "var&lt;-iridl:hasDependentVariables": {
  "+td.name": "var.iridl:long_name",
 "td.name@class+": "var.iridl:name",
 "+td.value": "var.iridl:value",
"td.value@class+": "var.iridl:name"

}
}
}
</script>
                  <div>
                     
                     
                     <table class="valid template">
                        <tr style="color : black">
                           <td class="name " rowspan="1" colspan="1"></td>
                           <td align="right" class="value " rowspan="1" colspan="1"></td>
                        </tr>
                     </table>
                     
                     
                     
                  </div>
                  
                  
                  
                  
               </div>
               
               
               
            </div>
            
            <br clear="none" />
            <img class="dlimgts regionwithinbbox" src="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.CAZALAC/.ASIS/.uVHI/%5B/%28.%29/%28avena%29//var/parameter/%5Dconcat/interp/1/maskgt/%28bb:-71.5%2C-5.0%2C-71.0%2C-4.5%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average/uvhi_colors/DATA/0/1.15/RANGE//long_name/%28uVHI%29/def/dup/T/fig-/colorbars2/-fig/+.gif" />  
            
            
            
         </fieldset> 
         
         
         
         
         <fieldset class="dlimage" id="content" about="">
            
           
            
            <link rel="iridl:hasFigure" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.CAZALAC/.ASIS/.uVHI/%5B/(.)/(avena)//var/parameter/%5Dconcat/interp/SOURCES/.Peru/.CAZALAC/.ASIS/.Peru_mask/.ASI_mask2/mul//name//ASIs/def/a-/-a/uvhi_colors/X/Y/fig%3A/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/%3Afig//T/last/plotvalue//Y/-17/-13.5/plotrange//X/-72/-68/plotrange//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//XOVY/null/psdef//layers%5B//ASIs//coasts_gaz//lakes//countries_gaz//states_gaz%5Dpsdef/" />
            <img class="dlimg" src="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.CAZALAC/.ASIS/.uVHI/%5B/%28.%29/%28avena%29//var/parameter/%5Dconcat/interp/SOURCES/.Peru/.CAZALAC/.ASIS/.Peru_mask/.ASI_mask2/mul//name//ASIs/def/a-/-a/uvhi_colors/X/Y/fig:/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//T/last/plotvalue//Y/-17/-13.5/plotrange//X/-72/-68/plotrange//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//XOVY/null/psdef//layers%5B//ASIs//coasts_gaz//lakes//countries_gaz//states_gaz%5Dpsdef/+.gif" border="0" alt="image" /><br clear="none" />
            <img class="dlauximg" src="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.CAZALAC/.ASIS/.uVHI/%5B%28.%29%28uVHI%29//var/parameter%5Dconcat/interp/-5/maskle/SOURCES/.Peru/.CAZALAC/.ASIS/.Peru_mask/.uVHI_mask/mul//plotlast/1.125/def//plotfirst/-0.125/def//name//SPI/def//long_name/%28uVHI%29def/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//T/last/plotvalue//Y/-18.5/0/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef//layers%5B//SPI//lakes//coasts_gaz//countries_gaz//states_gaz%5Dpsdef/+.auxfig/+.gif" />
            
            
            
         </fieldset>
         
         
         <div id="tabs-1" class="ui-tabs-panel" about="">
            
            <h3 align="center" property="term:title">Categorías de Sequía Agrícola (u*VHI)</h3>
            
            <p align="justify" property="term:description">Este mapa muestra la categoría de sequía agrícola, el cual informa sobre la condición de sequía agrícola en el Departamento de Puno.
            </p>
            
            <p align="justify">
            </p>
            <p align="center">
               <h6 align="center" > Tabla 1: Escala de Categorías de Sequía Agrícola</h6>
            </p>
            <p align="center"><img src="escala_uvhi_v1.png" alt="uVHI"> </img></p>
            <p align="justify">Este indice está disponible para diferentes tipos de cultivo: Avena Forrajera, Papa, Quinua, Cebada Forrajera. Selecciona el tipo de cultivo de interés en el Menú > Tipo de Cultivo.</p>
            
            
         </div>
         
         
         <div id="tabs-2" class="ui-tabs-panel">
            
            <h3 align="center">Categorías de Sequía Agrícola</h3>
            
            <p align="justify"></p>
            
            
            
         </div>
         
         
         <div id="tabs-3" class="ui-tabs-panel">
            
            <h3 align="center">Fuente de los Datos</h3>
            
            <p align="left"><a href="#" shape="rect">METOP-AVHRR</a></p>

		 </div>
         
         
         <div id="tabs-4" class="ui-tabs-panel">
            
            <h3 align="center">Soporte</h3>
            
            <p>
               Sus consultas a: <a href="mailto:snirh@ana.gob.pe?subject=CDI Per%C3%BA" shape="rect">snirh@ana.gob.pe</a>
               
            </p>
            
         </div>
         
         <div id="tabs-5" class="ui-tabs-panel">
            
            <h3 align="center">Instrucciones</h3>
            
            <div class="buttonInstructions"></div>
            
         </div>
         
         
      </div>
      
      <div class="optionsBar">
         
         <fieldset class="navitem" id="share">
            <legend>Compartir</legend>
         </fieldset>
         
      </div>
      
   </body>
</html>