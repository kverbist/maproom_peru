<?xml version="1.0" encoding="utf-8"?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:wms="http://www.opengis.net/wms#" xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#" xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#" xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#" xmlns:xs="http://www.w3.org/2001/XMLSchema#" xmlns:og="http://ogp.me/ns#" version="XHTML+RDFa 1.0" xml:lang="es">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="description" xml:lang="es" content="Mapas para el monitoreo de sequía a través de indicadores relevantes." />
      <meta name="twitter:card" content="summary_large_image" />
      <meta property="og:title" content="Monitoreo de Sequía" />
      <meta property="og:description" content="Mapas para el monitoreo de sequía a través de indicadores relevantes." />
      <meta property="og:image" content="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.SENAMHI/.PISCO/.SPI/.SPI1/SOURCES/.Peru/.SENAMHI/.PISCO/.Peru_mask/.SPI_mask/mul//plotlast/3/def//plotfirst/-3/def//name//SPI/def//long_name/%28Indice%20de%20Precipitacion%20Estandarizado%29/def/T/last/VALUE/%5BT%5Daverage/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//Y/-18.5/0/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef+//plotborder+0+psdef//plotaxislength+432+psdef//antialias+true+psdef//color_smoothing+1+psdef+.gif" />
      <meta name="viewport" content="width=device-width; initial-scale=1.0;" />
      <title>Monitoreo de Indicadores de Sequía</title>
      <link rel="stylesheet" type="text/css" href="../unesco.css" />
      <link class="altLanguage" rel="alternate" hreflang="en" href="index.html?Set-Language=en" />
      <link rel="canonical" href="index.html" />
      <meta property="maproom:Sort_Id" content="a04" />
      <link class="" rel="home" href="http://www.minagri.gob.cl/agroclimatico/unidad_nacional.php/" title="UNEA" />
      <link class="carryLanguage" rel="home alternate" type="application/json" href="../../maproom//navmenu.json" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#monitoring" />
      <link rel="term:icon" href="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.SENAMHI/.PISCO/.SPI/.SPI1/SOURCES/.Peru/.SENAMHI/.PISCO/.Peru_mask/.SPI_mask/mul//plotlast/3/def//plotfirst/-3/def//name//SPI/def//long_name/%28Indice%20de%20Precipitacion%20Estandarizado%29/def/T/last/VALUE/%5BT%5Daverage/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//Y/-18.5/0/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef+//plotborder+0+psdef//plotaxislength+432+psdef//antialias+true+psdef//color_smoothing+1+psdef+.gif" /><script type="text/javascript" src="../../uicore/uicore.js" xml:space="preserve"></script><script type="text/javascript" src="../../maproom/unesco.js" xml:space="preserve"></script></head>
   <body>
      
      
      <form name="pageform" id="pageform" method="get" enctype="application/x-www-form-urlencoded">
         <input class="carryLanguage carryup" name="Set-Language" type="hidden" />
         <input class="carry titleLink itemImage" name="bbox" type="hidden" />
         
      </form>
      
      <div class="controlBar">
         
         <fieldset class="navitem"> 
            
            <legend>Peru</legend> 
            <a rev="section" class="navlink carryup" href="/maproom/" shape="rect">Observatorio Nacional de Sequía</a>
            
         </fieldset> 
         
         <fieldset class="navitem"> 
            
            <legend>Peru</legend> 
            <span class="navtext">Monitoreo de Sequía</span>
            
         </fieldset> 
         
         <fieldset class="navitem">
            
            <legend>Región</legend>
            <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsPeru.json" shape="rect"></a>
            <select class="RegionMenu" name="bbox">
               <option value="">Peru</option>
               <optgroup class="template" label="Región">
                  <option class="irigaz:hasPart irigaz:id@value term:label"></option>
               </optgroup></select>
            
         </fieldset>
         
      </div>
      
      <div>
         
         <div id="content" class="searchDescription">
            
            <h2 property="term:title">Monitoreo de Sequía</h2>
            
            <p align="left" property="term:description">
               Mapas para el monitoreo de sequía a través de indicadores relevantes.
               
            </p>
            <p>Los indicadores fueron seleccionados para mostrar índices de sequía meteorológico
               (déficit pluviométrico), sequía hidrológico (déficit hidrico) y sequía agrícola (déficit
               en la vegetación).
               
            </p>
            
         </div>
         
         <div class="rightcol">
            <div class="ui-tabs">
               <ul class="ui-tabs-nav">
                  <li><a href="#tabs-1">Sequía Meteorológica</a></li>
                  <li><a href="#tabs-2">Sequía Hidrológica</a></li>
                  <li><a href="#tabs-3">Sequía Agrícola</a></li>
                  <li><a href="#tabs-4">Índice de Sequía Combinado</a></li>
               </ul>
               <div id="tabs-1" class="ui-tabs-panel">
                  <div class="itemGroup">Sequía Meteorológica</div>
                  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="Meteorological/DiasSecos.html">Número de Días sin Lluvia</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="Meteorological/DiasSecos.html"><img class="itemImage" src="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.UnifiedPrecipitation/.Monthly/.NrDaysWithoutRain_anomaly//long_name/%28Anomal%C3%ADa%20de%20d%C3%ADas%20secos%20comparado%20con%20lo%20normal%29/def//units/%28dias%29/def/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//NrDaysWithoutRain_anomaly/-10.0/10.0/plotrange//T/last/plotvalue//Y/-18.5/0/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef//T/last/plotvalue//Y/-18.5/0/plotrange//plotaxislength/432/psdef//plotborder/72/psdef/+.gif" /></a></div>
                     <div class="itemDescription">Este mapa muestra el número de días sin lluvia en un mes. Se considera que un día
                        fue lluvioso si la fuente detectó una precipitación mínima de 1 mm.
                     </div>
                     <div class="itemFooter"></div>
                  </div>
                  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="Meteorological/Precipitation_METEONET.html">Precipitación Observada - METEONET</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="Meteorological/Precipitation_METEONET.html"><img class="itemImage" src="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Tiempo_real/.Precipitacion/.Mensual/lon/lat/2/copy/Monthly_precip/-20/masklt/DATA/0/AUTO/RANGE/6/-1/roll/pop//long_name/(Precipitacion%20observada)/def//name/(st_precip)/def/precip_colors/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//Y/-19/0/plotrange//st_precip/-3/3/plotrange/(antialias)/true/psdef/(fntsze)/20/psdef//antialias/true/psdef//fntsze/20/psdef//plotaxislength/432/psdef//color_smoothing/1/psdef//plotborder/0/psdef+.gif" /></a></div>
                     <div class="itemDescription">Este mapa muestra la precipitación observada en las estaciones meteorológicas del
                        METEONET del Perú (ANA). Las observaciones tienen la unidad [mm/mes].
                     </div>
                     <div class="itemFooter"></div>
                  </div>
                  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="Meteorological/Precipitation_SENAMHI.html">Precipitación Observada - SENAMHI</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="Meteorological/Precipitation_SENAMHI.html"><img class="itemImage" src="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.SENAMHI/.PISCO/.Precipitation/.Monthly/.precip_percentage/SOURCES/.Peru/.SENAMHI/.PISCO/.Peru_mask/.SPI_mask/mul//long_name/%28Precipitacion-Porcentaje%20del%20Promedio%29/def/pdsi_colorbars//symmetric/false/def/DATA/0/100/RANGE/T/last/VALUE/X/Y/fig-/colors/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange+//plotborder+0+psdef//plotaxislength+432+psdef//fntsze+20+psdef//antialias+true+psdef//color_smoothing+1+psdef+.gif" /></a></div>
                     <div class="itemDescription">Este mapa muestra la precipitación observada en las estaciones meteorológicas del
                        Servicio Nacional de Meterología e Hidrología del Perú (SENAMHI).
                     </div>
                     <div class="itemFooter"></div>
                  </div>
                  <div class="item">
                     <div class="itemTitle"><a class="carry titleLink" href="Meteorological/SPI.html">Índice de Precipitación Estandarizado</a></div>
                     <div class="itemIcon"><a class="carry titleLink" href="Meteorological/SPI.html"><img class="itemImage" src="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.SENAMHI/.PISCO/.SPI/.SPI1/SOURCES/.Peru/.SENAMHI/.PISCO/.Peru_mask/.SPI_mask/mul//plotlast/3/def//plotfirst/-3/def//name//SPI/def//long_name/%28Indice%20de%20Precipitacion%20Estandardizado%29/def/T/last/VALUE/%5BT%5Daverage/X/Y/fig-/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/-fig//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//Y/-18.5/0/plotrange//XOVY/null/psdef//color_smoothing/null/psdef//antialias/true/psdef+//plotborder+0+psdef//plotaxislength+432+psdef//antialias+true+psdef//color_smoothing+1+psdef+.gif" /></a></div>
                     <div class="itemDescription">Este mapa muestra el índice de Precipitación Estandarizado (SPI por sus siglas en inglés) para múltiples periodos de acumulación.
                     </div>
                     <div class="itemFooter"></div>
                  </div>
               </div>
               <div id="tabs-2" class="ui-tabs-panel">
                  <div class="itemGroup">Sequía Hidrológica</div>
                  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="Hydrological/Caudal_METEONET.html">Niveles de Ríos Observados - METEONET</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="Hydrological/Caudal_METEONET.html"><img class="itemImage" src="http://ons.snirh.gob.pe/expert/%28Observed%29//anal/parameter/%28Observed%29eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Tiempo_real/.Nivel/.Mensual/lon/lat/2/copy/Nivel_mensual//units/%28m%29def/6/-1/roll/pop//long_name/%28Niveles%20de%20los%20Rios%29def/X/Y/fig-/colors/||/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef+//plotborder+0+psdef//plotaxislength+432+psdef//fntsze+20+psdef//antialias+true+psdef//color_smoothing+1+psdef+.gif" /></a></div>
                     <div class="itemDescription">Esta información es recopilada por parte del METEONET del ANA.</div>
                     <div class="itemFooter"></div>
                  </div>
                  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="Hydrological/DischargeDGA.html">Volumen de Embalses Observados</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="Hydrological/DischargeDGA.html"><img class="itemImage" src="http://ons.snirh.gob.pe/expert/%28Percentage%29//anal/parameter/%28Percentage%29/eq/%7BSOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Embalses/.Monthly/lon/lat/2/copy/Monthly_level_percentage_average/-20/masklt/DATA/-4/4/RANGE/6/-1/roll/pop//long_name/%28Volumen%20embalse%20-Porcentaje%20del%20Normal%29/def//name/%28st_precip%29/def/pdsi_colorbars//symmetric/false/def/DATA/0/100/RANGE/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//Y/-19/0/plotrange//st_precip/-3/3/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif//anal/get_parameter/%28Observed%29/eq/%7B/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Embalses/.Monthly/lon/lat/2/copy/Monthly_level//units/%28hm3%29/def/6/-1/roll/pop//long_name/%28Volumen%20de%20Embalse%20Observado%29/def/X/Y/fig-/colors/%7C%7C/white/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//antialias/true/psdef//fntsze/20/psdef//T/last/plotvalue//Y/-19/0/plotrange/%7Dif//anal/get_parameter/%28Anomaly%29/eq/%7B/SOURCES/.Peru/.Peru_mask/.background_Peru/SOURCES/.Peru/.ANA/.Embalses/.Monthly/lon/lat/2/copy/Monthly_level_st_anomaly/-20/masklt/DATA/-4/4/RANGE/6/-1/roll/pop//long_name/%28Anomalia%20del%20Volumen%20del%20Embalse%20Estandarizado%29/def//name/%28st_precip%29/def/X/Y/fig-/colors/%7C%7C/black/scatter/scattercolor/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//T/last/plotvalue//Y/-19/0/plotrange//st_precip/-3/3/plotrange/%28antialias%29/true/psdef/%28fntsze%29/20/psdef%7Dif//T/last/plotvalue//plotaxislength/432/psdef//plotborder/72/psdef/+.gif?plotaxislength=660" /></a></div>
                     <div class="itemDescription">Este mapa muestra los volúmenes de embalses observados en las cuencas principales
                        del Perú.
                     </div>
                     <div class="itemFooter"></div>
                  </div>
               </div>
               <div id="tabs-3" class="ui-tabs-panel">
                  <div class="itemGroup">Sequía Agrícola</div>
                  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="NDVI/ASI.html">Índice de Estrés Agrícola (ASI)</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="NDVI/ASI.html"><img class="itemImage" src="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.CAZALAC/.ASIS/.ASI/%5B/%28.%29/%28avena%29//var/parameter/%5Dconcat/interp/SOURCES/.Peru/.CAZALAC/.ASIS/.Peru_mask/.ASI_mask2/mul//name//ASIs/def/a-/-a/asi_colores2/X/Y/fig:/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//T/last/plotvalue//Y/-17/-13.5/plotrange//X/-72/-68/plotrange//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//XOVY/null/psdef//layers%5B//ASIs//coasts_gaz//lakes//countries_gaz//states_gaz%5Dpsdef/+.gif?plotaxislength=620" /></a></div>
                     <div class="itemDescription">Este mapa muestra el índice de Estrés Agrícola, el cual informa sobre el porcentaje
                        de área agrícola afectada por sequía en el Departamento de Puno.
                     </div>
                     <div class="itemFooter"></div>
                  </div>
                  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="NDVI/NDVI.html">Índice de la Diferencia de la Vegetación Normalizada (NDVI)</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="NDVI/NDVI.html"><img class="itemImage" src="http://iridl.ldeo.columbia.edu/expert/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.monthlyavg/.NDVI/X/-81.995/0.01/-67.505/0.8/evengridAverage/Y/-18.995/0.01/0.995/0.8/evengridAverage/2/RECHUNK/T//defaultvalue/%7Blast%7Ddef/pop//long_name/%28Normalized%20Difference%20Vegetation%20Index%29/def/SOURCES/.IRI/.Analyses/.LandDAAC/.MODIS/.version_005/.NSA/.Peru/.Countrymask/.mask_coarse/mul/X/Y/fig:/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef+//plotborder+0+psdef//plotaxislength+432+psdef//color_smoothing+1+psdef+.gif" /></a></div>
                     <div class="itemDescription">Este mapa muestra la condición de la vegetación actual, como reflejado por el Índice
                        de la Diferencia de la Vegetación Normalizada (NDVI).
                     </div>
                     <div class="itemFooter"></div>
                  </div>
                  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="NDVI/VHI.html">Índice de Salud de la Vegetación (VHI)</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="NDVI/VHI.html"><img class="itemImage" src="http://ons.snirh.gob.pe/SOURCES/.Peru/.CAZALAC/.ASIS/.VHI/.VHI/long_name/%28V%20H%20I%29def/X/Y/fig-/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//aprod/0/10/plotrange/X/-71.6/-68.4/plotrange/Y/-12.4/-17.5/plotrange//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef//T/last/plotvalue/Y/-12.4/-17.5/plotrange//plotborder/72/psdef//plotaxislength/432/psdef//XOVY/null/psdef/+.gif?plotaxislength=520" /></a></div>
                     <div class="itemDescription">Este mapa muestra el Índice de Salud de la Vegetación (VHI), el cual informa sobre
                        la condición vegetal actual en el Departamento de Puno.
                     </div>
                     <div class="itemFooter"></div>
                  </div>
                  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="NDVI/uVHI.html">Categorías de Sequía Agrícola (u*VHI)</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="NDVI/uVHI.html"><img class="itemImage" src="http://ons.snirh.gob.pe/expert/SOURCES/.Peru/.CAZALAC/.ASIS/.uVHI/%5B/%28.%29/%28avena%29//var/parameter/%5Dconcat/interp/SOURCES/.Peru/.CAZALAC/.ASIS/.Peru_mask/.ASI_mask2/mul//name//ASIs/def/a-/-a/uvhi_colors/X/Y/fig:/colors/white/ocean/blue/lakes/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts_gaz/:fig//T/last/plotvalue//Y/-17/-13.5/plotrange//X/-72/-68/plotrange//plotaxislength/550/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//XOVY/null/psdef//layers%5B//ASIs//coasts_gaz//lakes//countries_gaz//states_gaz%5Dpsdef/+.gif?plotaxislength=620" /></a></div>
                     <div class="itemDescription">Este mapa muestra la categoría de sequía agrícola, el cual informa sobre la condición
                        de sequía agrícola en el Departamento de Puno.
                     </div>
                     <div class="itemFooter"></div>
                  </div>
               </div>
               <div id="tabs-4" class="ui-tabs-panel">
                  <div class="itemGroup">Índice de Sequía Combinado</div>
                  <div class="item">
                     <div class="itemTitle"><a class="carryLanguage carry titleLink" href="Combined/Combined.html">Índice de Sequía Combinado</a></div>
                     <div class="itemIcon"><a class="carryLanguage carry titleLink" href="Combined/Combined.html"><img class="itemImage" src="http://ons.snirh.gob.pe/SOURCES/.Peru/.CAZALAC/.CDI/.CDI//long_name/%28Indice%20Combinado%20de%20Sequia%29def/X/Y/fig-/colors/black/verythin/countries_gaz/lightgrey/verythin/states_gaz/black/verythin/coasts/-fig//aprod/0/10/plotrange/X/-86.00999/-66.0/plotrange/Y/1/-19/plotrange//plotborder/72/psdef//XOVY/null/psdef//plotaxislength/432/psdef//T/last/plotvalue/Y/-19/1/plotrange//plotborder/72/psdef//plotaxislength/432/psdef//XOVY/null/psdef/+.gif?plotaxislength=520" /></a></div>
                     <div class="itemDescription">Este mapa muestra el índice de Sequía Combinado (CDI por sus siglas en inglés), el
                        cual informa sobre la condición actual de sequía en Perú, para múltiples periodos
                        de acumulación.
                     </div>
                     <div class="itemFooter"></div>
                  </div>
               </div>
            </div>
         </div>
         
         
      </div>
      
      <div class="optionsBar">
         	
         <fieldset class="navitem" id="share">
            <legend>Compartir</legend>
         </fieldset>
         
      </div>
      
   </body>
</html>