<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#" xmlns:og="http://ogp.me/ns#" 
      version="XHTML+RDFa 1.0"
      >
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta xml:lang="es" property="maproom:Entry_Id" content="Riesgo_2016" />
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<title>Planes Para Escenarios de Alto Riesgo Ante Sequía - 2016</title>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="IRI_Forecast.html?Set-Language=en" />
<link class="share" rel="canonical" href="AltoRiesgo.html" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Riesgo_2016_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation"/>
  <link rel="term:icon" href="MapaVulnerabilidad.png" />
  <script type="text/javascript" src="../../../uicore/uicore.js" xml:space="preserve"></script>
  <script type="text/javascript" src="../../maproom/unesco.js" xml:space="preserve"></script>
  </head>
<body xml:lang="es">
<form name="pageform" id="pageform" class="carryup carry dlimg dlauximg dlimgts dlimgloc share starttime startleadtime" method="get" enctype="application/x-www-form-urlencoded">
<input class="carryLanguage carryup carry " name="Set-Language" type="hidden" />
<input class="carry dlimg dlimgloc share" name="bbox" type="hidden" />
<input class="dlimg dlimgts share starttime startleadtime" name="F" type="hidden" />
<input class="dlimg dlimgts share startleadtime" name="L" type="hidden" value="1." />
<input class="dlimgnotuseddlauximg" name="plotaxislength" type="hidden" />
<input class="share dlimgts dlimgloc" name="region" type="hidden" />
<input class="pickarea" name="resolution" type="hidden" value=".5" />
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>Maproom</legend> 
                      <a rev="section" class="navlink carryup" href="/maproom/Riesgo/" shape="rect">Riesgo Ante Sequía</a>
            </fieldset> 
            <fieldset class="navitem" id="chooseSection"> 
                <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Riesgo_2016_term"><span property="term:label">Escenarios de Riesgo</span></legend>
            </fieldset> 
			<!--
            <fieldset class="navitem">
                <legend>Region</legend>
                <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsPeru.json" shape="rect"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Peru</option>
                <optgroup class="template" label="Región">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup></select>
            </fieldset>
           <fieldset class="navitem valid">
          <legend>Target Time</legend>
			<link class="starttime" rel="iridl:hasJSON" href="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/.target_date/F/last/cvsunits//F/parameter/VALUE/info.json" />
			<select class="pageformcopy" name="L">
			<optgroup class="template" label="Target Time">
			<option class="iridl:values L@value target_date"></option>
			</optgroup></select>
		  </fieldset>
		  -->
</div>
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
            <li><a href="#tabs-1" shape="rect">Descripción</a></li>
            <li><a href="#tabs-2" shape="rect">Más información</a></li>
            <li><a href="#tabs-3" shape="rect">Fuente</a></li>
            <li><a href="#tabs-4" shape="rect">Soporte</a></li>
            <li><a href="#tabs-5" shape="rect">Instrucciones</a></li>
    </ul>
<span id="content">
    <fieldset class="regionwithinbbox dlimage" about="">

<!-- comentando
<table>
<tr>
  <td rowspan="2" colspan="1"><img class="dlimgloc" src="http://iridl.ldeo.columbia.edu/SOURCES/.WORLDBATH/.bath/X/Y/%28bb:-82:-18:-68:0:bb%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-78%2C-7%2C-77.5%2C-6.5%29//region/parameter/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/grey/mews_prov/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif" />
  </td>
  <td rowspan="1" colspan="1">
    <span class="valid">
       <a class="startleadtime" rel="iridl:hasJSON" href="http://iridl.ldeo.columbia.edu/expert/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/.target_date/F/last/cvsunits//F/parameter/VALUE/L/first/cvsnamedunits//L/parameter/VALUE%5BL/F%5DREORDER/info.json" shape="rect"></a>
      <span class="template">
        <table bgcolor="f0f0f0">
           <tr>
           	<td rowspan="1" colspan="1">Target Date</td>
           	<td rowspan="1" colspan="1">Lead Time</td>
           	<td rowspan="1" colspan="1">Issue Date</td>
           </tr>
           <tr>
           	<th class="iridl:value" rowspan="1" colspan="1"></th>
           	<th class="iridl:hasIndependentVariables" rowspan="1" colspan="1">
               <span class="iridl:value"></span>
             </th>
           </tr>
         </table>
       </span>
     </span>
   </td>
</tr>
</table>
-->

<!--
   <br clear="none" />
    <div  style="display: inline-block; width:49%">
      <h4>Precipitación</h4>
  <img class="dlimgts" src="http://iridl.ldeo.columbia.edu/expert/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/a:/.prob/Y/low/high/RANGE//long_name/%28Probabilidad%20de%20Ocurrencia%29/def/dup/Y/0.5/boxAverage/X/0.5/boxAverage/%28bb:-78%2C-7%2C-77.5%2C-6.5%29//region/parameter/geoobject/rasterize/%5BX/Y%5DregridAverage/0/flaggt/exch/1/index/%5BX/Y%5Dweighted-average/dup/0/mul/3/-1/roll/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/:a:/.target_date/:a/C/fig-/grey/deltabars/plotlabel/plotlabel/plotlabel/black/dashed/33/1/33/horizontallines/-fig//X/-63.75/plotvalue//Y/-11.25/plotvalue//L/1.0/plotvalue//F/last/plotvalue//framelabel/%28Pronostico%20para%20%28%25=%5BX%5D%2C%25=%5BY%5D%29%20%25=%5Btarget_date%5D%20emitido%20en%20%25=%5BF%5D%29/psdef//XOVY/1/psdef//plotaxislength/200/psdef/+.gif" />
      </div>
      <div style="display: inline-block; width:49%">
	<h4>Temperatura</h4>
  <img class="dlimgts" src="http://iridl.ldeo.columbia.edu/expert/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Temperature/a:/.prob//long_name/%28Probabilidad%20de%20Ocurrencia%29/def/dup/%28bb:-78%2C-7%2C-77.5%2C-6.5%29//region/parameter/geoobject/rasterize/exch/1/index/%5BX/Y%5Dweighted-average/dup/0/mul/3/-1/roll/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/:a:/.target_date/:a/C/fig-/grey/deltabars/plotlabel/plotlabel/plotlabel/black/dashed/33/1/33/horizontallines/-fig//X/-63.75/plotvalue//Y/-11.25/plotvalue//L/1.0/plotvalue//F/last/plotvalue//framelabel/%28Pronostico%20para%20%28%25=%5BX%5D%2C%25=%5BY%5D%29%20%25=%5Btarget_date%5D%20emitido%20en%20%25=%5BF%5D%29/psdef//XOVY/1/psdef//plotaxislength/200/psdef/+.gif" />
	</div>
-->
	
	
  </fieldset>
<fieldset class="dlimage" about="">
<!--
<link rel="iridl:hasFigure" href="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/Y/-85/85/RANGE/a:/.dominant//long_name/%28Probabilidad%20de%20la%20clase%20mas%20dominante%29def/:a:/.target_date/:a/X/Y/fig-/colors/plotlabel/black/thin/coasts_gaz/thin/countries_gaz/-fig//L/1.0/plotvalue//F/last/plotvalue//antialias/true/psdef//framelabel/%28Pronostico%20para%20%25=%5Btarget_date%5D%29psdef//plotaxislength/590/psdef//plotborderbottom/40/psdef//plotbordertop/40/psdef//L/1.0/plotvalue//F/last/plotvalue/X/-82/-68/plotrange/Y/-18/0/plotrange//framelabelstart/%28%25=%5Btarget_date%5D%20Seasonal%20Forecast%20issued%20%25=%5BF%5D%29psdef/" />
<img class="dlimg" src="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/Y/-85/85/RANGE/a:/.dominant//long_name/%28Probabilidad%20de%20la%20clase%20mas%20dominante%29def/:a:/.target_date/:a/X/Y/fig-/colors/plotlabel/black/thin/coasts_gaz/thin/countries_gaz/-fig//L/1.0/plotvalue//F/last/plotvalue//antialias/true/psdef//framelabel/%28Pronostico%20para%20%25=%5Btarget_date%5D%29psdef//plotaxislength/590/psdef//plotborderbottom/40/psdef//plotbordertop/40/psdef//L/1.0/plotvalue//F/last/plotvalue/X/-82/-68/plotrange/Y/-18/0/plotrange//framelabelstart/%28%25=%5Btarget_date%5D%20Seasonal%20Forecast%20issued%20%25=%5BF%5D%29psdef/+.gif" border="0" alt="image" /><br clear="none" />
<img class="dlauximg" src="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/Y/-85/85/RANGE/a:/.dominant//long_name/%28Probabilidad%20de%20la%20clase%20mas%20dominante%29def/:a:/.target_date/:a/X/Y/fig-/colors/plotlabel/black/thin/coasts_gaz/thin/countries_gaz/-fig//L/1.0/plotvalue//F/last/plotvalue//antialias/true/psdef//framelabel/%28Pronostico%20para%20%25=%5Btarget_date%5D%29psdef//plotaxislength/590/psdef//plotborderbottom/40/psdef//plotbordertop/40/psdef//L/1.0/plotvalue//F/last/plotvalue/X/-82/-68/plotrange/Y/-18/0/plotrange//framelabelstart/%28%25=%5Btarget_date%5D%20Seasonal%20Forecast%20issued%20%25=%5BF%5D%29psdef/+.auxfig/+.gif" />
-->
<img class="dlimg" src="../../Riesgo/2016/MapaVulnerabilidad.png" alt="mapa riesgo"></img>
</fieldset>
</span>
<div id="tabs-1" class="ui-tabs-panel" about="">
<h2 align="center"  property="term:title" >Planes Para Escenarios de Alto Riesgo Ante Sequía - 2016</h2>
<p align="left" property="term:description">Para el periodo enero – marzo	del 2016 se presenta la siguiente tabla con el cronograma de actividades y plan de contingencia por Autoridad Administrativa del Agua. Estas actividades fueron desarrolladas con el objetivo de minimizar las pérdidas por las condiciones de sequía que se presenten ante un fenómeno El Niño.</p>
<p>Los escenarios de alto riesgo ante sequía, el cual se basó en los niveles de susceptibilidad del ámbito expuesto a sequías meteorológicas y los niveles de exposición según el índice de pobreza, sirvieron de sustento para el desarrollo del plan de contingencia. Producto de estos escenarios, se identificaron 187 distritos con un nivel de riesgo muy alto, las cuales se encuentran distribuidas en 36 cuencas.</p>
<p>Las condiciones de sequía meteorológica fueron dadas por el SPI-3 (Índice de Precipitación Estandarizado de tres meses) para los periodos de enero – marzo de los años 83 y 92, ambos afectados con periodos Niño pero de diferentes intensidades. También se utilizaron los pronósticos climáticos del SENAMHI y otras agencias internacionales.</p>

<p>Los cronogramas por cuenca:</p>

<p>
	<table border="1">
            <tr style="text-align:center; background-color:#4c69ba; color:#ffffff">
                <td><b>AAA</b></td>
			    <td><b>ALA</b></td>
			    <td><b>CODIGO U.H.</b></td>
			    <td><b>NOMBRE U.H.</b></td>
			    <td><b>CRONOGRAMA</b></td>
            </tr>
            <tr style="background-color:#E0ECF8">
			    <td rowspan="14">Caplina - Ocoña</td>
			    <td rowspan="6">Tacna</td>
			    <td>1314</td>
			    <td>Cuenca Lluta</td>
			    <td rowspan="14" style="text-align:center"><a href="CronAAA_CaplinaOcona.pdf" target="_blank"><img src="../../icons/pdf.png"></img></a></td>
		    </tr>
            <tr style="background-color:#E0ECF8">
			    <td>13154</td>
			    <td>Cuenca Hospicio</td>
		    </tr>
            <tr style="background-color:#E0ECF8">
			    <td>13156</td>
			    <td>Cuenca Caplina</td>
		    </tr>
            <tr style="background-color:#E0ECF8">
			    <td>0144</td>
			    <td>Cuenca Maure</td>
		    </tr>
            <tr style="background-color:#E0ECF8">
			    <td>0146</td>
			    <td>Cuenca Caño</td>
		    </tr>
            <tr style="background-color:#E0ECF8">
			    <td>0148</td>
			    <td>Cuenca Ushusuma</td>
		    </tr>
            <tr style="background-color:#E0ECF8">
                <td rowspan="2">Locumba - Sama</td>
			    <td>13158</td>
			    <td>Cuenca Sama</td>
		    </tr>
            <tr style="background-color:#E0ECF8">
			    <td>1316</td>
			    <td>Cuenca Locumba</td>
		    </tr>
            <tr style="background-color:#E0ECF8">
                <td rowspan="1">Moquegua</td>
			    <td>13172</td>
			    <td>Cuenca Ilo - Moquegua</td>
		    </tr>
            <tr style="background-color:#E0ECF8">
                <td rowspan="1">Tambo - Alto Tambo</td>
			    <td>1318</td>
			    <td>Cuenca Tambo</td>
		    </tr>
            <tr style="background-color:#E0ECF8">
                <td rowspan="1">Chili</td>
			    <td>132</td>
			    <td>Cuenca Quilca - Vitor - Chili</td>
		    </tr>
            <tr style="background-color:#E0ECF8">
                <td rowspan="1">Camana - Majes</td>
			    <td>134</td>
			    <td>Cuenca Camaná (*)</td>
		    </tr>
            <tr style="background-color:#E0ECF8">
                <td rowspan="1">Colca - Sihuas - Chivay</td>
			    <td>134</td>
			    <td>Cuenca Camaná (*)</td>
		    </tr>
            <tr style="background-color:#E0ECF8">
                <td rowspan="1">Ocoña - Pausa</td>
			    <td>136</td>
			    <td>Cuenca Ocoña</td>
		    </tr>
            <tr>
			    <td rowspan="5">Chaparra - Chincha</td>
			    <td rowspan="2">Chaparra - Acarí</td>
			    <td>13716</td>
			    <td>Cuenca Yauca</td>
			    <td rowspan="5" style="text-align:center">
					<!--<a href="../../Manual_Observatorio_Sequias_esp.pdf" target="_blank"><img src="../../icons/pdf.png"></img></a>-->
				</td>
		    </tr>
            <tr>
			    <td>13718</td>
			    <td>Cuenca Acari</td>
		    </tr>
            <tr>
                <td rowspan="1">Ica</td>
			    <td>1374</td>
			    <td>Cuenca Ica</td>
		    </tr>
            <tr>
                <td rowspan="1">Pisco</td>
			    <td>13752</td>
			    <td>Cuenca Pisco</td>
		    </tr>
             <tr>
                <td rowspan="1">San Juan</td>
			    <td>137532</td>
			    <td>Cuenca San Juan</td>
		    </tr>
            <tr style="background-color:#E0ECF8">
			    <td rowspan="2">Cañete - Fortaleza</td>
			    <td rowspan="2">Mala - Omas - Cañete</td>
			    <td>13754</td>
			    <td>Cuenca Cañete</td>
			    <td rowspan="2" style="text-align:center">
					<!--<a href="../../Manual_Observatorio_Sequias_esp.pdf" target="_blank"><img src="../../icons/pdf.png"></img></a>-->
				</td>
		    </tr>
            <tr style="background-color:#E0ECF8">
			    <td>137552</td>
			    <td>Cuenca Mala</td>
		    </tr>
            <tr>
			    <td rowspan="2">Ucayali</td>
			    <td rowspan="2">Perene</td>
			    <td>4992</td>
			    <td>Cuenca Pachitea</td>
			    <td rowspan="2" style="text-align:center">
					<!--<a href="../../Manual_Observatorio_Sequias_esp.pdf" target="_blank"><img src="../../icons/pdf.png"></img></a>-->
				</td>
		    </tr>
            <tr>
			    <td>49954</td>
			    <td>Cuenca Perene</td>
		    </tr>
            <tr style="background-color:#E0ECF8">
			    <td rowspan="1">Mantaro</td>
			    <td rowspan="1">Mantaro</td>
			    <td>4996</td>
			    <td>Cuenca Mantaro</td>
			    <td rowspan="1" style="text-align:center">
					<!--<a href="../../Manual_Observatorio_Sequias_esp.pdf" target="_blank"><img src="../../icons/pdf.png"></img></a>-->
				</td>
		    </tr>
            <tr>
			    <td rowspan="3">Pampas - Apurimac</td>
			    <td rowspan="2">Bajo Apurímac - Pampas</td>
			    <td>4997</td>
			    <td>Intercuenca Bajo Apurímac</td>
			    <td rowspan="3" style="text-align:center">
					<!--<a href="../../Manual_Observatorio_Sequias_esp.pdf" target="_blank"><img src="../../icons/pdf.png"></img></a>-->
				</td>
		    </tr>
            <tr>
			    <td>4998</td>
			    <td>Cuenca Pampas</td>
		    </tr>
            <tr>
                <td rowspan="1">Alto Apurímac - Velille</td>
			    <td>4999</td>
			    <td>Cuenca Alto Apurímac</td>
		    </tr>
            <tr style="background-color:#E0ECF8">
			    <td rowspan="2">Urubamba - Vilcanota</td>
			    <td rowspan="1">Cusco</td>
			    <td>4994</td>
			    <td>Cuenca Urubamba (*)</td>
			    <td rowspan="2" style="text-align:center">
					<!--<a href="../../Manual_Observatorio_Sequias_esp.pdf" target="_blank"><img src="../../icons/pdf.png"></img></a>-->
				</td>
		    </tr>
            <tr style="background-color:#E0ECF8">
                <td rowspan="1">La Convención</td>
			    <td>4994</td>
			    <td>Cuenca Urubamba (*)</td>
		    </tr>
            <tr>
			    <td rowspan="9">Titicaca</td>
			    <td rowspan="4">Ilave</td>
			    <td>0156</td>
			    <td>Cuenca Callaccame</td>
			    <td rowspan="9" style="text-align:center">
					<!--<a href="../../Manual_Observatorio_Sequias_esp.pdf" target="_blank"><img src="../../icons/pdf.png"></img></a>-->
				</td>
		    </tr>
            <tr>
			    <td>016</td>
			    <td>Cuenca Ilave</td>
		    </tr>
            <tr>
			    <td>0174</td>
			    <td>Cuenca Ilpa</td>
		    </tr>
			<tr>
			    <td>0154</td>
			    <td>Cuenca Maure Chico</td>
		    </tr>
            <tr>
                <td rowspan="1">Juliaca</td>
			    <td>0176</td>
			    <td>Cuenca Coata</td>
		    </tr>
            <tr>
                <td rowspan="1">Huancané</td>
			    <td>0178</td>
			    <td>Cuenca Huancané</td>
		    </tr>
            <tr>
                <td rowspan="3">Ramis</td>
			    <td>0179</td>
			    <td>Intercuenca Ramis</td>
		    </tr>
            <tr>
			    <td>018</td>
			    <td>Cuenca Pucará</td>
		    </tr>
            <tr>
			    <td>019</td>
			    <td>Cuenca Azángaro</td>
		    </tr>
        </table>
		<b><i><small>(*) Cuenca ubicada en más de una Administración Local de Agua</small></i></b>
</p>

<p align="left">Mapa elaborado por  <a href="http://cenepred.gob.pe/" target="_blank"><img src="../../icons/Logo_Cenepred.png" alt="Logo Cenepred"></img></a></p>
<p align="left">Cuadro elaborado por  <a href="http://www.ana.gob.pe/" target="_blank"><img src="../../icons/LogoANA_Azul2.png" alt="Logo ANA"></img></a></p>
</div>

<div id="tabs-2"  class="ui-tabs-panel">
<h2 align="center">Documentos consultados</h2>
<p>Para un mayor detalle y entendimiento respecto a los mapas presentados, su desarrollo y análisis consultar el 
	"ESCENARIO DE RIESGO POR SEQUÍA ANTE UN FENÓMENO EL NIÑO, PARA EL PERIODO ENERO-MARZO 2016" 
</p>
<p>
	Ver documento 
	<a href="EscenarioSequiaFEN_EneMar2016.pdf" target="_blank"><img src="../../icons/pdf.png"></img></a>
	desarrollado por el CENEPRED.
 </p>
</div>

<div id="tabs-3" class="ui-tabs-panel">
<h2  align="center">Fuente de los Datos</h2>
<p>Escenarios de riesgo, elaborado por  <a href="http://cenepred.gob.pe/"><img src="../../icons/Logo_Cenepred.png" alt="Logo IRI"></img></a></p>
</div>

<div id="tabs-4"  class="ui-tabs-panel">
<h3  align="center">Soporte</h3>
<p>
Sus consultas a: <a href="mailto:snirh@ana.gob.pe?subject=Pron&#243;stico Per&#250;">snirh@ana.gob.pe</a>
 </p>
</div>
<div id="tabs-5"  class="ui-tabs-panel">
<h3  align="center">Instrucciones</h3>
<div class="buttonInstructions"></div>
</div>
</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend></fieldset>
</div>
 </body>
 </html>
