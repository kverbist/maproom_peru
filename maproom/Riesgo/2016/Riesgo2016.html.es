<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#" xmlns:og="http://ogp.me/ns#" 
      version="XHTML+RDFa 1.0"
      >
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta xml:lang="es" property="maproom:Entry_Id" content="Riesgo_2016" />
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<title>Escenario de Riesgo Ante Sequía - 2016</title>
<link rel="stylesheet" type="text/css" href="../../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="IRI_Forecast.html?Set-Language=en" />
<link class="share" rel="canonical" href="Riesgo2016.html" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Riesgo_2016_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation"/>
  <link rel="term:icon" href="MapaRiesgoSequiaNac-300.jpg" />
  <script type="text/javascript" src="../../../uicore/uicore.js" xml:space="preserve"></script>
  <script type="text/javascript" src="../../maproom/unesco.js" xml:space="preserve"></script>
  </head>
<body xml:lang="es">
<form name="pageform" id="pageform" class="carryup carry dlimg dlauximg dlimgts dlimgloc share starttime startleadtime" method="get" enctype="application/x-www-form-urlencoded">
<input class="carryLanguage carryup carry " name="Set-Language" type="hidden" />
<input class="carry dlimg dlimgloc share" name="bbox" type="hidden" />
<input class="dlimg dlimgts share starttime startleadtime" name="F" type="hidden" />
<input class="dlimg dlimgts share startleadtime" name="L" type="hidden" value="1." />
<input class="dlimgnotuseddlauximg" name="plotaxislength" type="hidden" />
<input class="share dlimgts dlimgloc" name="region" type="hidden" />
<input class="pickarea" name="resolution" type="hidden" value=".5" />
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>Maproom</legend> 
                      <a rev="section" class="navlink carryup" href="/maproom/Riesgo/" shape="rect">Riesgo Ante Sequía</a>
            </fieldset> 
            <fieldset class="navitem" id="chooseSection"> 
                <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Riesgo_2016_term"><span property="term:label">Escenarios de Riesgo</span></legend>
            </fieldset> 
			<!--
            <fieldset class="navitem">
                <legend>Region</legend>
                <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregionsPeru.json" shape="rect"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Peru</option>
                <optgroup class="template" label="Región">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup></select>
            </fieldset>
           <fieldset class="navitem valid">
          <legend>Target Time</legend>
			<link class="starttime" rel="iridl:hasJSON" href="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/.target_date/F/last/cvsunits//F/parameter/VALUE/info.json" />
			<select class="pageformcopy" name="L">
			<optgroup class="template" label="Target Time">
			<option class="iridl:values L@value target_date"></option>
			</optgroup></select>
		  </fieldset>
		  -->
 </div>
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
            <li><a href="#tabs-1" shape="rect">Descripción</a></li>
            <li><a href="#tabs-2" shape="rect">Más información</a></li>
            <li><a href="#tabs-3" shape="rect">Fuente</a></li>
            <li><a href="#tabs-4" shape="rect">Soporte</a></li>
            <li><a href="#tabs-5" shape="rect">Instrucciones</a></li>
    </ul>
<span id="content">
    <fieldset class="regionwithinbbox dlimage" about="">

<!-- comentando
<table>
<tr>
  <td rowspan="2" colspan="1"><img class="dlimgloc" src="http://iridl.ldeo.columbia.edu/SOURCES/.WORLDBATH/.bath/X/Y/%28bb:-82:-18:-68:0:bb%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-78%2C-7%2C-77.5%2C-6.5%29//region/parameter/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/grey/mews_prov/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif" />
  </td>
  <td rowspan="1" colspan="1">
    <span class="valid">
       <a class="startleadtime" rel="iridl:hasJSON" href="http://iridl.ldeo.columbia.edu/expert/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/.target_date/F/last/cvsunits//F/parameter/VALUE/L/first/cvsnamedunits//L/parameter/VALUE%5BL/F%5DREORDER/info.json" shape="rect"></a>
      <span class="template">
        <table bgcolor="f0f0f0">
           <tr>
           	<td rowspan="1" colspan="1">Target Date</td>
           	<td rowspan="1" colspan="1">Lead Time</td>
           	<td rowspan="1" colspan="1">Issue Date</td>
           </tr>
           <tr>
           	<th class="iridl:value" rowspan="1" colspan="1"></th>
           	<th class="iridl:hasIndependentVariables" rowspan="1" colspan="1">
               <span class="iridl:value"></span>
             </th>
           </tr>
         </table>
       </span>
     </span>
   </td>
</tr>
</table>
-->

<!--
   <br clear="none" />
    <div  style="display: inline-block; width:49%">
      <h4>Precipitación</h4>
  <img class="dlimgts" src="http://iridl.ldeo.columbia.edu/expert/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/a:/.prob/Y/low/high/RANGE//long_name/%28Probabilidad%20de%20Ocurrencia%29/def/dup/Y/0.5/boxAverage/X/0.5/boxAverage/%28bb:-78%2C-7%2C-77.5%2C-6.5%29//region/parameter/geoobject/rasterize/%5BX/Y%5DregridAverage/0/flaggt/exch/1/index/%5BX/Y%5Dweighted-average/dup/0/mul/3/-1/roll/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/:a:/.target_date/:a/C/fig-/grey/deltabars/plotlabel/plotlabel/plotlabel/black/dashed/33/1/33/horizontallines/-fig//X/-63.75/plotvalue//Y/-11.25/plotvalue//L/1.0/plotvalue//F/last/plotvalue//framelabel/%28Pronostico%20para%20%28%25=%5BX%5D%2C%25=%5BY%5D%29%20%25=%5Btarget_date%5D%20emitido%20en%20%25=%5BF%5D%29/psdef//XOVY/1/psdef//plotaxislength/200/psdef/+.gif" />
      </div>
      <div style="display: inline-block; width:49%">
	<h4>Temperatura</h4>
  <img class="dlimgts" src="http://iridl.ldeo.columbia.edu/expert/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Temperature/a:/.prob//long_name/%28Probabilidad%20de%20Ocurrencia%29/def/dup/%28bb:-78%2C-7%2C-77.5%2C-6.5%29//region/parameter/geoobject/rasterize/exch/1/index/%5BX/Y%5Dweighted-average/dup/0/mul/3/-1/roll/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/:a:/.target_date/:a/C/fig-/grey/deltabars/plotlabel/plotlabel/plotlabel/black/dashed/33/1/33/horizontallines/-fig//X/-63.75/plotvalue//Y/-11.25/plotvalue//L/1.0/plotvalue//F/last/plotvalue//framelabel/%28Pronostico%20para%20%28%25=%5BX%5D%2C%25=%5BY%5D%29%20%25=%5Btarget_date%5D%20emitido%20en%20%25=%5BF%5D%29/psdef//XOVY/1/psdef//plotaxislength/200/psdef/+.gif" />
	</div>
-->
	
	
  </fieldset>
<fieldset class="dlimage" about="">
<!--
<link rel="iridl:hasFigure" href="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/Y/-85/85/RANGE/a:/.dominant//long_name/%28Probabilidad%20de%20la%20clase%20mas%20dominante%29def/:a:/.target_date/:a/X/Y/fig-/colors/plotlabel/black/thin/coasts_gaz/thin/countries_gaz/-fig//L/1.0/plotvalue//F/last/plotvalue//antialias/true/psdef//framelabel/%28Pronostico%20para%20%25=%5Btarget_date%5D%29psdef//plotaxislength/590/psdef//plotborderbottom/40/psdef//plotbordertop/40/psdef//L/1.0/plotvalue//F/last/plotvalue/X/-82/-68/plotrange/Y/-18/0/plotrange//framelabelstart/%28%25=%5Btarget_date%5D%20Seasonal%20Forecast%20issued%20%25=%5BF%5D%29psdef/" />
<img class="dlimg" src="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/Y/-85/85/RANGE/a:/.dominant//long_name/%28Probabilidad%20de%20la%20clase%20mas%20dominante%29def/:a:/.target_date/:a/X/Y/fig-/colors/plotlabel/black/thin/coasts_gaz/thin/countries_gaz/-fig//L/1.0/plotvalue//F/last/plotvalue//antialias/true/psdef//framelabel/%28Pronostico%20para%20%25=%5Btarget_date%5D%29psdef//plotaxislength/590/psdef//plotborderbottom/40/psdef//plotbordertop/40/psdef//L/1.0/plotvalue//F/last/plotvalue/X/-82/-68/plotrange/Y/-18/0/plotrange//framelabelstart/%28%25=%5Btarget_date%5D%20Seasonal%20Forecast%20issued%20%25=%5BF%5D%29psdef/+.gif" border="0" alt="image" /><br clear="none" />
<img class="dlauximg" src="http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.FD/.Seasonal_Forecast/.Precipitation/Y/-85/85/RANGE/a:/.dominant//long_name/%28Probabilidad%20de%20la%20clase%20mas%20dominante%29def/:a:/.target_date/:a/X/Y/fig-/colors/plotlabel/black/thin/coasts_gaz/thin/countries_gaz/-fig//L/1.0/plotvalue//F/last/plotvalue//antialias/true/psdef//framelabel/%28Pronostico%20para%20%25=%5Btarget_date%5D%29psdef//plotaxislength/590/psdef//plotborderbottom/40/psdef//plotbordertop/40/psdef//L/1.0/plotvalue//F/last/plotvalue/X/-82/-68/plotrange/Y/-18/0/plotrange//framelabelstart/%28%25=%5Btarget_date%5D%20Seasonal%20Forecast%20issued%20%25=%5BF%5D%29psdef/+.auxfig/+.gif" />
-->
<img class="dlimg" src="../../Riesgo/2016/MapaRiesgoSequiaNac-300.jpg" alt="mapa riesgo"></img>
</fieldset>
</span>
<div id="tabs-1" class="ui-tabs-panel" about="">
<h2 align="center"  property="term:title" >Escenario de Riesgo Ante Sequía - 2016</h2>
<p align="left" property="term:description">Para el desarrollo de los niveles de susceptibilidad ante sequías, se hizo una recopilación de información de instituciones como el SENAMHI, ANA y DIA-MINAGRI. La información recopilada fue analizada, dando como resultado las variables que intervienen en la determinación de las zonas más susceptibles a las sequías.</p>
<p>Para  la caracterización del fenómeno de sequías se utilizó el Índice de Precipitación Estandarizada (SPI por sus siglas en inglés) durante los periodos enero – marzo de los años 1983 y 1992 (ambos periodos afectados por la presencia del evento “Niño”, pero de diferente intensidad); así como las precipitaciones observadas en lo que va del periodo lluvioso 2015 – 2016.</p>
<p>Con las consideraciones expuestas, se logra identificar los distritos con mayor susceptibilidad a la ocurrencia de sequías meteorológicas. Finalmente, se hace un análisis de elementos expuestos, donde se ha estimado la producción agrícola expuesta ante estas condiciones de sequía ante el evento “El Niño” para el trimestre enero – marzo 2016.</p>
<p>Una vez identificados los niveles de susceptibilidad del ámbito expuesto a sequías y los niveles de exposición según el índice de pobreza se procede a la conjunción de ambos factores para el cálculo de la probabilidad de riesgo. El resultado muestra un total de 187 distritos con un nivel de riesgo muy alto, que representa zonas con déficit calificado de extremadamente seco; así como zonas con alto porcentaje de pobreza. Así mismo un total de población expuesta de 777 740 habitantes.</p>
</div>

<div id="tabs-2"  class="ui-tabs-panel">
<h2 align="center">Documentos consultados</h2>
<p>Para un mayor detalle y entendimiento respecto a los mapas presentados, su desarrollo y análisis consultar el 
	"ESCENARIO DE RIESGO POR SEQUÍA ANTE UN FENÓMENO EL NIÑO, PARA EL PERIODO ENERO-MARZO 2016" 
</p>
<p>
	Ver documento 
	<a href="EscenarioSequiaFEN_EneMar2016.pdf" target="_blank"><img src="../../icons/pdf.png"></img></a>
	desarrollado por el CENEPRED.
 </p>
</div>

<div id="tabs-3" class="ui-tabs-panel">
<h2  align="center">Fuente de los Datos</h2>
<p>Escenarios de riesgo, elaborado por  <a href="http://cenepred.gob.pe/"><img src="../../icons/Logo_Cenepred.png" alt="Logo IRI"></img></a></p>
</div>

<div id="tabs-4"  class="ui-tabs-panel">
<h3  align="center">Soporte</h3>
<p>
Sus consultas a: <a href="mailto:snirh@ana.gob.pe?subject=Pron&#243;stico Per&#250;">snirh@ana.gob.pe</a>
 </p>
</div>
<div id="tabs-5"  class="ui-tabs-panel">
<h3  align="center">Instrucciones</h3>
<div class="buttonInstructions"></div>
</div>
</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend></fieldset>
</div>
 </body>
 </html>
