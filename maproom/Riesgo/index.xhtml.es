<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
 xml:lang="es"
      >
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<title>Escenario de Riesgo Ante Sequía</title>
<link rel="stylesheet" type="text/css" href="../unesco.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="index.html?Set-Language=en" />
<link rel="canonical" href="index.html" />
<meta property="maproom:Sort_Id" content="a04" />
<link class="" rel="home" href="http://www.minagri.gob.cl/agroclimatico/unidad_nacional.php/" title="UNEA" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="../../maproom//navmenu.json" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#forecast" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#riesgo" />
<link rel="term:icon" href="2016/MapaRiesgoSequiaNac-300.jpg" />
<script type="text/javascript" src="../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../maproom/unesco.js"></script>
</head>
<body>

<form name="pageform" id="pageform">
<input class="carryLanguage carryup" name="Set-Language" type="hidden" />
</form>
<div class="controlBar">
           <fieldset class="navitem"> 
                <legend>Peru</legend> 
                    <a rev="section" class="navlink carryup" href="/maproom/">Observatorio Nacional de Sequía</a>
            </fieldset> 
           <fieldset class="navitem"> 
                <legend>Peru</legend> 
                     <span class="navtext">Escenario de Riesgo Ante Sequía</span>
            </fieldset> 
</div>
<div>
<div id="content" class="searchDescription">
<h2 property="term:title">Escenario de Riesgo Ante Sequía</h2>
<p align="left" property="term:description">
	Mapas de los escenarios de riesgo ante sequías por unidades hidrográficas para el periodo enero - marzo del 2016 
	con sus respectivos planes de contingencia por parte de las Autoridades Administrativas del Agua.
</p>
<p>Estos escenarios fueron elaborados por CENEPRED.</p>
</div>
<div class="rightcol tabbedentries" about="/maproom/Riesgo/" >
  <link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Riesgo_2016_term" />
</div>

</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend>

</fieldset>
</div>
 </body>

 </html>
